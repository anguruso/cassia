﻿using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Common.DataContainers;
using Game.Models.Stage;
using NUnit.Framework;

namespace UnitTest.Game.Models.Stage
{
    [TestFixture]
    public class SelectableSquaresTest
    {
        private SelectableSquares _selectableSquares;

        [SetUp]
        public void SetUp()
        {
            _selectableSquares = new SelectableSquares();
        }

        [Test]
        public void Select()
        {
            _selectableSquares.Set(new List<GridCoords> { new GridCoords(1, 2), new GridCoords(3, 4) });
            CollectionAssert.AreEqual(new List<GridCoords> { new GridCoords(1, 2), new GridCoords(3, 4) }, _selectableSquares.Get());
        }

        [Test]
        public void SelectWithOtherSelected()
        {
            _selectableSquares.Set(new List<GridCoords> { new GridCoords(1, 2) });
            _selectableSquares.Set(new List<GridCoords> { new GridCoords(3, 4) });
            CollectionAssert.AreEqual(new List<GridCoords> { new GridCoords(3, 4) }, _selectableSquares.Get());
        }

        [Test]
        public void Deselect()
        {
            _selectableSquares.Set(new List<GridCoords> { new GridCoords(1, 2), new GridCoords(3, 4) });
            _selectableSquares.Clear();
            CollectionAssert.AreEqual(new List<GridCoords>(), _selectableSquares.Get());
        }

        [Test]
        public void GetDefault()
        {
            CollectionAssert.AreEqual(new List<GridCoords>(), _selectableSquares.Get());
        }

        // TODO add?
    }
}
