﻿using System.Collections.Generic;
using Common.DataContainers;

namespace Game.Controllers.Units
{
    public interface IUnitActionRangeCalculator
    {
        IEnumerable<GridCoords> GetRange(GridCoords target, Facing direction, IEnumerable<GridCoordsOffset> directionalOffsets);
        IEnumerable<GridCoords> GetRotatedRange(GridCoords gridCoords, List<GridCoordsOffset> directionalOffsets);
    }
}