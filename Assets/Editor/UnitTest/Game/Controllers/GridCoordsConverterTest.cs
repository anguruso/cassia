﻿using System;
using Common.DataContainers;
using Config;
using Game.Controllers;
using NUnit.Framework;
using UnitTest.Utils;
using UnityEngine;

namespace UnitTest.Game.Controllers
{
	[TestFixture]
	public class GridCoordsConverterTest
	{
		private WorldGridCoordsConverter _converter;

		[SetUp]
		public void SetUp()
		{
		    var heights = new int[,]
		    {
		        {1, 3, 1},
		        {2, 3, 1},
		        {1, 2, 2}
		    };


            var mapBuilderConfig = new MapInstantiatorConfig
			{
				ChunkSize = 1,
				ChunkHeight = 0.1f
			};
			
			_converter = new WorldGridCoordsConverter();
			_converter.LoadConfigs(heights, mapBuilderConfig);
		}

		[Test]
		public void ConvertWorldPositionToGridCoords()
		{
			var worldPosition = new Vector3(-1, 0.3f, 0);
			var expected = new GridCoords(0, 1);
			var actual = _converter.GetGridCoords(worldPosition);

			Assert.AreEqual(expected, actual);
		}

		[Test]
		public void ConvertGridCoordsToWorldPosition()
		{
			var gridCoords = new GridCoords(0, 2);
			var expected = new Vector3(-1f, 0.1f, 1f);
			var actual = _converter.GetWorldPosition(gridCoords);

			Assert.AreEqual(expected, actual);
		}

		[Test]
		public void ConvertWorldPositionToNearestGridCoords()
		{
			var worldPosition = new Vector3(0.5f, 0f, 1.4f);
			var expected = new GridCoords(2, 2);
			var actual = _converter.GetGridCoords(worldPosition);

			Assert.AreEqual(expected, actual);
		}
		
		[Test]
		public void WorldPositionsNotInMap()
		{
			var worldPosition = new Vector3(999f, 0f, -999f);
			var expected = new GridCoords(2, 0);
			var actual = _converter.GetGridCoords(worldPosition);

			Assert.AreEqual(expected, actual);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException),
			ExpectedMessage = "Cannot get world position for GridCoords(0, 3).  Map is of size (3, 3).")]
		public void GetInvalidGridCoords()
		{
			var gridCoords = new GridCoords(0, 3);
			_converter.GetWorldPosition(gridCoords);
		}

		[Test]
		public void GetClosestWorldPositionCenter()
		{
			var worldPosition = new Vector3(-1.5f, 0f, 0.5f);
			var expected = new Vector3(-1f, 0.1f, 1f);
			var actual = _converter.GetClosestWorldPositionCenter(worldPosition);

			Assert.AreEqual(expected, actual);
		}

		[Test]
		public void GetRotation()
		{
			var forwardQuaternion = Quaternion.Euler(0, 0, 0);
			var backQuaternion = Quaternion.Euler(0, 180, 0);
			var leftQuaternion = Quaternion.Euler(0, 270, 0);
			var rightQuaternion = Quaternion.Euler(0, 90, 0);

			Assert.AreEqual(forwardQuaternion, _converter.GetRotation(Facing.Forward));
			Assert.AreEqual(backQuaternion, _converter.GetRotation(Facing.Back));
			Assert.AreEqual(leftQuaternion, _converter.GetRotation(Facing.Left));
			Assert.AreEqual(rightQuaternion, _converter.GetRotation(Facing.Right));
		}
	}
}
