﻿using System.Collections.Generic;
using Common.DataContainers;
using Config;
using Game.Controllers.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Signals.ViewSignals;

namespace Game.Models.Occupants.Units
{
	public interface IUnit : IOccupant
    {
		IMoveUnitViewSignal MoveUnitViewSignal { get; }
		ISetUnitPositionViewSignal SetUnitPositionViewSignal { get; }
	    void LoadConfig(UnitConfig config);
        string Name { get; }
		int Jump { get; }
		int Move { get; }
	    IUnitStats Stats { get; }
	    IList<GridCoords> GetPathTo(GridCoords targetCoords);
		IList<GridCoords> GetMovementRange();
	    IUnitAction GetUnitAction(int index);
        IList<IUnitAction> EquippedUnitActions { get; }
		Facing Facing { get; set; }
		void ApplyUnitActionEffect(IUnitActionEffect unitActionEffect, IUnitStats actorStats);
    }
}