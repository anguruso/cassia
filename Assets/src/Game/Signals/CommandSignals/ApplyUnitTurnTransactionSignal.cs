﻿using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.CommandSignals
{
    public class ApplyUnitTurnTransactionSignal : Signal, IApplyUnitTurnTransactionSignal
    {
    }

    public interface IApplyUnitTurnTransactionSignal : ISignal
    {
    }
}
