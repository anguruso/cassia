﻿using Common.Signals;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using strange.extensions.signal.impl;

namespace Game.Signals.ControllerSignals
{
    public class MoveUnitViewCompleteSignal : Signal<IUnit>, IMoveUnitViewCompleteSignal
    {
    }

    public interface IMoveUnitViewCompleteSignal : ISignal<IUnit>
    {
    }
}
