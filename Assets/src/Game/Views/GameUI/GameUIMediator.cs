﻿using System.Linq;
using Common.DataContainers;
using Game.Controllers.UI;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitTurnTransactions;
using Game.Signals.CommandSignals;
using Game.Signals.ControllerSignals;
using Game.Signals.ViewSignals;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views.GameUI
{
    public class GameUIMediator : Mediator
    {
        [Inject]
        public GameUIView View { get; set; }

        [Inject]
        public IUnitTurnTransaction UnitTurnTransaction { get; set; }

        [Inject]
        public ITriggerUnitActionButtonSignal TriggerUnitActionButtonSignal { get; set; }

        [Inject]
        public ITriggerApplyTransactionButtonSignal TriggerApplyTransactionButtonSignal { get; set; }

		[Inject]
		public ITriggerCancelTransactionButtonSignal TriggerCancelTransactionButtonSignal { get; set; }

        [Inject]
        public IUpdateGameUIViewSignal UpdateGameUISignal { get; set; }

		[Inject]
		public ITriggerUnitSignal TriggerUnitSignal { get; set; }

        public override void OnRegister()
        {
            UpdateGameUISignal.AddListener(UpdateUI);
			TriggerUnitSignal.AddListener(TriggerUnit);

            View.UnitActionButtonClicked.AddListener(TriggerUnitActionButtonSignal.Dispatch);
            View.ApplyButtonClicked.AddListener(TriggerApplyTransactionButtonSignal.Dispatch);
			View.CancelButtonClicked.AddListener(TriggerCancelTransactionButtonSignal.Dispatch);

            View.DisplayNothing();
        }

        private void UpdateUI()
        {
	        switch (UnitTurnTransaction.CurrentState)
	        {
		        case UnitTurnTransactionState.ActorSelected:
			        View.DisplayUnitOverview(UnitTurnTransaction.Actor);
			        break;

		        case UnitTurnTransactionState.Moving:
			        View.DisplayUnitOverview(UnitTurnTransaction.Actor);
			        break;

		        case UnitTurnTransactionState.PreSelectAction:
					View.DisplayUnitActions(UnitTurnTransaction.Actor, UnitTurnTransaction.Actor.EquippedUnitActions.Select(x => x.Name).ToList());
			        break;

				case UnitTurnTransactionState.PreSelectTarget:
					View.DisplayUnitOverview(UnitTurnTransaction.Actor);
			        break;

				case UnitTurnTransactionState.PreApplyTransaction:
			        var firstOutcome = UnitTurnTransaction.Outcome.UnitActionOutcomes.First();
					View.DisplayUnitActionEffects(firstOutcome.Effects);
			        break;

				default:
					View.DisplayNothing();
			        break;
	        }
        }

	    private void TriggerUnit(IUnit unit, GridCoords position)
	    {
		    if (UnitTurnTransaction.CurrentState != UnitTurnTransactionState.PreApplyTransaction) return;

		    var selectedAffectedUnit = UnitTurnTransaction.Outcome.UnitActionOutcomes.FirstOrDefault(x => x.AffectedUnit == unit);
		    if (selectedAffectedUnit != null)
		    {
			    View.DisplayUnitActionEffects(selectedAffectedUnit.Effects);
		    }
	    }
    }
}
