﻿
using Common.DataContainers;
using Game.Controllers;
using NUnit.Framework;

namespace UnitTest.Game.Controllers
{
    [TestFixture]
    public class DirectionCalculatorTest
    {
        private DirectionCalculator _directionCalculator;

        [SetUp]
        public void SetUp()
        {
            _directionCalculator = new DirectionCalculator();
        }

        [Test]
        public void GetExactDirection()
        {
            Assert.AreEqual(Facing.Right, _directionCalculator.GetFacing(new GridCoords(5, 5), new GridCoords(6, 5)));
            Assert.AreEqual(Facing.Left, _directionCalculator.GetFacing(new GridCoords(5, 5), new GridCoords(4, 5)));
            Assert.AreEqual(Facing.Forward, _directionCalculator.GetFacing(new GridCoords(5, 5), new GridCoords(5, 6)));
            Assert.AreEqual(Facing.Back, _directionCalculator.GetFacing(new GridCoords(5, 5), new GridCoords(5, 4)));
        }

        [Test]
        public void GetClosestDirection()
        {
            var direction = _directionCalculator.GetFacing(new GridCoords(5, 5), new GridCoords(7, 6));
            Assert.AreEqual(Facing.Right, direction);
        }
    }
}
