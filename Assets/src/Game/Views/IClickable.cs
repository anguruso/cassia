﻿namespace Game.Views
{
	public interface IClickable
	{
		void OnClick(Click click);
	}
}