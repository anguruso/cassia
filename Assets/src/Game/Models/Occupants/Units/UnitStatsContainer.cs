using System.Collections.Generic;
using System.Linq;
using Config;

namespace Game.Models.Occupants.Units
{
    public class UnitStatsContainer
    {
        private Dictionary<StatType, int> _stats;

        private UnitStatsContainer() { }

        public static UnitStatsContainer CreateEmpty()
        {
            return new UnitStatsContainer
            {
                _stats = new Dictionary<StatType, int>()
            };
        }

        public static UnitStatsContainer CreateFromConfig(UnitStatsConfig config)
        {
            return new UnitStatsContainer
            {
                _stats = config != null ? config.ToDictionary(x => x.StatType, x => x.Value) : new Dictionary<StatType, int>()
            };
        }

        public int Get(StatType statType)
        {
            if (!_stats.ContainsKey(statType)) return 0;
            return _stats[statType];
        }

        public void SetDelta(StatType statType, int delta)
        {
            if (!_stats.ContainsKey(statType))
            {
                _stats.Add(statType, delta);
                return;
            }

            _stats[statType] += delta;
        }
    }
}