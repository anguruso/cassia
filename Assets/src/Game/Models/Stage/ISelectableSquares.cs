﻿using System.Collections.Generic;
using Common.DataContainers;

namespace Game.Models.Stage
{
    public interface ISelectableSquares
    {
        IEnumerable<GridCoords> Get();
        void Set(IEnumerable<GridCoords> squares);
        void Clear();
    }
}