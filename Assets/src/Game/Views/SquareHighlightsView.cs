﻿using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;
using Vectrosity;

namespace Game.Views
{
	public class SquareHighlightsView : View
	{
		private readonly List<GameObject> _highlights = new List<GameObject>();

		public void HighlightSquares(IEnumerable<Vector3> positions)
		{
			foreach (var position in positions)
			{
                var highlight = CreateHighlight(position);
                _highlights.Add(highlight);
			}
		}

		private GameObject CreateHighlight(Vector3 position)
		{
			var widthOffset = Vector3.left * 0.5f;
			var depthOffset = Vector3.back * 0.5f;
			var linePoints = new List<Vector3>
			{
				position - widthOffset - depthOffset,
				position - widthOffset + depthOffset,
				position + widthOffset + depthOffset,
				position + widthOffset - depthOffset,
				position - widthOffset - depthOffset
			};
			var line = new VectorLine("Highlight", linePoints, 4.0f, LineType.Continuous);
			line.color = Color.red;
			line.Draw3DAuto();
			return line.rectTransform.gameObject;
		}

		public void UnhighlightAllSquares()
		{
			foreach (var highlight in _highlights)
			{
				GameObject.Destroy(highlight);
			}
		}
	}
}
