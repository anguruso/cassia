using System;
using System.Collections.Generic;
using Common.DataContainers;
using Config;
using Game.Controllers.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Signals.ViewSignals;

namespace Game.Models.Occupants.Units
{
	public static class EmptyUnit
	{
		public static readonly IUnit None = new NullUnit();

		public class NullUnit : IUnit
		{
			public IMoveUnitViewSignal MoveUnitViewSignal
			{
				get { return null; }
			}

			public ISetUnitPositionViewSignal SetUnitPositionViewSignal
			{
				get { return null; }
			}

			public string Id { get { return "Null"; } }
		    public int Jump { get { return 0; } }
			public int Move { get { return 0; } }
		    public IUnitStats Stats { get { return new UnitStats(); } }

		    public IList<GridCoords> GetPathTo(GridCoords targetCoords)
			{
				throw new NotImplementedException("Cannot get path for NullUnit.");
			}

			public IList<GridCoords> GetMovementRange()
			{
				return new List<GridCoords>();
			}

		    public IUnitAction GetUnitAction(int index)
		    {
		        throw new NotImplementedException("Cannot get UnitAction for NullUnit.");
		    }

		    public IList<IUnitAction> EquippedUnitActions
		    {
		        get { return new List<IUnitAction>(); }
		    }

			public Facing Facing { get; set; }

			public void ApplyUnitActionEffect(IUnitActionEffect unitActionEffect, IUnitStats actorStats)
		    {
		        throw new NotImplementedException("Cannot apply unitActionEffectBuilder for NullUnit");
		    }

		    public string Name
			{
				get { return "Null"; }
				set { }
			}

			public void LoadConfig(UnitConfig config)
			{
				throw new NotImplementedException("Cannot load config for NullUnit.");
			}

			public Affiliation Affiliation { get; set; }
			public bool AllowsPassingBy(IOccupant other)
			{
				return true;
			}

			public bool IsSelectable {
				get { return false; }
			}

			public void Select()
			{
				throw new NotImplementedException("Cannot select NullUnit.");
			}
		}
	}
}