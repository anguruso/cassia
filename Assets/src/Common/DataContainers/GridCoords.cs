﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Config;

namespace Common.DataContainers
{
    public struct GridCoords
    {
        public GridCoords(int row, int column)
        {
            Row = row;
            Column = column;
        }

		public static GridCoords CreateFromList(IList<int> ints)
		{
			if (ints == null)
			{
				throw new ArgumentNullException("ints");
			}

			if (ints.Count != 2)
			{
				var message = string.Format("Can only create a GridCoords with a list of size 2.  List of size {0} was provided.", ints.Count);
				throw new ArgumentException(message);
			}

			return new GridCoords(ints[0], ints[1]);
		}

		public int Row { get; private set; }
		public int Column { get; private set; }

		public override string ToString()
		{
			return string.Format("GridCoords({0}, {1})", Row, Column);
		}

		public override int GetHashCode()
		{
			var hash = 19;
			hash += (hash * 23) + Row.GetHashCode();
			hash += (hash * 23) + Column.GetHashCode();;
			return hash;
		}

		public override bool Equals(Object obj)
		{
			return obj is GridCoords && this == (GridCoords)obj;
		}

		public static bool operator ==(GridCoords first, GridCoords second)
		{
			return first.Row == second.Row && first.Column == second.Column;
		}

		public static bool operator !=(GridCoords first, GridCoords second)
		{
			return !(first == second);
		}

		public static readonly GridCoords Left = new GridCoords(0, -1);
		public static readonly GridCoords Right = new GridCoords(0, 1);
		public static readonly GridCoords Back = new GridCoords(-1, 0);
		public static readonly GridCoords Forward = new GridCoords(1, 0);

        public GridCoords GetOffset(GridCoordsOffset gridCoordsOffset)
        {
            var row = Row + gridCoordsOffset.Row;
            var column = Column + gridCoordsOffset.Column;
            return new GridCoords(row, column);
        }
    }
}
