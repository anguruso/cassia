﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Models.Occupants.Units.UnitActions
{
	public class UnitActionOutcomeFactory : IUnitActionOutcomeFactory
	{
		[Inject]
		public IUnitActionEffectFactory UnitActionEffectFactory { get; set; }

		public IUnitActionOutcome CreateUnitActionOutcome(IUnitStats actorStats, IUnit affectedUnit, IEnumerable<IUnitActionEffectBuilder> effectBuilders)
		{
			if (actorStats == null)
				throw new ArgumentNullException("actorStats");

			if (affectedUnit == null)
				throw new ArgumentNullException("affectedUnit");

			effectBuilders = effectBuilders ?? new List<IUnitActionEffectBuilder>();
			var effects =
				effectBuilders.Select(x => UnitActionEffectFactory.CreateUnitActionEffect(x, actorStats, affectedUnit.Stats));
			return new UnitActionOutcome
			{
				AffectedUnit = affectedUnit,
				Effects = effects
			};
		}

	}
}