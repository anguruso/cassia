﻿using Common.Signals;
using Game.Controllers.Units;
using strange.extensions.signal.impl;

namespace Game.Signals.ViewSignals
{
    public class UpdateGameUIViewSignal : Signal, IUpdateGameUIViewSignal
    {
    }

    public interface IUpdateGameUIViewSignal : ISignal
    {
    }
}
