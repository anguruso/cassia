﻿using Common.DataContainers;
using Game.Models.Occupants.Units;

namespace Instantiator.Units
{
    public class InstantiatableUnitView : IInstantiatableUnitView
    {
        public IUnit Unit { get; internal set; }
        public string PrefabPath { get; internal set; }
        public GridPosition StartPosition { get; set; }
    }
}