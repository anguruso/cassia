﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Config;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.Pathing;
using Game.Signals.ControllerSignals;
using Game.Signals.ViewSignals;

namespace Game.Models.Occupants.Units
{
	public class Unit : Occupant, IInjectableUnit
	{
	    private UnitConfig _config;
	    private List<IUnitAction> _equippedUnitActions;

		[Inject]
		public IPathfinder Pathfinder { get; set; }

        [Inject]
	    public IUnitActionLibrary UnitActionLibrary { get; set; }

        [Inject]
	    public IMoveUnitViewSignal MoveUnitViewSignal { get; set; }

		[Inject]
		public ISetUnitPositionViewSignal SetUnitPositionViewSignal { get; set; }

        [Inject]
        public IUnitStats Stats { get; set; }

		public void LoadConfig(UnitConfig config)
        {
            _config = config;

            _equippedUnitActions = new List<IUnitAction>();
            foreach (var unitActionId in config.EquippedActionIds)
            {
                _equippedUnitActions.Add(UnitActionLibrary.GetUnitAction(unitActionId));
            }

            Stats.LoadConfig(config.Stats);
        }

		public string Name { get { return _config != null ? _config.Name : "Unnamed" ; } }
		public int Move { get { return Stats.Get(StatType.Move); } }
	    public int Jump { get { return Stats.Get(StatType.Jump); } }
		public override bool IsSelectable { get { return true; } }

	    public IList<IUnitAction> EquippedUnitActions
	    {
	        get { return _equippedUnitActions.ToList(); }
	    }


		public IList<GridCoords> GetPathTo(GridCoords targetCoords)
		{
			return Pathfinder.GetPathTo(this, targetCoords).ToList();
		}

		public IList<GridCoords> GetMovementRange()
		{
			return Pathfinder.GetMovementRange(this).ToList();
		}
	    public override string ToString()
	    {
	        return string.Format("Unit({0})", Name);
	    }

	    public IUnitAction GetUnitAction(int unitActionIndex)
	    {
	        if (unitActionIndex < 0 || unitActionIndex >= _equippedUnitActions.Count)
	        {
	            var message = string.Format("No UnitAction with index {0} for {1}.", unitActionIndex, this);
                throw new ArgumentException(message);
	        }

	        return _equippedUnitActions[unitActionIndex];
	    }

	    public void ApplyUnitActionEffect(IUnitActionEffect unitActionEffect, IUnitStats actorStats)
	    {
	        if (UnityEngine.Random.value <= unitActionEffect.Accuracy)
	        {
                var modifier = StatsModifier.Create(unitActionEffect, actorStats, Stats);
                Stats.ApplyModifier(modifier);
            }
	    }
	}
}
