﻿using System.Collections.Generic;
using Common.DataContainers;
using Config;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Occupants.Units.UnitActions
{
    [TestFixture]
    public class UnitActionTest
    {
        [Test]
        public void LoadConfig()
        {
            var unitActionConfig = new UnitActionConfig
            {
                Name = "TestAttack",
                Description = "Test Description",
                TargetRange = new List<GridCoordsOffset> { new GridCoordsOffset(0, 1) },
                AreaOfEffect = new List<GridCoordsOffset> { new GridCoordsOffset(1, 2) }
            };

            var unitAction = new UnitAction();
            unitAction.LoadConfig(unitActionConfig);

            Assert.AreEqual("TestAttack", unitAction.Name);
            Assert.AreEqual("Test Description", unitAction.Description);
            CollectionAssert.AreEqual(new List<GridCoordsOffset> { new GridCoordsOffset(0, 1) }, unitAction.TargetRange);
            CollectionAssert.AreEqual(new List<GridCoordsOffset> { new GridCoordsOffset(1, 2) }, unitAction.EffectiveRange);
        }

        [Test]
        public void GetEffects()
        {
            var unitActionConfig = new UnitActionConfig
            {
                Id = "TestAttack",
                TargetEffects = new List<UnitActionEffectConfig>(),
                ActorEffects = new List<UnitActionEffectConfig>()
            };

            var unitAction = new UnitAction();
            unitAction.LoadConfig(unitActionConfig);
            unitAction.EffectBuildersFactory = Substitute.For<IUnitActionEffectBuildersFactory>();

            var testStats = new UnitStats();

            var expectedTargetEffects = new List<IUnitActionEffectBuilder>();
            unitAction.EffectBuildersFactory.GetEffectBuilders("TestAttack", unitActionConfig.TargetEffects).Returns(expectedTargetEffects);

            var expectedSelfEffects = new List<IUnitActionEffectBuilder>();
            unitAction.EffectBuildersFactory.GetEffectBuilders("TestAttack", unitActionConfig.ActorEffects).Returns(expectedSelfEffects);

            Assert.AreEqual(expectedTargetEffects, unitAction.TargetEffectBuilders);
            Assert.AreEqual(expectedSelfEffects, unitAction.SelfEffectBuilders);
        }

        [Test]
        public void GetRotatedRange()
        {
            var unitActionConfig = new UnitActionConfig
            {
                TargetRange = new List<GridCoordsOffset>()
            };

            var unitAction = new UnitAction();
            unitAction.LoadConfig(unitActionConfig);
            unitAction.RangeCalculator = Substitute.For<IUnitActionRangeCalculator>();

            var expectedTargetRange = new List<GridCoords> { new GridCoords(1, 2), new GridCoords(3, 4) };
            unitAction.RangeCalculator.GetRotatedRange(new GridCoords(1, 1), unitActionConfig.TargetRange).Returns(expectedTargetRange);

            CollectionAssert.AreEqual(expectedTargetRange, unitAction.GetTargetRange(new GridCoords(1, 1)));
        }

        [Test]
        public void GetAreaOfEffect()
        {
            var unitActionConfig = new UnitActionConfig
            {
                AreaOfEffect = new List<GridCoordsOffset>()
            };

            var unitAction = new UnitAction();
            unitAction.LoadConfig(unitActionConfig);
            unitAction.RangeCalculator = Substitute.For<IUnitActionRangeCalculator>();

            var expectedAreaOfEffect = new List<GridCoords> { new GridCoords(1, 2), new GridCoords(3, 4) };
            unitAction.RangeCalculator.GetRange(new GridCoords(1, 1), Facing.Left, unitActionConfig.AreaOfEffect).Returns(expectedAreaOfEffect);

            CollectionAssert.AreEqual(expectedAreaOfEffect, unitAction.GetAreaOfEffect(new GridCoords(1, 1), Facing.Left));
        }
    }
}
