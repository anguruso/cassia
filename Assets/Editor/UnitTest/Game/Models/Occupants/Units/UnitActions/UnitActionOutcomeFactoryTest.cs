﻿using System;
using System.Collections.Generic;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Occupants.Units.UnitActions
{
    [TestFixture]
    public class UnitActionOutcomeFactoryTest
    {
	    private UnitActionOutcomeFactory _factory;

	    [SetUp]
	    public void SetUp()
	    {
		    _factory = new UnitActionOutcomeFactory
		    {
				UnitActionEffectFactory = Substitute.For<IUnitActionEffectFactory>()
		    };
	    }

        [Test]
        public void Create()
        {
            var actorStats = Substitute.For<IUnitStats>();
	        var affectedUnit = Substitute.For<IUnit>();

            var effectBuilders = new List<IUnitActionEffectBuilder> { Substitute.For<IUnitActionEffectBuilder>(), Substitute.For<IUnitActionEffectBuilder>() };
	        var effects = new List<IUnitActionEffect>
	        {
		        Substitute.For<IUnitActionEffect>(),
		        Substitute.For<IUnitActionEffect>()
	        };

	        _factory.UnitActionEffectFactory.CreateUnitActionEffect(effectBuilders[0], actorStats, affectedUnit.Stats).Returns(effects[0]);
			_factory.UnitActionEffectFactory.CreateUnitActionEffect(effectBuilders[1], actorStats, affectedUnit.Stats).Returns(effects[1]);

			var outcome = _factory.CreateUnitActionOutcome(actorStats, affectedUnit, effectBuilders);

            Assert.AreEqual(affectedUnit, outcome.AffectedUnit);
            CollectionAssert.AreEqual(effectBuilders, effectBuilders);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateWithNullAffectedUnit()
        {
			_factory.CreateUnitActionOutcome(Substitute.For<IUnitStats>(), null, new List<IUnitActionEffectBuilder>());
        }

	    [Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreateWithNullActorStats()
	    {
		    _factory.CreateUnitActionOutcome(null, Substitute.For<IUnit>(), new List<IUnitActionEffectBuilder>());
	    }

        [Test]
        public void CreateWithNullEffects()
        {
	        var actorStats = Substitute.For<IUnitStats>();
            var affectedUnit = Substitute.For<IUnit>();

            var outcome = _factory.CreateUnitActionOutcome(actorStats, affectedUnit, null);

            Assert.AreEqual(affectedUnit, outcome.AffectedUnit);
            CollectionAssert.IsEmpty(outcome.Effects);
        }
    }
}
