﻿using Common.DataContainers;
using Game.Commands;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitTurnTransactions;
using Game.Signals.CommandSignals;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Commands
{
    [TestFixture]
    public class SelectUnitActionTargetCommandTest
    {
        private SelectUnitActionTargetCommand _command;

        [SetUp]
        public void SetUp()
        {
            _command = new SelectUnitActionTargetCommand();
            _command.UnitTurnTransaction = Substitute.For<IUnitTurnTransaction>();
        }

        [Test]
        public void Execute()
        {
            _command.TargetPosition = new GridCoords(4, 5);

            _command.Execute();

            _command.UnitTurnTransaction.Received(1).SelectTargetPosition(new GridCoords(4, 5));
        }
    }
}
