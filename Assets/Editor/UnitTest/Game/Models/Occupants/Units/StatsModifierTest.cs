﻿using System;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Occupants.Units
{
    [TestFixture]
    public class StatsModifierTest
    {
        [Test]
        public void Create()
        {
	        var actorStats = Substitute.For<IUnitStats>();
	        var targetStats = Substitute.For<IUnitStats>();

            var unitActionEffect = Substitute.For<IUnitActionEffect>();
            unitActionEffect.UnitActionId.Returns("TestUnitAction");
            unitActionEffect.StatType.Returns(StatType.Power);
	        unitActionEffect.Delta.Returns(-5);
			unitActionEffect.Duration.Returns(3);
            unitActionEffect.AbnormalStatus.Returns(AbnormalStatusType.Blind);
            unitActionEffect.IsStackable.Returns(true);
            unitActionEffect.IsRemoveable.Returns(true);

            var modifier = StatsModifier.Create(unitActionEffect, actorStats, targetStats);

            Assert.AreEqual("TestUnitAction", modifier.UnitActionId);
            Assert.AreEqual(StatType.Power, modifier.StatType);
			Assert.AreEqual(-5, modifier.Delta);
			Assert.AreEqual(3, modifier.Duration);
            Assert.AreEqual(AbnormalStatusType.Blind, modifier.AbnormalStatus);
            Assert.IsTrue(modifier.IsStackable);
            Assert.IsTrue(modifier.IsRemoveable);
        }

        [Test]
        public void CombineStackable()
        {
	        var actorStats = Substitute.For<IUnitStats>();
	        var targetStats = Substitute.For<IUnitStats>();

            var unitActionEffect = Substitute.For<IUnitActionEffect>();
            unitActionEffect.StatType.Returns(StatType.Power);
	        unitActionEffect.Delta.Returns(3);
			unitActionEffect.IsStackable.Returns(true);
            unitActionEffect.Duration.Returns(3);

            var modifier = StatsModifier.Create(unitActionEffect, actorStats, targetStats);

            var stackedModifier = Substitute.For<IStatsModifier>();
            stackedModifier.StatType.Returns(StatType.Power);
            stackedModifier.Delta.Returns(3);
            stackedModifier.IsStackable.Returns(true);
            stackedModifier.Duration.Returns(1);

            modifier.Combine(stackedModifier);

            Assert.AreEqual(StatType.Power, modifier.StatType);
            Assert.AreEqual(6, modifier.Delta);
            Assert.AreEqual(1, modifier.Duration);
            Assert.IsTrue(modifier.IsStackable);
        }

        [Test]
        public void CombineNonStackable()
        {
	        var actorStats = Substitute.For<IUnitStats>();
			var targetStats = Substitute.For<IUnitStats>();

			var unitActionEffect = Substitute.For<IUnitActionEffect>();
            unitActionEffect.StatType.Returns(StatType.Power);
	        unitActionEffect.Delta.Returns(3);
			unitActionEffect.IsStackable.Returns(false);
            unitActionEffect.Duration.Returns(3);

            var modifier = StatsModifier.Create(unitActionEffect, actorStats, targetStats);

            var stackedModifier = Substitute.For<IStatsModifier>();
            stackedModifier.StatType.Returns(StatType.Power);
			stackedModifier.Delta.Returns(3);
			stackedModifier.IsStackable.Returns(false);
            stackedModifier.Duration.Returns(1);

            modifier.Combine(stackedModifier);

            Assert.AreEqual(StatType.Power, modifier.StatType);
			Assert.AreEqual(3, modifier.Delta);
			Assert.AreEqual(1, modifier.Duration);
            Assert.IsFalse(modifier.IsStackable);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException), ExpectedMessage = "Cannot combine StatsModifier with type 'Power' with one of type 'Defense'.")]
        public void CombineDifferentStatTypes()
        {
            var unitActionEffect = Substitute.For<IUnitActionEffect>();
            unitActionEffect.StatType.Returns(StatType.Power);

            var modifier = StatsModifier.Create(unitActionEffect, null, null);

            var stackedModifier = Substitute.For<IStatsModifier>();
            stackedModifier.StatType.Returns(StatType.Defense);

            modifier.Combine(stackedModifier);
        }

        [Test]
        public void ReduceDuration()
        {
            var unitActionEffect = Substitute.For<IUnitActionEffect>();
            unitActionEffect.Duration.Returns(3);

            var modifier = StatsModifier.Create(unitActionEffect, null, null);
            modifier.ReduceDuration();

            Assert.AreEqual(2, modifier.Duration);
        }

        [Test]
        public void ReduceDurationForNoDuration()
        {
            var unitActionEffect = Substitute.For<IUnitActionEffect>();
            unitActionEffect.Duration.Returns(default(int?));

            var modifier = StatsModifier.Create(unitActionEffect, null, null);
            modifier.ReduceDuration();

            Assert.IsNull(modifier.Duration);
        }
    }
}
