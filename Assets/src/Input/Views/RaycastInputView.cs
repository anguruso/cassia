﻿using System;
using Game.Views;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Input.Views
{
	[RequireComponent(typeof(Camera))]
	public class RaycastInputView : View
	{
		private Camera _camera;

		protected override void Start()
		{
			_camera = GetComponent<Camera>();
		}

		private void Update()
		{
			if (UnityEngine.Input.GetMouseButtonDown(0))
			{
				var ray = _camera.ScreenPointToRay(UnityEngine.Input.mousePosition);

				RaycastHit hit; 
				if (Physics.Raycast(ray, out hit, float.MaxValue))
				{
					var clickable = hit.collider.GetComponent<IClickable>();

					if (clickable == null)
					{
						throw new MissingComponentException("Monobehaviour that extends IClickable not found on clicked object.");
					}

					clickable.OnClick(Click.CreateFromRaycastHit(hit));
				}
			}
		}
	}
}
