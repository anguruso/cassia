﻿using System;
using Game.Controllers;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitTurnTransactions;
using Game.Models.Stage;
using strange.extensions.command.impl;

namespace Game.Commands
{
    public class ApplyUnitTurnTransactionCommand : Command
    {
        [Inject]
        public IUnitTurnTransaction UnitTurnTransaction { get; set; }

        [Inject]
        public IMap Map { get; set; }

        public override void Execute()
        {
	        if (!UnitTurnTransaction.IsReady)
	        {
		        var message = "Cannot apply UnitTurnTransaction if it is not ready.";
				throw new ArgumentException(message);
	        }

            Map.SetOccupant(UnitTurnTransaction.Actor, UnitTurnTransaction.MovePosition);
	        UnitTurnTransaction.Actor.Facing = UnitTurnTransaction.Facing;

            foreach (var outcome in UnitTurnTransaction.Outcome.UnitActionOutcomes)
            {
                foreach (var effect in outcome.Effects)
                {
                    outcome.AffectedUnit.ApplyUnitActionEffect(effect, UnitTurnTransaction.Actor.Stats);
                }
            }
        }
    }
}
