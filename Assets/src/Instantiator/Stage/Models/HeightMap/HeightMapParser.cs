﻿using System;
using Common.DataContainers;

namespace Instantiator.Stage.Models.HeightMap
{
    public class HeightMapParser
    {
        public static int BaseTileCount = 2;

        private HeightMapParser() { }

        public static HeightMapParser Create()
        {
            return new HeightMapParser();
        }

        public HeightMapGrid Parse(int[,] heightMap)
        {
            if (heightMap == null)
            {
                throw new ArgumentNullException("heightMap");
            }

            var width = heightMap.GetLength(1);
            var depth = heightMap.GetLength(0);
            var heightMapGrid = HeightMapGrid.CreateWithDimensions(width, depth);

            for (var column = 0; column < width; ++column)
            {
                for (var row = 0; row < depth; ++row)
                {
                    var height = heightMap[row, column];
                    heightMapGrid.AddSquareWithHeight(height, new GridCoords(row, column));
                }
            }

            return heightMapGrid;
        }
    }
}
