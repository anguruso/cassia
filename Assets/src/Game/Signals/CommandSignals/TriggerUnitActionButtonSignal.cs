using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.CommandSignals
{
    public class TriggerUnitActionButtonSignal : Signal<int>, ITriggerUnitActionButtonSignal
    {
    }

    public interface ITriggerUnitActionButtonSignal : ISignal<int>
    {
    }
}