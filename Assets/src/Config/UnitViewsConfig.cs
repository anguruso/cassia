﻿using System.Collections.Generic;

namespace Config
{
    public class UnitViewsConfig
    {
        public Dictionary<string, UnitViewConfig> UnitViews { get; set; }
    }
}