﻿using Common.DataContainers;
using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.CommandSignals
{
	public class ClickMapSignal : Signal<GridCoords>, IClickMapSignal
	{
	}

    public interface IClickMapSignal : ISignal<GridCoords>
    {
    }
}
