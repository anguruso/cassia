﻿using System.Collections;
using Common.DataContainers;
using Game.Signals.ViewSignals;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Game.Controllers;
using Game.Models.Stage;
using NSubstitute;

namespace UnitTest.Game.Controllers
{
    [TestFixture]
    public class SelectableSquaresControllerTest
    {
        private SelectableSquaresController _selectableSquaresController;

        [SetUp]
        public void SetUp()
        {
            _selectableSquaresController = new SelectableSquaresController();
            _selectableSquaresController.ShowMovementRangeViewSignal = Substitute.For<IShowMovementRangeViewSignal>();
            _selectableSquaresController.HideMovementRangeViewSignal = Substitute.For<IHideMovementRangeViewSignal>();
            _selectableSquaresController.SelectableSquares = Substitute.For<ISelectableSquares>();
        }

        [Test]
        public void SetSelectableSquares()
        {
            var highlightedSquares = new List<GridCoords> {new GridCoords(1, 2), new GridCoords(3, 4)};
            _selectableSquaresController.Set(highlightedSquares);
            
            _selectableSquaresController.HideMovementRangeViewSignal.Received(1).Dispatch();
            _selectableSquaresController.ShowMovementRangeViewSignal.Received(1).Dispatch(Arg.Is<IEnumerable<GridCoords>>(x => x.SequenceEqual(highlightedSquares)));
            _selectableSquaresController.SelectableSquares.Received(1).Set(Arg.Is<IEnumerable<GridCoords>>(x => x.SequenceEqual(highlightedSquares)));
        }

        [Test]
        public void ClearSelectableSquares()
        {
            _selectableSquaresController.Clear();

            _selectableSquaresController.HideMovementRangeViewSignal.Received(1).Dispatch();
            _selectableSquaresController.SelectableSquares.Received(1).Clear();
        }

        // TODO do i need to add? i dont think so
    }
}
