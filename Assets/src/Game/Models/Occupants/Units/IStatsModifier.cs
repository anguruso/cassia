﻿using System;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.Occupants.Units
{
    public interface IStatsModifier
    {
        string UnitActionId { get; }
        AbnormalStatusType? AbnormalStatus { get; }
        int Delta { get; }
        int? Duration { get; }
        bool IsStackable { get; }
        StatType StatType { get; }
        bool IsRemoveable { get; }
        void Combine(IStatsModifier modifier);
        void ReduceDuration();
    }
}