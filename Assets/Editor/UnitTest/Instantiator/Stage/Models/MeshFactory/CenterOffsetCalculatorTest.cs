﻿using System;
using Instantiator.Stage.Models.MeshFactory;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Builder.Stage.Models.MeshFactory
{
    [TestFixture]
    public class CenterOffsetCalculatorTest
    {
        CenterOffsetCalculator _calculator;

        [SetUp]
        public void SetUp()
        {
            var chunkSize = 1f;
            _calculator = CenterOffsetCalculator.CreateWithDimensions(chunkSize);
		}

		[Test]
        public void CalculateCenterOffsetOfOneChunk()
		{
		    var heights = new int[,] {{1}};
            var offset = _calculator.GetCenterOffset(heights);

            Assert.AreEqual(new Vector3(-0.5f, 0, -0.5f), offset);
        }

        [Test]
        public void CalculateCenterOffsetOfMultipleChunks()
        {
            var heights = new int[,]
            {
                { 1, 2, 5 },
                { 0, 1, 0 }
            };
            var offset = _calculator.GetCenterOffset(heights);

            Assert.AreEqual(new Vector3(-1.5f, 0, -1f), offset);
        }

        [Test]
        public void CalculateCenterOffsetOfNoChunks()
        {
            var heights = new int[,] {};
            var offset = _calculator.GetCenterOffset(heights);

            Assert.AreEqual(new Vector3(0, 0, 0), offset);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CalculateCenterOffsetOfNull()
        {
            _calculator.GetCenterOffset(null);
        }
    }
}
