﻿using Config;

namespace Instantiator.Units
{
    public interface IInstantiatableUnitViewFactory
    {
        IInstantiatableUnitView Build(LevelUnitConfig config, UnitViewConfig viewConfig);
    }
}