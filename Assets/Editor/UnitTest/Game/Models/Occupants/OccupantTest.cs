﻿using Game.Models.Occupants;
using NUnit.Framework;

namespace UnitTest.Game.Models.Occupants
{
	public class OccupantTest
	{
		[Test]
		public void IsSelectable()
		{
			var occupant = new BlockingOccupant();
			Assert.IsFalse(occupant.IsSelectable);
		}
	}
}