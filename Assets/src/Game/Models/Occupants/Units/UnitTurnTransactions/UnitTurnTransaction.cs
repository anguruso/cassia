﻿using Common.DataContainers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.Occupants.Units.UnitTurnTransactions
{
    public class UnitTurnTransaction : IUnitTurnTransaction
    {
        [Inject]
        public IUnitTurnTransactionOutcomeFactory UnitTurnTransactionOutcomeFactory { get; set; }

		[Inject]
		public IUnitTurnTransactionStateMachine StateMachine { get; set; }

		[PostConstruct]
	    public void PostConstruct()
	    {
		    StateMachine.Init(UnitTurnTransactionState.Nothing);
	    }

		public IUnit Actor { get; private set; }

		public GridCoords StartPosition { get; private set; }

		public GridCoords MovePosition { get; private set; }

        public IUnitAction UnitAction { get; private set; }

	    public Facing Facing { get; private set; }

        public IUnitTurnTransactionOutcome Outcome { get; private set; }

        public bool IsReady
        {
            get { return Outcome != null; }
        }

	    public UnitTurnTransactionState CurrentState
	    {
		    get { return StateMachine.CurrentState; }
	    }


	    public void SelectActor(IUnit unit, GridCoords startPosition)
        {
            Actor = unit;
		    StartPosition = startPosition;
        }

		public void SelectMovePosition(GridCoords movePosition)
        {
            MovePosition = movePosition;
        }

        public void SelectUnitAction(IUnitAction unitAction)
        {
            UnitAction = unitAction;
        }

        public void SelectTargetPosition(GridCoords targetPosition)
        {
            var unitActionOrigin = UnitActionOrigin.CreateFromActorAndPosition(Actor, targetPosition);
            Outcome = UnitTurnTransactionOutcomeFactory.GetOutcome(unitActionOrigin, UnitAction, targetPosition);
        }

		public void SelectFacing(Facing facing)
		{
			Facing = facing;
		}

		public void Clear()
        {
			Actor = null;
			UnitAction = null;
			Outcome = null;
        }
    }
}
