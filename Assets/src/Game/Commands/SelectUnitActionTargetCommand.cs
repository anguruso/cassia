﻿using Common.DataContainers;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitTurnTransactions;
using Game.Signals.CommandSignals;
using strange.extensions.command.impl;

namespace Game.Commands
{
    public class SelectUnitActionTargetCommand : Command
    {
        [Inject]
        public IUnitTurnTransaction UnitTurnTransaction { get; set; }

        [Inject]
        public GridCoords TargetPosition { get; set; }

        public override void Execute()
        {
            UnitTurnTransaction.SelectTargetPosition(TargetPosition);
        }
    }
}
