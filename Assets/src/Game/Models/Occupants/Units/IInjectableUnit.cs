﻿using Config;

namespace Game.Models.Occupants.Units
{
    public interface IInjectableUnit : IUnit
    {
    }
}