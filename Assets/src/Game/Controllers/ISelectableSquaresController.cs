﻿using System.Collections.Generic;
using Common.DataContainers;

namespace Game.Controllers
{
    public interface ISelectableSquaresController
    {
        void Clear();
        void Set(IEnumerable<GridCoords> highlightedSquares);
        IEnumerable<GridCoords> Get();
    }
}