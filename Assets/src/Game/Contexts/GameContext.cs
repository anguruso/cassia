﻿using Game.Commands;
using Game.Controllers;
using Game.Controllers.UI;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.Occupants.Units.UnitTurnTransactions;
using Game.Models.Pathing;
using Game.Models.Stage;
using Game.Signals.CommandSignals;
using Game.Signals.ControllerSignals;
using Game.Signals.ViewSignals;
using Game.Views;
using Game.Views.GameUI;
using Input.Main.Contexts;
using Input.Views;
using Instantiator.Stage.Signals.CommandSignals;
using Instantiator.Stage.Signals.ViewSignals;
using Instantiator.Stage.Views;
using Instantiator.Units;
using Instantiator.Units.Commands;
using strange.extensions.injector.impl;
using UnityEngine;

namespace Game.Contexts
{
	public class GameContext : SignalContext
	{
		public GameContext(MonoBehaviour view) : base(view)
		{

		}

		protected override void mapBindings()
		{
		    injectionBinder.Bind<IUnitFactory>().To<UnitFactory>();
		    injectionBinder.Bind<IInstantiatableUnitViewFactory>().To<InstantiatableUnitViewFactory>();
		    injectionBinder.Bind<IUnitActionFactory>().To<UnitActionFactory>();

			injectionBinder.Bind<IPathfinder>().To<Pathfinder>();
		    injectionBinder.Bind<IInjectableUnit>().To<Unit>();
		    injectionBinder.Bind<IInjectableUnitAction>().To<UnitAction>();
		    injectionBinder.Bind<IUnitStats>().To<UnitStats>();
			injectionBinder.Bind<IUnitTurnTransactionStateMachine>().To<UnitTurnTransactionStateMachine>();
		    injectionBinder.Bind<IUnitTurnTransaction>().To<UnitTurnTransaction>().ToSingleton();

		    injectionBinder.Bind<IUnitActionLibrary>().To<UnitActionLibrary>().ToSingleton();
			injectionBinder.Bind<IUnitActionEffectFactory>().To<UnitActionEffectFactory>().ToSingleton();
			injectionBinder.Bind<IUnitActionOutcomeFactory>().To<UnitActionOutcomeFactory>().ToSingleton();
		    injectionBinder.Bind<IUnitActionRangeCalculator>().To<UnitActionRangeCalculator>().ToSingleton();
		    injectionBinder.Bind<IUnitActionEffectBuildersFactory>().To<UnitActionEffectBuildersFactory>().ToSingleton();
		    injectionBinder.Bind<IDirectionCalculator>().To<DirectionCalculator>().ToSingleton();
		    injectionBinder.Bind<IUnitTurnTransactionOutcomeFactory>().To<UnitTurnTransactionOutcomeFactory>().ToSingleton();
		    injectionBinder.Bind<IUnitLookup>().To<UnitLookup>().ToSingleton();

			injectionBinder.Bind<IMap>().To<Map>().ToSingleton();
			injectionBinder.Bind<IWorldGridCoordsConverter>().To<WorldGridCoordsConverter>().ToSingleton();
		    injectionBinder.Bind<ISelectableSquares>().To<SelectableSquares>().ToSingleton();
		    injectionBinder.Bind<ISelectableSquaresController>().To<SelectableSquaresController>().ToSingleton();

            BindInputSignals();
		    BindViewSignals();
		    BindCommandSignals();
		    BindViews();
		}

	    private void BindInputSignals()
	    {
            // these signals are player input signals. maybe call them input signals?
            injectionBinder.Bind<IClickMapSignal>().To<ClickMapSignal>().ToSingleton();
            commandBinder.Bind<IClickMapSignal>().To<ClickMapCommand>().Pooled();

            injectionBinder.Bind<ITriggerUnitSignal>().To<TriggerUnitSignal>().ToSingleton();
            injectionBinder.Bind<ITriggerMapSignal>().To<TriggerMapSignal>().ToSingleton();
            injectionBinder.Bind<IMoveUnitViewCompleteSignal>().To<MoveUnitViewCompleteSignal>().ToSingleton();
	        injectionBinder.Bind<ITriggerUnitActionButtonSignal>().To<TriggerUnitActionButtonSignal>().ToSingleton();
	        injectionBinder.Bind<ITriggerApplyTransactionButtonSignal>().To<TriggerApplyTransactionButtonSignal>().ToSingleton();
			injectionBinder.Bind<ITriggerCancelTransactionButtonSignal>().To<TriggerCancelTransactionButtonSignal>().ToSingleton();
	    }

	    private void BindViewSignals()
	    {
            injectionBinder.Bind<IInstantiateUnitViewSignal>().To<InstantiateUnitViewSignal>().ToSingleton();
            injectionBinder.Bind<IInstantiateMapViewSignal>().To<InstantiateMapViewSignal>().ToSingleton();
            injectionBinder.Bind<IShowMovementRangeViewSignal>().To<ShowMovementRangeViewSignal>().ToSingleton();
            injectionBinder.Bind<IHideMovementRangeViewSignal>().To<HideMovementRangeViewSignal>().ToSingleton();
	        injectionBinder.Bind<IUpdateGameUIViewSignal>().To<UpdateGameUIViewSignal>().ToSingleton();
	        injectionBinder.Bind<IMoveUnitViewSignal>().To<MoveUnitViewSignal>(); // not singleton, per unit.
		    injectionBinder.Bind<ISetUnitPositionViewSignal>().To<SetUnitPositionViewSignal>(); // not singleton, per unit
	    }

	    private void BindCommandSignals()
	    {
            injectionBinder.Bind<IInstantiateUnitSignal>().To<InstantiateUnitSignal>().ToSingleton();
	        injectionBinder.Bind<IApplyUnitTurnTransactionSignal>().To<ApplyUnitTurnTransactionSignal>().ToSingleton();

	        commandBinder.Bind<IInstantiateUnitSignal>().To<InstantiateUnitCommand>().Pooled();
	        commandBinder.Bind<IApplyUnitTurnTransactionSignal>().To<ApplyUnitTurnTransactionCommand>();
	    }

	    private void BindViews()
	    {
            mediationBinder.Bind<RaycastInputView>().To<RaycastInputMediator>();
            mediationBinder.Bind<GameInstantiatorView>().To<GameInstantiatorMediator>();
            mediationBinder.Bind<MapView>().To<MapMediator>();
            mediationBinder.Bind<SquareHighlightsView>().To<SquareHighlightsMediator>();
            mediationBinder.Bind<UnitView>().To<UnitMediator>();
            mediationBinder.Bind<GameUIView>().To<GameUIMediator>();
        }

		public override void Launch()
		{
            commandBinder.Bind<GameStartupSignal>().To<GameStartupCommand>().Once();
            injectionBinder.GetInstance<GameStartupSignal>().Dispatch();
        }
	}
}
