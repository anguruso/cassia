﻿using Common.DataContainers;
using Game.Commands;
using Game.Controllers.Units;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using Game.Signals;
using Game.Signals.ControllerSignals;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Commands
{
	[TestFixture]
	public class ClickMapCommandTest
	{
		private ClickMapCommand _command;

		[SetUp]
		public void SetUp()
		{
			_command = new ClickMapCommand();

			var map = Substitute.For<IMap>();
			_command.Map = map;
            _command.TriggerMapSignal = Substitute.For<ITriggerMapSignal>();
		    _command.TriggerUnitSignal = Substitute.For<ITriggerUnitSignal>();
		}

		[Test]
		public void ClickUnit()
		{
			var square = Substitute.For<ISquare>();
			var unit = Substitute.For<IUnit>();
			unit.IsSelectable.Returns(true);
			square.Occupant.Returns(unit);
			square.IsOccupied.Returns(true);

			_command.Map.GetSquare(new GridCoords(1, 2)).Returns(square);

			_command.TargetCoords = new GridCoords(1, 2);
			_command.Execute();

            _command.TriggerUnitSignal.Received(1).Dispatch(unit, new GridCoords(1, 2));
			_command.TriggerMapSignal.DidNotReceive().Dispatch(Arg.Any<GridCoords>());
		}

		[Test]
		public void ClickUnselectableUnit()
		{
			var square = Substitute.For<ISquare>();
			var occupant = Substitute.For<IOccupant>();
			occupant.IsSelectable.Returns(false);
			square.Occupant.Returns(occupant);
			square.IsOccupied.Returns(true);

			_command.Map.GetSquare(new GridCoords(1, 2)).Returns(square);

			_command.TargetCoords = new GridCoords(1, 2);
			_command.Execute();

			_command.TriggerMapSignal.Received(1).Dispatch(new GridCoords(1, 2));
			_command.TriggerUnitSignal.DidNotReceive().Dispatch(Arg.Any<IUnit>(), Arg.Any<GridCoords>());
		}
	}
}
