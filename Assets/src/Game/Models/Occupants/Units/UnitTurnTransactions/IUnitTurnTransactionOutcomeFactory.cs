﻿using Common.DataContainers;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.Occupants.Units.UnitTurnTransactions
{
    public interface IUnitTurnTransactionOutcomeFactory
    {
        IUnitTurnTransactionOutcome GetOutcome(IUnitActionOrigin unitActionOrigin, IUnitAction unitAction, GridCoords targetPosition);
    }
}