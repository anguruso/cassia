﻿using System;
using System.Collections.Generic;
using Common.Exceptions;
using Config;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Instantiator.Units;

namespace Game.Controllers
{
    public class UnitActionLibrary : IUnitActionLibrary
    {
        private readonly Dictionary<string, IUnitAction> _unitActions;

        [Inject]
        public IUnitActionFactory UnitActionFactory { get; set; }

        public UnitActionLibrary()
        {
            _unitActions = new Dictionary<string, IUnitAction>();
        }

        public void LoadConfigs(IEnumerable<UnitActionConfig> configs)
        {
            foreach (var config in configs)
            {
                if (_unitActions.ContainsKey(config.Id))
                {
                    var message = string.Format("UnitAction with duplicate id '{0}'.", config.Id);
                    throw new InvalidConfigException(message);
                }

                var unitAction = UnitActionFactory.CreateUnitAction(config);
                _unitActions.Add(config.Id, unitAction);
            }
        }

        public IUnitAction GetUnitAction(string unitActionId)
        {
            if (!_unitActions.ContainsKey(unitActionId))
            {
                var message = string.Format("Could not get UnitAction with id '{0}'.", unitActionId);
                throw new ArgumentException(message);
            }

            return _unitActions[unitActionId];
        }
    }
}
