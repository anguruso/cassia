﻿using Config;
using Game.Models.Occupants.Units;

namespace Instantiator.Units
{
    public interface IUnitFactory
    {
        IUnit CreateUnit(UnitConfig config);
    }
}