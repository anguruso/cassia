﻿using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.ViewSignals
{
	public class HideMovementRangeViewSignal : Signal, IHideMovementRangeViewSignal
	{
		
	}

	public interface IHideMovementRangeViewSignal : ISignal
	{
		
	}
}