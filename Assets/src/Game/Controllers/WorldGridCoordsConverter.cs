﻿using System;
using Common.DataContainers;
using Config;
using Instantiator.Stage.Models.MeshFactory;
using UnityEngine;

namespace Game.Controllers
{
	public class WorldGridCoordsConverter : IWorldGridCoordsConverter
	{
		private float _chunkSize;
		private float _chunkHeight;
		private int[,] _heights;
		private Vector3 _mapOffset;

		public void LoadConfigs(int[,] heights, MapInstantiatorConfig mapInstantiatorConfig)
		{
			_heights = heights;
			_chunkSize = mapInstantiatorConfig.ChunkSize;
			_chunkHeight = mapInstantiatorConfig.ChunkHeight;
			_mapOffset = GetMapOffset(heights, mapInstantiatorConfig);
		}

		private float Width { get { return _heights.GetLength(0); } }
		private float Depth { get { return _heights.GetLength(1); } }

		private static Vector3 GetMapOffset(int[,] heights, MapInstantiatorConfig mapInstantiatorConfig)
		{
			var centerOffsetCalculator = CenterOffsetCalculator.CreateWithDimensions(mapInstantiatorConfig.ChunkSize);
			return centerOffsetCalculator.GetCenterOffset(heights);
		}

		public GridCoords GetGridCoords(Vector3 worldPosition)
		{
			var maxRow = Width - 1;
			var row = Mathf.Clamp((worldPosition.x - _mapOffset.x) / _chunkSize, 0, maxRow);

			var maxColumn = Depth - 1;
			var column = Mathf.Clamp((worldPosition.z - _mapOffset.z) / _chunkSize, 0, maxColumn);

			return new GridCoords(Mathf.FloorToInt(row), Mathf.FloorToInt(column));
		}

		public Vector3 GetWorldPosition(GridCoords gridCoords)
		{
			if (gridCoords.Row < 0 || gridCoords.Row >= Width || gridCoords.Column < 0 || gridCoords.Column >= Depth)
			{
				var message = string.Format("Cannot get world position for {0}.  Map is of size ({1}, {2}).", gridCoords, _heights.GetLength(0), _heights.GetLength(1));
				throw new ArgumentException(message);
			}

			var x = gridCoords.Row*_chunkSize + _mapOffset.x + _chunkSize/2f;
			var y = _heights[gridCoords.Row, gridCoords.Column]*_chunkHeight;
			var z = gridCoords.Column*_chunkSize + _mapOffset.z + _chunkSize/2f;

			return new Vector3(x, y, z);
		}

		public Vector3 GetClosestWorldPositionCenter(Vector3 worldPosition)
		{
			var gridCoords = GetGridCoords(worldPosition);
			return GetWorldPosition(gridCoords);
		}

		public Quaternion GetRotation(Facing facing)
		{
			if (facing == Facing.Forward)
				return Quaternion.Euler(0, 0, 0);
			else if (facing == Facing.Back)
				return Quaternion.Euler(0, 180, 0);
			else if (facing == Facing.Left)
				return Quaternion.Euler(0, 270, 0);
			else if (facing == Facing.Right)
				return Quaternion.Euler(0, 90, 0);

			var message = string.Format("Rotation for {0} is not defined.", facing);
			throw new NotImplementedException(message);
		}
	}
}
