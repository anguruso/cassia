﻿using Common.DataContainers;
using Common.Signals;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using strange.extensions.signal.impl;

namespace Game.Signals.ControllerSignals
{
    public class TriggerUnitSignal : Signal<IUnit, GridCoords>, ITriggerUnitSignal
    {
    }

    public interface ITriggerUnitSignal : ISignal<IUnit, GridCoords>
    {
    }
}
