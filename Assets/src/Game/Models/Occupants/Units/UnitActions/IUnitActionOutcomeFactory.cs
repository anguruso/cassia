﻿using System.Collections.Generic;

namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IUnitActionOutcomeFactory
	{
		IUnitActionOutcome CreateUnitActionOutcome(IUnitStats actorStats, IUnit affectedUnit, IEnumerable<IUnitActionEffectBuilder> effectBuilders);
	}
}