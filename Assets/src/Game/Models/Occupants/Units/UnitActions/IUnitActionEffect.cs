﻿namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IUnitActionEffect
	{
		AbnormalStatusType? AbnormalStatus { get; }
		float Accuracy { get; }
		int Delta { get; }
		int? Duration { get; }
		bool IsRemoveable { get; }
		bool IsStackable { get; }
		StatType StatType { get; }
		string UnitActionId { get; }
	}
}