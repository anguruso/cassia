﻿using Common.DataContainers;
using Instantiator.Stage.Models.MeshFactory;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.StageBuilder.Models.MeshFactory
{
	[TestFixture]
	public class VertexLocatorTest
	{
		private Vector3 _offset = new Vector3(-0.5f, 0, 1);
        private VertexLocator _vertexLocator;

        [SetUp]
        public void SetUp()
        {
            _vertexLocator = VertexLocator.CreateForDimensions(1f, 0.1f);
	        _vertexLocator.SetOffset(_offset);
        }

        [Test]
        public void GetOrigin()
        {
	        var expected = _offset;
            var actual = _vertexLocator.GetWorldPosition(new IntVector3(0, 0, 0));
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetGridPoint()
        {
            var expected = new Vector3(10, 0.3f, 4) + _offset;
            var actual = _vertexLocator.GetWorldPosition(new IntVector3(10, 3, 4));
            Assert.AreEqual(expected, actual);
        }
    }
}
