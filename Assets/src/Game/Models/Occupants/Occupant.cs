﻿using System;
using Common.DataContainers;

namespace Game.Models.Occupants
{
	public abstract class Occupant : IOccupant
	{
		public Affiliation Affiliation { get; set; }

		public virtual bool AllowsPassingBy(IOccupant other)
		{
			return Affiliation.IsFriendlyWith(other.Affiliation);
		}

		public virtual bool IsSelectable
		{
			get { return false; }
		}

		public Facing Facing { get; set; }

		protected Occupant()
		{
			Affiliation = Affiliation.None;
		}
	}
}
