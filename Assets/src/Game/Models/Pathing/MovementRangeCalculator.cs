﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Common.DataContainers;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Stage;

namespace Game.Models.Pathing
{
	public class MovementRangeCalculator
	{
		private IMap _map;
		private IUnit _unit;
		private List<GridCoords> _movementRange;

		private MovementRangeCalculator() { }

		public static MovementRangeCalculator CreateForMapAndUnit(IMap map, IUnit unit)
		{
			return new MovementRangeCalculator
			{
				_map = map,
				_unit = unit
			};
		}

        // TODO: this function is garbage and slow
		public IEnumerable<GridCoords> GetMovementRange()
		{
            _movementRange = new List<GridCoords>();

            VisitGridCoord(_map.GetGridCoords(_unit));
            VisitAdjacentRecursively(_map.GetGridCoords(_unit), _unit.Move);

            return _movementRange;
        }

		private void VisitAdjacentRecursively(GridCoords currentCoords, int stepsRemaining)
		{
			if (stepsRemaining <= 0) return;

			var adjacentGridCoords = _map.GetAdjacentGridCoords(currentCoords);
			--stepsRemaining;
			
			foreach (var adjacentCoords in adjacentGridCoords)
			{
				if (CannotVisit(currentCoords, adjacentCoords)) continue;
				VisitGridCoord(adjacentCoords);
				VisitAdjacentRecursively(adjacentCoords, stepsRemaining);
			}
		}

		private bool CannotVisit(GridCoords currentCoords, GridCoords targetCoords)
		{
			var currentSquare = _map.GetSquare(currentCoords);
			var targetSquare = _map.GetSquare(targetCoords);

			return !targetSquare.IsPathableBy(_unit) ||
			       targetSquare.Height - currentSquare.Height > _unit.Jump;

		}

		private void VisitGridCoord(GridCoords gridCoords)
		{
			if (!_movementRange.Contains(gridCoords)) _movementRange.Add(gridCoords);
		}
	}
}
