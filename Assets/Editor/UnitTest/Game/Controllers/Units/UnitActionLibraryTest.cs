﻿using System;
using System.Collections.Generic;
using Common.Exceptions;
using Config;
using Game.Controllers;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Instantiator.Units;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Controllers.Units
{
    [TestFixture]
    public class UnitActionLibraryTest
    {
        private UnitActionLibrary _library;

        [SetUp]
        public void SetUp()
        {
            _library = new UnitActionLibrary();
            _library.UnitActionFactory = Substitute.For<IUnitActionFactory>();
        }

        [Test]
        public void LoadAndGetConfigs()
        {
            var unitActionConfigs = new List<UnitActionConfig>
            {
                new UnitActionConfig { Id = "One" },
                new UnitActionConfig { Id = "Two" }
            };

            var expectedFirstUnitAction = new UnitAction();
            _library.UnitActionFactory.CreateUnitAction(unitActionConfigs[0]).Returns(expectedFirstUnitAction);

            var expectedSecondUnitAction = new UnitAction();
            _library.UnitActionFactory.CreateUnitAction(unitActionConfigs[1]).Returns(expectedSecondUnitAction);

            _library.LoadConfigs(unitActionConfigs);

            Assert.AreEqual(expectedFirstUnitAction, _library.GetUnitAction("One"));
            Assert.AreEqual(expectedSecondUnitAction, _library.GetUnitAction("Two"));
        }

        [Test]
        [ExpectedException(typeof(InvalidConfigException), ExpectedMessage = "UnitAction with duplicate id 'One'.")]
        public void LoadDuplicateId()
        {
            var unitActionConfigs = new List<UnitActionConfig>
            {
                new UnitActionConfig { Id = "One" },
                new UnitActionConfig { Id = "One" }
            };

            _library.LoadConfigs(unitActionConfigs);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException), ExpectedMessage = "Could not get UnitAction with id 'Three'.")]
        public void GetNonExistantConfig()
        {
            _library.GetUnitAction("Three");
        }
    }
}
