﻿// ReSharper disable InconsistentNaming

using System;

namespace Common.DataContainers
{
    public struct IntVector3
    {
        public IntVector3(int xValue, int yValue, int zValue)
        {
            x = xValue;
            y = yValue;
            z = zValue;
        }

        public readonly int x;
        public readonly int y;
        public readonly int z;

        public static IntVector3 operator +(IntVector3 v1, IntVector3 v2)
        {
            return new IntVector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
        }

        public override string ToString()
        {
            return string.Format("IntVector3({0}, {1}, {2})", x, y, z);
        }

        public override bool Equals(Object obj)
        {
            return obj is IntVector3 && this == (IntVector3)obj;
        }
        public override int GetHashCode()
        {
            var hash = 13;
            hash += (hash * 7) + x.GetHashCode();
            hash += (hash * 7) + y.GetHashCode();
            hash += (hash * 7) + z.GetHashCode();
            return hash;
        }
        public static bool operator ==(IntVector3 first, IntVector3 second)
        {
            return first.x == second.x && first.y == second.y && first.z == second.z;
        }
        public static bool operator !=(IntVector3 first, IntVector3 second)
        {
            return !(first == second);
        }

        public static readonly IntVector3 Zero = new IntVector3(0, 0, 0);
        public static readonly IntVector3 Left = new IntVector3(-1, 0, 0);
        public static readonly IntVector3 Right = new IntVector3(1, 0, 0);
        public static readonly IntVector3 Back = new IntVector3(0, 0, -1);
        public static readonly IntVector3 Forward = new IntVector3(0, 0, 1);
        public static readonly IntVector3 Up = new IntVector3(0, 1, 0);
        public static readonly IntVector3 Down = new IntVector3(0, -1, 0);
    }
}