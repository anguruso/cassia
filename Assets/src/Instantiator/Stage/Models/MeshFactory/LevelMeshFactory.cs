﻿using System.Collections.Generic;
using System.Linq;
using Config;
using Instantiator.Stage.Models.MeshModel;
using UnityEngine;

namespace Instantiator.Stage.Models.MeshFactory
{
	public class LevelMeshFactory
	{
	    private float _chunkSize;
	    private VertexLocator _vertexLocator;

		public static LevelMeshFactory CreateWithDimensions(float chunkSize, float chunkHeight)
		{
		    return new LevelMeshFactory
		    {
                _chunkSize = chunkSize,
                _vertexLocator = VertexLocator.CreateForDimensions(chunkSize, chunkHeight),
		    };
		}

        public Mesh CreateMesh(int[,] heights, MapViewConfig mapViewConfig, MapTextureConfig mapTextureConfig)
        {
	        var chunkFactory = ChunkFactory.Create(mapTextureConfig);

            _vertexLocator.SetOffset(GetCenterOffset(heights));
            var vertices = new List<Vector3>();
            var triangles = new List<int>();
            var uv = new List<Vector2>();

            var chunkCreator = ChunkCreator.CreateWithChunkFactory(chunkFactory);
            var chunks = chunkCreator.CreateChunks(heights, mapViewConfig.Squares, mapTextureConfig);

            foreach (var chunk in chunks)
            {
                var faces = chunk.GetFaces();
                foreach (var face in faces)
                {
                    triangles.AddRange(GetFaceTriangles(vertices.Count));
                    uv.AddRange(face.GetUv());
                    vertices.AddRange(face.GetCornerPositions().Select(x => _vertexLocator.GetWorldPosition(x)));
                }
            }

            return CreateMesh(vertices, triangles, uv);
        }

        private Vector3 GetCenterOffset(int[,] heights)
        {
            var centerOffsetCalculator = CenterOffsetCalculator.CreateWithDimensions(_chunkSize);
            return centerOffsetCalculator.GetCenterOffset(heights);
        }

	    private IEnumerable<int> GetFaceTriangles(int offset)
	    {
	        return new List<int> { 0 + offset, 1 + offset, 2 + offset, 2 + offset, 3 + offset, 0 + offset };
	    }

	    private Mesh CreateMesh(IEnumerable<Vector3> vertices, IEnumerable<int> triangles, IEnumerable<Vector2> uv)
	    {
            var mesh = new Mesh
            {
                vertices = vertices.ToArray(),
                triangles = triangles.ToArray(),
                uv = uv.ToArray()
            };
            mesh.RecalculateNormals();
            return mesh;
        }
    }
}
