﻿using System;
using Common.DataContainers;
using Config;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using NUnit.Framework;

namespace UnitTest.Game.Models.Stage
{
	[TestFixture]
	class MapSetOccupantTest
	{
		private Map _map;
		private Unit _unit;

		[SetUp]
		public void SetUp()
		{
			var heights = new[,]
            {
                { 1, 1, 1 }
            };

            _map = new Map();
			_map.LoadHeights(heights);

			_unit = new Unit();
			_map.SetOccupant(_unit, new GridCoords(0, 0));
		    _map.SetOccupant(EmptyOccupant.Blocking, new GridCoords(0, 2));
		}

		[Test]
		public void SetOccupant()
		{
			_map.SetOccupant(_unit, new GridCoords(0, 0));

			Assert.AreEqual(_unit, _map.GetSquare(new GridCoords(0, 0)).Occupant);
			Assert.AreEqual(EmptyOccupant.None, _map.GetSquare(new GridCoords(0, 1)).Occupant);

			_map.SetOccupant(_unit, new GridCoords(0, 1));

			Assert.AreEqual(EmptyOccupant.None, _map.GetSquare(new GridCoords(0, 0)).Occupant);
			Assert.AreEqual(_unit, _map.GetSquare(new GridCoords(0, 1)).Occupant);
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException),
			ExpectedMessage = "Cannot set occupant at GridCoords(0, 2) because this square is occupied by Game.Models.Occupants.BlockingOccupant.")]
		public void SetOccupantAtUnpathableSquare()
		{
			_map.SetOccupant(_unit, new GridCoords(0, 2));
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException),
			ExpectedMessage = "Cannot set occupant at GridCoords(0, 0) because this square is occupied by Unit(Unnamed).")]
		public void SetOccupantAtOccupiedSquare()
		{
			_map.SetOccupant(_unit, new GridCoords(0, 0));

			var characterModelNew = new Unit();
			_map.SetOccupant(characterModelNew, new GridCoords(0, 0));
		}

		[Test]
		public void SetOccupantOnOwnSquare()
		{
			_map.SetOccupant(_unit, new GridCoords(0, 0));
			_map.SetOccupant(_unit, new GridCoords(0, 0));

			Assert.AreEqual(_unit, _map.GetSquare(new GridCoords(0, 0)).Occupant);
		}
	}
}
