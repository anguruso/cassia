﻿using System;
using System.Collections.Generic;

namespace Common.StateMachine.StateRuleBuilders
{
	public class PreTransitionStateRuleBuilder<TKey> : StateRuleBuilder<TKey>
	{
		private PreTransitionStateRuleBuilder() { }

		public static PreTransitionStateRuleBuilder<TKey> Create(List<StateMachine<TKey>.BuildRuleDelegate> buildActions)
		{
			return new PreTransitionStateRuleBuilder<TKey>
			{
				_buildActions = buildActions
			};
		}   

		public TransitionStateRuleBuilder<TKey> GoTo(TKey targetState)
		{
			_buildActions.Add(rule =>
			{
				((StateRule<TKey>)rule).AddTransition(targetState, () => true);
				return rule;
			});
			return TransitionStateRuleBuilder<TKey>.Create(_buildActions, () => true);
		}

	    public GuardedStateRuleBuilder<TKey> If(Func<bool> predicate)
	    {
            return GuardedStateRuleBuilder<TKey>.CreateFromPredicate(_buildActions, predicate);
        }
	}

	public class PreTransitionStateRuleBuilder<TKey, T0> : StateRuleBuilder<TKey>
	{
		private PreTransitionStateRuleBuilder() { }

		public static PreTransitionStateRuleBuilder<TKey, T0> Create(List<StateMachine<TKey>.BuildRuleDelegate> buildActions)
		{
			return new PreTransitionStateRuleBuilder<TKey, T0>
			{
				_buildActions = buildActions
			};
		}

		public TransitionStateRuleBuilder<TKey, T0> GoTo(TKey targetState)
		{
			_buildActions.Add(rule =>
			{
				((StateRule<TKey, T0>)rule).AddTransition(targetState, x => true);
				return rule;
			});
			return TransitionStateRuleBuilder<TKey, T0>.Create(_buildActions, x => true);
		}

		public GuardedStateRuleBuilder<TKey, T0> If(Func<T0, bool> predicate)
		{
			return GuardedStateRuleBuilder<TKey, T0>.CreateFromPredicate(_buildActions, predicate);
		}
	}

    public class PreTransitionStateRuleBuilder<TKey, T0, T1> : StateRuleBuilder<TKey>
    {
        private PreTransitionStateRuleBuilder() { }

        public static PreTransitionStateRuleBuilder<TKey, T0, T1> Create(List<StateMachine<TKey>.BuildRuleDelegate> buildActions)
        {
            return new PreTransitionStateRuleBuilder<TKey, T0, T1>
            {
                _buildActions = buildActions
            };
        }

        public TransitionStateRuleBuilder<TKey, T0, T1> GoTo(TKey targetState)
        {
            _buildActions.Add(rule =>
            {
                ((StateRule<TKey, T0, T1>)rule).AddTransition(targetState, (x, y) => true);
                return rule;
            });
            return TransitionStateRuleBuilder<TKey, T0, T1>.Create(_buildActions, (x, y) => true);
        }

        public GuardedStateRuleBuilder<TKey, T0, T1> If(Func<T0, T1, bool> predicate)
        {
            return GuardedStateRuleBuilder<TKey, T0, T1>.CreateFromPredicate(_buildActions, predicate);
        }
    }
}