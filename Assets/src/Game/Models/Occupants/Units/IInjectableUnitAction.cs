﻿using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.Occupants.Units
{
    public interface IInjectableUnitAction : IUnitAction
    {
        
    }
}