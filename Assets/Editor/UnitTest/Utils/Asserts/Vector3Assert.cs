﻿/*
 * Copyright (c) 2014 Martin Preisler
 * Licensed under http://opensource.org/licenses/MIT
 */

using System;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Utils.Asserts
{
	public class Vector3Assert
	{
		#region Vector3
		public static void AreEqual(Vector3 expected, Vector3 actual, float delta, string message)
		{
			if (delta <= 0)
				delta = 0.001f;

			float distance = Vector3.Distance(expected, actual);

			if (string.IsNullOrEmpty(message))
				message = String.Format("Expected: Vector3({0}, {1}, {2})\nBut was:  Vector3({3}, {4}, {5})\nDistance: {6} is greated than allowed delta {7}",
					expected.x, expected.y, expected.z,
					actual.x, actual.y, actual.z,
					distance, delta);

			Assert.That(distance, Is.LessThanOrEqualTo(delta), message);
		}

		public static void AreEqual(Vector3 expected, Vector3 actual, float delta)
		{
			AreEqual(expected, actual, delta, null);
		}

		public static void AreEqual(Vector3 expected, Vector3 actual, string message)
		{
			AreEqual(expected, actual, 0, message);
		}

		public static void AreEqual(Vector3 expected, Vector3 actual)
		{
			AreEqual(expected, actual, 0, null);
		}
		#endregion
	}
}