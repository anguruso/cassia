﻿using Config;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using strange.extensions.injector.api;

namespace Instantiator.Units
{
    public class UnitActionFactory : IUnitActionFactory
    {
        [Inject]
        public IInjectionBinder InjectionBinder { get; set; }

        public IUnitAction CreateUnitAction(UnitActionConfig config)
        {
            var unitAction = InjectionBinder.GetInstance<IInjectableUnitAction>();
            unitAction.LoadConfig(config);

            return unitAction;
        }
    }
}
