﻿namespace Game.Models.Occupants
{
	public static class EmptyOccupant
	{
		public static IOccupant None = new NoneOccupant();
		public static IOccupant Blocking = new BlockingOccupant();
	}

	public class NoneOccupant : Occupant
	{
		public override bool AllowsPassingBy(IOccupant other)
		{
			return true;
		}
	}

	public class BlockingOccupant : Occupant
	{
		public override bool AllowsPassingBy(IOccupant other)
		{
			return false;
		}
	}
}