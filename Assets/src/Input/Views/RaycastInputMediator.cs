﻿using Game.Controllers;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Input.Views
{
	public class RaycastInputMediator : Mediator
	{
		[Inject]
		public RaycastInputView View { get; set; }

		[Inject]
		public IWorldGridCoordsConverter WorldGridCoordsConverter { get; set; }

		public override void OnRegister()
		{
		}
	}
}
