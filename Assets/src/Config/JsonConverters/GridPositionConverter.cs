﻿using System;
using System.Linq;
using Common.DataContainers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Config.JsonConverters
{
    public class GridPositionConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException("WriteJson is not implemented for GridPositionConverter.");
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
	        var jObject = JObject.Load(reader);

	        var gridCoordsList = jObject.Value<JArray>("GridCoords").Select(x => x.Value<int>()).ToList();
	        var gridCoords = GridCoords.CreateFromList(gridCoordsList);

	        var direction = jObject.Value<string>("Facing");
			var facing = Facing.CreateFromDirection((Direction)Enum.Parse(typeof(Direction), direction));

	        return new GridPosition(gridCoords, facing);
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(GridPosition).IsAssignableFrom(objectType);
        }
    }
}
