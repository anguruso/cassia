﻿using Config;
using Game.Models.Occupants.Units;
using strange.extensions.injector.api;

namespace Instantiator.Units
{
    public class UnitFactory : IUnitFactory
    {
        [Inject]
        public IInjectionBinder InjectionBinder { get; set; }

        public IUnit CreateUnit(UnitConfig config)
        {
            var unit = InjectionBinder.GetInstance<IInjectableUnit>();
            unit.LoadConfig(config);
            return unit;
        }
    }
}
