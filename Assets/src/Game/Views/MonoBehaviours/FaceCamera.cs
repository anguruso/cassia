﻿using UnityEngine;

namespace Game.Views.MonoBehaviours
{
    public class FaceCamera : MonoBehaviour
    {
        private void Awake()
        {
            var cameraRotation = Camera.main.transform.rotation;
            transform.rotation = cameraRotation;
        }
    }
}
