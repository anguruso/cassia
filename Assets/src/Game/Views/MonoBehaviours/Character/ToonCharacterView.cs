using UnityEngine;

namespace Game.Views.MonoBehaviours.Character
{
    public class ToonCharacterView : CharacterView
    {
        public Animator Animator;

        private enum AnimationState
        {
            Idle,
            Walking
        }

        private AnimationState _animationState;

        // temp
        public override void Walk()
        {
            if (_animationState != AnimationState.Walking)
            {
                Animator.SetTrigger("Walk");
                _animationState = AnimationState.Walking;
            }
        }

        public override void Stop()
        {
            if (_animationState == AnimationState.Walking)
            {
                Animator.SetTrigger("Walk");
                _animationState = AnimationState.Idle;
            }
        }
    }
}