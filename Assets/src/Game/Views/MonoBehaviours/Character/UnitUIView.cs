﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Views.MonoBehaviours.Character
{
    public class UnitUIView : MonoBehaviour
    {
        public Text UnitNameText;

        public string UnitName
        {
            get { return UnitNameText.text; }
            set { UnitNameText.text = value; }
        }
    }
}
