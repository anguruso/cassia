﻿using System;
using System.Collections.Generic;
using Common.DataContainers;

namespace Instantiator.Stage.Models.HeightMap
{
    public class HeightMapSquare
    {
        private IntVector3 _leftBackPoint;
        private List<HeightMapTile> _tiles; 

        private HeightMapSquare() { }

        public static HeightMapSquare CreateAtPosition(int x, int z)
        {
            return new HeightMapSquare
            {
                _tiles = new List<HeightMapTile>(),
                _leftBackPoint = new IntVector3(x, 0, z)
            };
        }

        public HeightMapTile GetTile(int height)
        {
            if (height >= _tiles.Count) throw new ArgumentOutOfRangeException("height");

            return _tiles[height];
        }

        public void CreateTilesForHeight(int height)
        {
	        Height = height;
	        if (height == 0) return;

            for (var i = 0; i < height + 1; ++i)
            {
                var tile = HeightMapTile.CreateWithLeftBackPoint(_leftBackPoint + new IntVector3(0, TileCount, 0));
                _tiles.Add(tile);
            }
        }

        public int TileCount
        {
            get { return _tiles.Count; }
        }

		public int Height { get; private set; }
    }
}