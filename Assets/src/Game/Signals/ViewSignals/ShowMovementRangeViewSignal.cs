﻿using System.Collections.Generic;
using Common.DataContainers;
using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.ViewSignals
{
	public class ShowMovementRangeViewSignal : Signal<IEnumerable<GridCoords>>, IShowMovementRangeViewSignal
	{
	}

	public interface IShowMovementRangeViewSignal : ISignal<IEnumerable<GridCoords>>
	{
	}
}
