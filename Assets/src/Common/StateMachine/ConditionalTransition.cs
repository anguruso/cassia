using System;
using JetBrains.Annotations;

namespace Common.StateMachine
{
	public class ConditionalTransition<TKey>
	{
	    private Func<bool> _predicate; 

		private ConditionalTransition() { }

		public static ConditionalTransition<TKey> CreateForTargetStateAndPredicate(TKey targetState, Func<bool> predicate)
		{
			return new ConditionalTransition<TKey>
			{
				TargetState = targetState,
                _predicate = predicate,
				TransitionAction = () => { }
			};
		}

		public TKey TargetState { get; private set; }
		public Action TransitionAction { get; set; }

		public bool ShouldTransition()
		{
			return _predicate();
		}
	}

	public class ConditionalTransition<TKey, T0>
	{
		private Func<T0, bool> _predicate;

		private ConditionalTransition() { }

		public static ConditionalTransition<TKey, T0> CreateForTargetStateAndPredicate(TKey targetState, Func<T0, bool> predicate)
		{
			return new ConditionalTransition<TKey, T0>
			{
				TargetState = targetState,
				_predicate = predicate,
				TransitionAction = x => { }
			};
		}

		public TKey TargetState { get; private set; }
		public Action<T0> TransitionAction { get; set; }

		public bool ShouldTransition(T0 arg)
		{
			return _predicate(arg);
		}
	}

    public class ConditionalTransition<TKey, T0, T1>
    {
        private Func<T0, T1, bool> _predicate;

        private ConditionalTransition() { }

        public static ConditionalTransition<TKey, T0, T1> CreateForTargetStateAndPredicate(TKey targetState, Func<T0, T1, bool> predicate)
        {
            return new ConditionalTransition<TKey, T0, T1>
            {
                TargetState = targetState,
                _predicate = predicate,
                TransitionAction = (x, y) => { }
            };
        }

        public TKey TargetState { get; private set; }
        public Action<T0, T1> TransitionAction { get; set; }

        public bool ShouldTransition(T0 arg0, T1 arg1)
        {
            return _predicate(arg0, arg1);
        }
    }
}