﻿using System;
using strange.extensions.signal.api;

namespace Common.Signals
{
	public interface ISignal : IBaseSignal
	{
		void Dispatch();
		void AddListener(Action listenerAction);
	}

	public interface ISignal<T> : IBaseSignal
	{
		void Dispatch(T arg);
		void AddListener(Action<T> listenerAction);
	    void RemoveListener(Action<T> listenerAction);
	}

	public interface ISignal<T0, T1> : IBaseSignal
	{
		void Dispatch(T0 arg0, T1 arg1);
		void AddListener(Action<T0, T1> listenerAction);
        void RemoveListener(Action<T0, T1> listenerAction);
    }

	public interface ISignal<T0, T1, T2> : IBaseSignal
	{
		void Dispatch(T0 arg0, T1 arg1, T2 arg2);
		void AddListener(Action<T0, T1, T2> listenerAction);
        void RemoveListener(Action<T0, T1, T2> listenerAction);
    }
}
