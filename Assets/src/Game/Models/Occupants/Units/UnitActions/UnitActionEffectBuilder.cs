namespace Game.Models.Occupants.Units.UnitActions
{
    public class UnitActionEffectBuilder : IUnitActionEffectBuilder
    {
	    public delegate int GetDeltaDelegate(IUnitStats actorStats, IUnitStats targetStats);

        private UnitActionEffectBuilder() { }

        public static UnitActionEffectBuilder Create()
        {
            return new UnitActionEffectBuilder();
        }

        public string UnitActionId { get; set; }
        public float Accuracy { get; set; }
        public AbnormalStatusType? AbnormalStatus { get; set; }
        public StatType StatType { get; set; }
        public int? Duration { get; set; }
        public bool IsStackable { get; set; }
        public bool IsRemoveable { get; set; }
	    public GetDeltaDelegate GetDelta { get; set; } 
    }
}