﻿using System.Collections;
using System.Collections.Generic;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Views.MonoBehaviours.Character;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Game.Views
{
	public class UnitView : View
	{
        public Signal MoveCompleteSignal = new Signal();

        public IUnit Unit { get; private set; }

	    private CharacterView _characterView;
	    private UnitUIView _unitUIView;

	    public void Ready()
	    {
	        _characterView = GetComponentInChildren<CharacterView>();
	        _unitUIView = GetComponentInChildren<UnitUIView>();

	        _unitUIView.UnitName = Unit.Name;
	    }

		public void MoveTo(IEnumerable<Vector3> path)
		{
			StartCoroutine(CoMoveAlongPath(path));
		}

		public void SetTo(Vector3 position, Quaternion rotation)
		{
			transform.position = position;
			_characterView.transform.localRotation = rotation;
		}

		private IEnumerator CoMoveAlongPath(IEnumerable<Vector3> path)
		{
            _characterView.Walk();
			foreach (var position in path)
			{
				while (transform.position != position)
				{
                    _characterView.transform.LookAt(position);
					transform.position = Vector3.MoveTowards(transform.position, position, Time.deltaTime * 2);
					yield return null;
				}
			}

            _characterView.Stop();
            MoveCompleteSignal.Dispatch();
		}

		public void SetUnit(IUnit unit)
		{
			Unit = unit;
		}
	}
}
