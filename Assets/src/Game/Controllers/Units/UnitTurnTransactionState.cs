﻿namespace Game.Controllers.Units
{
	public enum UnitTurnTransactionState
	{
		Nothing,
		ActorSelected,
		Moving,
		PreSelectAction,
		PreSelectTarget,
		PreApplyTransaction
	}
}