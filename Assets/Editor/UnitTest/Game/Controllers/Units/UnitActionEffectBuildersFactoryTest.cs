using System.Collections.Generic;
using System.Linq;
using Config;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Controllers.Units
{
    [TestFixture]
    public class UnitActionEffectBuildersFactoryTest
    {
        [Test]
        public void GetSinglePermanentEffect()
        {
            var effectConfigs = new List<UnitActionEffectConfig>
            {
                new UnitActionEffectConfig
                {
                    TargetStatType = StatType.Health,
					ActorMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Power,
						Value = -1.5f
					},
					TargetMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Defense,
						Value = 1
					},
                    Accuracy = 1,
                    IsStackable = true,
                    IsRemoveable = true
                }
            };

            var factory = new UnitActionEffectBuildersFactory();
            var effects = factory.GetEffectBuilders("TEST", effectConfigs).ToList();

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

	        var targetStats = Substitute.For<IUnitStats>();
	        targetStats.Get(StatType.Defense).Returns(10);

			Assert.AreEqual("TEST", effects[0].UnitActionId);
            Assert.AreEqual(1, effects.Count);
            Assert.AreEqual(StatType.Health, effects[0].StatType);
            Assert.AreEqual(-5, effects[0].GetDelta(actorStats, targetStats));
            Assert.AreEqual(1, effects.First().Accuracy);
            Assert.IsNull(effects[0].AbnormalStatus);
            Assert.IsNull(effects[0].Duration);
            Assert.IsTrue(effects[0].IsStackable);
            Assert.IsTrue(effects[0].IsRemoveable);
        }

        [Test]
        public void GetSingleTimedEffect()
        {
            var effectConfigs = new List<UnitActionEffectConfig>
            {
                new UnitActionEffectConfig
                {
                    AbnormalStatus = AbnormalStatusType.Weaken,
                    Duration = 3,
                    Accuracy = 0.5f
                }
            };

            var factory = new UnitActionEffectBuildersFactory();
            var effects = factory.GetEffectBuilders("TEST", effectConfigs).ToList();

            Assert.AreEqual("TEST", effects[0].UnitActionId);
            Assert.AreEqual(1, effects.Count);
            Assert.AreEqual(0.5f, effects[0].Accuracy);
            Assert.AreEqual(AbnormalStatusType.Weaken, effects[0].AbnormalStatus);
            Assert.AreEqual(3, effects[0].Duration);
        }

        [Test]
        public void GetMultipleEffects()
        {
            var effectConfigs = new List<UnitActionEffectConfig>
            {
                new UnitActionEffectConfig
                {
                    TargetStatType = StatType.Health,
					ActorMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Power,
						Value = -1
					},
					TargetMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Defense,
						Value = 0.5f
					},
					Accuracy = 1
                },
                new UnitActionEffectConfig
                {
                    TargetStatType = StatType.Defense,
					ActorMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Power,
						Value = -0.5f
					},
					TargetMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Defense,
						Value = -0.5f
					},
					Accuracy = 0.5f
                }
            };

            var factory = new UnitActionEffectBuildersFactory();
            var effects = factory.GetEffectBuilders("TEST", effectConfigs).ToList();

			var actorStats = Substitute.For<IUnitStats>();
            actorStats.Get(StatType.Power).Returns(10);

	        var targetStats = Substitute.For<IUnitStats>();
	        targetStats.Get(StatType.Defense).Returns(10);

            Assert.AreEqual(2, effects.Count);

            Assert.AreEqual("TEST", effects[0].UnitActionId);
            Assert.AreEqual(StatType.Health, effects[0].StatType);
            Assert.AreEqual(-5, effects[0].GetDelta(actorStats, targetStats));
            Assert.AreEqual(1, effects[0].Accuracy);

            Assert.AreEqual("TEST", effects[1].UnitActionId);
            Assert.AreEqual(StatType.Defense, effects[1].StatType);
            Assert.AreEqual(-10, effects[1].GetDelta(actorStats, targetStats));
            Assert.AreEqual(0.5f, effects[1].Accuracy);
        }

        [Test]
        public void GetRoundedDownDelta()
        {
            var effectConfigs = new List<UnitActionEffectConfig>
            {
                new UnitActionEffectConfig
                {
                    TargetStatType = StatType.Health,
					ActorMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Power,
						Value = 1.4f
					},
					TargetMultiplier = new StatsMultiplierConfig()
                }
            };

            var factory = new UnitActionEffectBuildersFactory();
            var effects = factory.GetEffectBuilders("TEST", effectConfigs).ToList();

            var actorStats = Substitute.For<IUnitStats>();
            actorStats.Get(StatType.Power).Returns(1);

            Assert.AreEqual(StatType.Health, effects[0].StatType);
            Assert.AreEqual(1, effects[0].GetDelta(actorStats, Substitute.For<IUnitStats>()));
        }

        [Test]
        public void GetRoundedUpDelta()
        {
            var effectConfigs = new List<UnitActionEffectConfig>
            {
                new UnitActionEffectConfig
                {
                    TargetStatType = StatType.Health,
					ActorMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Power,
						Value = 1.5f
					},
					TargetMultiplier = new StatsMultiplierConfig()
				}
            };

            var factory = new UnitActionEffectBuildersFactory();
            var effects = factory.GetEffectBuilders("TEST", effectConfigs).ToList();

            var actorStats = Substitute.For<IUnitStats>();
            actorStats.Get(StatType.Power).Returns(1);

            Assert.AreEqual(StatType.Health, effects[0].StatType);
            Assert.AreEqual(2, effects[0].GetDelta(actorStats, Substitute.For<IUnitStats>()));
        }

	    [Test]
	    public void NoTargetMultiplier()
	    {
			var effectConfigs = new List<UnitActionEffectConfig>
			{
				new UnitActionEffectConfig
				{
					TargetStatType = StatType.Health,
					ActorMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Power,
						Value = -1.5f
					}
				}
			};

			var factory = new UnitActionEffectBuildersFactory();

			var effects = factory.GetEffectBuilders("TEST", effectConfigs).ToList();

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

			Assert.AreEqual(-15, effects[0].GetDelta(actorStats, Substitute.For<IUnitStats>()));
		}

	    [Test]
	    public void TargetCanOnlyPreventDamage()
	    {
			var effectConfigs = new List<UnitActionEffectConfig>
			{
				new UnitActionEffectConfig
				{
					TargetStatType = StatType.Health,
					ActorMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Power,
						Value = -1
					},
					TargetMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Defense,
						Value = 1
					},
					Accuracy = 1,
					IsStackable = true,
					IsRemoveable = true
				}
			};

			var factory = new UnitActionEffectBuildersFactory();
			var effects = factory.GetEffectBuilders("TEST", effectConfigs).ToList();

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

			var targetStats = Substitute.For<IUnitStats>();
			targetStats.Get(StatType.Defense).Returns(20);

			Assert.AreEqual(0, effects[0].GetDelta(actorStats, targetStats));
		}

	    [Test]
	    public void TargetCanOnlyPreventHeal()
	    {
			var effectConfigs = new List<UnitActionEffectConfig>
			{
				new UnitActionEffectConfig
				{
					TargetStatType = StatType.Health,
					ActorMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Power,
						Value = 1
					},
					TargetMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Defense,
						Value = -1
					},
					Accuracy = 1,
					IsStackable = true,
					IsRemoveable = true
				}
			};

			var factory = new UnitActionEffectBuildersFactory();
			var effects = factory.GetEffectBuilders("TEST", effectConfigs).ToList();

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

			var targetStats = Substitute.For<IUnitStats>();
			targetStats.Get(StatType.Defense).Returns(20);

			Assert.AreEqual(0, effects[0].GetDelta(actorStats, targetStats));
		}

	    [Test]
	    public void TargetCanIncreaseDamage()
	    {
			var effectConfigs = new List<UnitActionEffectConfig>
			{
				new UnitActionEffectConfig
				{
					TargetStatType = StatType.Health,
					ActorMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Power,
						Value = -1
					},
					TargetMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Defense,
						Value = -1
					},
					Accuracy = 1,
					IsStackable = true,
					IsRemoveable = true
				}
			};

			var factory = new UnitActionEffectBuildersFactory();
			var effects = factory.GetEffectBuilders("TEST", effectConfigs).ToList();

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

			var targetStats = Substitute.For<IUnitStats>();
			targetStats.Get(StatType.Defense).Returns(20);

			Assert.AreEqual(-30, effects[0].GetDelta(actorStats, targetStats));
		}

		[Test]
		public void TargetCanIncreaseHeal()
		{
			var effectConfigs = new List<UnitActionEffectConfig>
			{
				new UnitActionEffectConfig
				{
					TargetStatType = StatType.Health,
					ActorMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Power,
						Value = 1
					},
					TargetMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Defense,
						Value = 1
					},
					Accuracy = 1,
					IsStackable = true,
					IsRemoveable = true
				}
			};

			var factory = new UnitActionEffectBuildersFactory();
			var effects = factory.GetEffectBuilders("TEST", effectConfigs).ToList();

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

			var targetStats = Substitute.For<IUnitStats>();
			targetStats.Get(StatType.Defense).Returns(20);

			Assert.AreEqual(30, effects[0].GetDelta(actorStats, targetStats));
		}

		[Test]
		public void NoActorMultiplier()
		{
			var effectConfigs = new List<UnitActionEffectConfig>
			{
				new UnitActionEffectConfig
				{
					TargetStatType = StatType.Health,
					TargetMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Defense,
						Value = 1
					},
					Accuracy = 1,
					IsStackable = true,
					IsRemoveable = true
				}
			};

			var factory = new UnitActionEffectBuildersFactory();
			var effects = factory.GetEffectBuilders("TEST", effectConfigs).ToList();

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

			var targetStats = Substitute.For<IUnitStats>();
			targetStats.Get(StatType.Defense).Returns(20);

			Assert.AreEqual(20, effects[0].GetDelta(actorStats, targetStats));
		}
	}
}