﻿using System.Collections.Generic;

namespace Config
{
    public class UnitActionsConfig
    {
        public List<UnitActionConfig> UnitActions { get; set; } 
    }
}