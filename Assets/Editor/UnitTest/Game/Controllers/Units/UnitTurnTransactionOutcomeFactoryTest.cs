﻿using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.Occupants.Units.UnitTurnTransactions;
using Game.Models.Stage;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Controllers.Units
{
    [TestFixture]
    public class UnitTurnTransactionOutcomeFactoryTest
    {
        private UnitTurnTransactionOutcomeFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _factory = new UnitTurnTransactionOutcomeFactory
            {
                DirectionCalculator = Substitute.For<IDirectionCalculator>(),
                UnitLookup = Substitute.For<IUnitLookup>(),
				UnitActionOutcomeFactory = Substitute.For<IUnitActionOutcomeFactory>()
            };
        }

        [Test]
        public void GetUnitActionOutcome()
        {
            var actor = Substitute.For<IUnit>();
            var unitActionOrigin = UnitActionOrigin.CreateFromActorAndPosition(actor, new GridCoords(1, 1));
            var unitAction = Substitute.For<IUnitAction>();
            var selfEffectBuilders = new List<IUnitActionEffectBuilder> {Substitute.For<IUnitActionEffectBuilder>()};
	        var selfOutcome = Substitute.For<IUnitActionOutcome>();
            var targetEffectBuilders = new List<IUnitActionEffectBuilder> { Substitute.For<IUnitActionEffectBuilder>() };
			var targetOutcomes = new List<IUnitActionOutcome> { Substitute.For<IUnitActionOutcome>(), Substitute.For<IUnitActionOutcome>()};
            var areaOfEffect = new List<GridCoords> {new GridCoords(1, 2), new GridCoords(2, 3)};
            var affectedUnits = new List<IUnit> {Substitute.For<IUnit>(), Substitute.For<IUnit>()};

            unitAction.SelfEffectBuilders.Returns(selfEffectBuilders);
            unitAction.TargetEffectBuilders.Returns(targetEffectBuilders);
            unitAction.GetAreaOfEffect(new GridCoords(2, 2), Facing.Forward).Returns(areaOfEffect);

            _factory.DirectionCalculator.GetFacing(new GridCoords(1, 1), new GridCoords(2, 2)).Returns(Facing.Forward);
            _factory.UnitLookup.GetUnits(Arg.Is<List<GridCoords>>(aoe => aoe[0] == new GridCoords(1, 2) && aoe[1] == new GridCoords(2, 3))).Returns(affectedUnits);
	        _factory.UnitActionOutcomeFactory.CreateUnitActionOutcome(actor.Stats, actor, selfEffectBuilders).Returns(selfOutcome);
			_factory.UnitActionOutcomeFactory.CreateUnitActionOutcome(actor.Stats, affectedUnits[0], targetEffectBuilders).Returns(targetOutcomes[0]);
			_factory.UnitActionOutcomeFactory.CreateUnitActionOutcome(actor.Stats, affectedUnits[1], targetEffectBuilders).Returns(targetOutcomes[1]);

			var outcome = _factory.GetOutcome(unitActionOrigin, unitAction, new GridCoords(2, 2));

            CollectionAssert.AreEquivalent(areaOfEffect, outcome.AreaOfEffect);
	        CollectionAssert.AreEquivalent(new List<IUnitActionOutcome> { selfOutcome, targetOutcomes[0], targetOutcomes[1] }, outcome.UnitActionOutcomes);
        }

        [Test]
        public void GetUnitActionOutcomeWithoutSelf()
        {
            var actor = Substitute.For<IUnit>();
            var unitActionOrigin = UnitActionOrigin.CreateFromActorAndPosition(actor, new GridCoords(1, 1));
            var unitAction = Substitute.For<IUnitAction>();
            var targetEffectBuilders = new List<IUnitActionEffectBuilder> { UnitActionEffectBuilder.Create() };
			var targetOutcomes = new List<IUnitActionOutcome> { Substitute.For<IUnitActionOutcome>(), Substitute.For<IUnitActionOutcome>() };
			var areaOfEffect = new List<GridCoords> { new GridCoords(1, 2), new GridCoords(2, 3) };
            var affectedUnits = new List<IUnit> { Substitute.For<IUnit>(), Substitute.For<IUnit>() };

            unitAction.SelfEffectBuilders.Returns(new List<IUnitActionEffectBuilder>());
            unitAction.TargetEffectBuilders.Returns(targetEffectBuilders);
            unitAction.GetAreaOfEffect(new GridCoords(2, 2), Facing.Forward).Returns(areaOfEffect);

            _factory.DirectionCalculator.GetFacing(new GridCoords(1, 1), new GridCoords(2, 2)).Returns(Facing.Forward);
            _factory.UnitLookup.GetUnits(Arg.Is<List<GridCoords>>(aoe => aoe[0] == new GridCoords(1, 2) && aoe[1] == new GridCoords(2, 3))).Returns(affectedUnits);
			_factory.UnitActionOutcomeFactory.CreateUnitActionOutcome(actor.Stats, affectedUnits[0], targetEffectBuilders).Returns(targetOutcomes[0]);
			_factory.UnitActionOutcomeFactory.CreateUnitActionOutcome(actor.Stats, affectedUnits[1], targetEffectBuilders).Returns(targetOutcomes[1]);

			var outcome = _factory.GetOutcome(unitActionOrigin, unitAction, new GridCoords(2, 2));

            Assert.AreEqual(2, outcome.UnitActionOutcomes.Count());

			CollectionAssert.AreEquivalent(areaOfEffect, outcome.AreaOfEffect);
			CollectionAssert.AreEquivalent(targetOutcomes, outcome.UnitActionOutcomes);
		}

        [Test]
        public void GetUnitActionOutcomeOnlySelf()
        {
            var actor = Substitute.For<IUnit>();
            var unitActionOrigin = UnitActionOrigin.CreateFromActorAndPosition(actor, new GridCoords(1, 1));
            var unitAction = Substitute.For<IUnitAction>();
            var selfEffectBuilders = new List<IUnitActionEffectBuilder> { UnitActionEffectBuilder.Create() };
			var selfOutcome = Substitute.For<IUnitActionOutcome>();
			var areaOfEffect = new List<GridCoords> { new GridCoords(1, 2), new GridCoords(2, 3) };
            var affectedUnits = new List<IUnit> { Substitute.For<IUnit>(), Substitute.For<IUnit>() };

            unitAction.SelfEffectBuilders.Returns(selfEffectBuilders);
            unitAction.TargetEffectBuilders.Returns(new List<IUnitActionEffectBuilder>());
            unitAction.GetAreaOfEffect(new GridCoords(2, 2), Facing.Forward).Returns(areaOfEffect);

            _factory.DirectionCalculator.GetFacing(new GridCoords(1, 1), new GridCoords(2, 2)).Returns(Facing.Forward);
            _factory.UnitLookup.GetUnits(Arg.Is<List<GridCoords>>(aoe => aoe[0] == new GridCoords(1, 2) && aoe[1] == new GridCoords(2, 3))).Returns(affectedUnits);
			_factory.UnitActionOutcomeFactory.CreateUnitActionOutcome(actor.Stats, actor, selfEffectBuilders).Returns(selfOutcome);

			var outcome = _factory.GetOutcome(unitActionOrigin, unitAction, new GridCoords(2, 2));

            CollectionAssert.AreEquivalent(areaOfEffect, outcome.AreaOfEffect);
            CollectionAssert.AreEquivalent(new List<IUnitActionOutcome> {selfOutcome}, outcome.UnitActionOutcomes);
        }
    }
}
