﻿using System;
using Game.Views;
using Instantiator.Units;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Instantiator.Stage.Views
{
    public class GameInstantiatorView : View
    {
	    private GameObject _map;

	    public void BuildLevel(Mesh mesh)
	    {
		    var material = Resources.Load("Materials/tiles") as Material;

		    _map = new GameObject("Map");
			_map.transform.SetParent(transform);
			_map.transform.position = Vector3.zero;

			_map.AddComponent<MeshRenderer>().material = material;
			_map.AddComponent<MeshFilter>().mesh = mesh;
			_map.AddComponent<MeshCollider>();
			_map.AddComponent<MapView>();
		    _map.AddComponent<SquareHighlightsView>();
	    }

		public void BuildUnit(IInstantiatableUnitView instantiatableUnit, Vector3 position, Quaternion rotation)
		{
			if (_map == null)
			{
				var message = "Cannot build unit if map has not been built";
				throw new InvalidOperationException(message);
			}

            var unitViewGameObject = new GameObject(instantiatableUnit.Unit.Name);
            var unitView = unitViewGameObject.AddComponent<UnitView>();
            unitView.SetUnit(instantiatableUnit.Unit);

		    BuildUnitViewComponent(unitViewGameObject, instantiatableUnit.PrefabPath);
            BuildUnitViewComponent(unitViewGameObject, "Prefabs/UnitUI"); // TODO this should be in config too?

            unitViewGameObject.transform.SetParent(_map.transform);

            unitView.Ready();
            unitView.SetTo(position, rotation);
		}

        private void BuildUnitViewComponent(GameObject unitViewGameObject, string prefabPath)
        {
            var prefab = Resources.Load<GameObject>(prefabPath);
            var componentGameObject = Instantiate(prefab);
            componentGameObject.transform.SetParent(unitViewGameObject.transform);
            componentGameObject.transform.localPosition = Vector3.zero;
        }
	}
}
