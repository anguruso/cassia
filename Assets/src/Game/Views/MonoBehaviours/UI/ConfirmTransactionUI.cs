﻿using System.Collections.Generic;
using Game.Controllers.Units;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Views.MonoBehaviours.UI
{
    public class ConfirmTransactionUI : MonoBehaviour
    {
        public Button ApplyButton, CancelButton;
	    public OutcomeOverviewUI OutcomeOverview;

        public void Show(int damage, int accuracy)
        {
            ApplyButton.gameObject.SetActive(true);
            CancelButton.gameObject.SetActive(true);

			OutcomeOverview.Show(damage, accuracy);
        }

        public void Hide()
        {
            ApplyButton.gameObject.SetActive(false);
            CancelButton.gameObject.SetActive(false);

			OutcomeOverview.Hide();
        }

        public Button.ButtonClickedEvent OnApply
        {
            get { return ApplyButton.onClick; }
        }

        public Button.ButtonClickedEvent OnCancel
        {
            get { return CancelButton.onClick; }
        }
    }
}