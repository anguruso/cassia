﻿namespace Game.Models.Occupants.Units.UnitActions
{
    public interface IUnitActionEffectBuilder
    {
        string UnitActionId { get; }
        AbnormalStatusType? AbnormalStatus { get; }
        float Accuracy { get; }
        int? Duration { get; }
        StatType StatType { get; }
		UnitActionEffectBuilder.GetDeltaDelegate GetDelta { get; }
		bool IsStackable { get; }
        bool IsRemoveable { get; }
    }
}