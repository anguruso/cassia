﻿namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IUnitActionEffectFactory
	{
		IUnitActionEffect CreateUnitActionEffect(IUnitActionEffectBuilder effectBuilder, IUnitStats actorStats, IUnitStats affectedUnitStats);
	}
}