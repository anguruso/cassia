﻿namespace Game.Models.Occupants
{
	public interface IOccupant
	{
		Affiliation Affiliation { get; set; }
		bool AllowsPassingBy(IOccupant other);
		bool IsSelectable { get; }
	}
}