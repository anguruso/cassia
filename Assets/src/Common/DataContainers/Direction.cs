﻿namespace Common.DataContainers
{
	public enum Direction
	{
		Forward, Back, Left, Right
	}
}