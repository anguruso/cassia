﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Occupants.Units;

namespace Game.Controllers.Units
{
    public interface IUnitLookup
    {
        IEnumerable<IUnit> GetUnits(IEnumerable<GridCoords> area);
    }
}