﻿using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Config;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Pathing;
using Game.Models.Stage;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils;

namespace UnitTest.Game.Models.Pathing
{
	[TestFixture]
	public class MovementRangeCalculatorTest
	{
		[Test]
		public void GetMovementRange()
		{
		    var unit = Substitute.For<IUnit>();
		    unit.Move.Returns(2);
		    unit.Jump.Returns(0);

			var map = MapFactory.Create().CreateMapWithHeights(new int[,]
			{
				{1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1}
			});
			var rangeCalculator = MovementRangeCalculator.CreateForMapAndUnit(map, unit);
			map.SetOccupant(unit, new GridCoords(2, 2));
			var expected = new List<GridCoords>
			{
				new GridCoords(0, 2), new GridCoords(1, 2), new GridCoords(2, 2), new GridCoords(3, 2), new GridCoords(4, 2),
				new GridCoords(2, 0), new GridCoords(2, 1), new GridCoords(2, 3), new GridCoords(2, 4),
				new GridCoords(1, 1), new GridCoords(3, 1), new GridCoords(3, 3), new GridCoords(1, 3)
			};
			var actual = rangeCalculator.GetMovementRange();

			CollectionAssert.AreEquivalent(expected, actual);
		}

		[Test]
		public void GetMovementRangeFor3()
		{
            var unit = Substitute.For<IUnit>();
            unit.Move.Returns(3);
            unit.Jump.Returns(0);

            var map = MapFactory.Create().CreateMapWithHeights(new int[,]
{
				{1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1, 1, 1}
});
			var rangeCalculator = MovementRangeCalculator.CreateForMapAndUnit(map, unit);
			map.SetOccupant(unit, new GridCoords(3, 3));
			var expected = new List<GridCoords>
			{
				new GridCoords(0, 3),
				new GridCoords(1, 2), new GridCoords(1, 3), new GridCoords(1, 4),
				new GridCoords(2, 1), new GridCoords(2, 2), new GridCoords(2, 3), new GridCoords(2, 4), new GridCoords(2, 5),
				new GridCoords(3, 0), new GridCoords(3, 1), new GridCoords(3, 2), new GridCoords(3, 3), new GridCoords(3, 4), new GridCoords(3, 5), new GridCoords(3, 6),
				new GridCoords(4, 1), new GridCoords(4, 2), new GridCoords(4, 3), new GridCoords(4, 4), new GridCoords(4, 5),
				new GridCoords(5, 2), new GridCoords(5, 3), new GridCoords(5, 4),
				new GridCoords(6, 3)
			};
			var actual = rangeCalculator.GetMovementRange();
			CollectionAssert.AreEquivalent(expected, actual);
		}

		[Test]
		public void GetMovementRangeForBlocked()
		{
            var unit = Substitute.For<IUnit>();
            unit.Move.Returns(2);
            unit.Jump.Returns(0);

            var map = MapFactory.Create().CreateMapWithHeights(new int[,]
			{
				{1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1},
				{1, 1, 1, 1, 1}
			});
			map.SetOccupant(EmptyOccupant.Blocking, new GridCoords(1, 2));

			var rangeCalculator = MovementRangeCalculator.CreateForMapAndUnit(map, unit);
			map.SetOccupant(unit, new GridCoords(2, 2));
			var expected = new List<GridCoords>
			{
				new GridCoords(2, 2), new GridCoords(3, 2), new GridCoords(4, 2),
				new GridCoords(2, 0), new GridCoords(2, 1), new GridCoords(2, 3), new GridCoords(2, 4),
				new GridCoords(1, 1), new GridCoords(3, 1), new GridCoords(3, 3), new GridCoords(1, 3)
			};
			var actual = rangeCalculator.GetMovementRange();

			CollectionAssert.AreEquivalent(expected, actual);
		}

		[Test]
		public void GetMovementRangeForTooHigh()
		{
            var unit = Substitute.For<IUnit>();
            unit.Move.Returns(2);
            unit.Jump.Returns(0);

            var map = MapFactory.Create().CreateMapWithHeights(new int[,]
			{
				{1, 2}
			});
			map.SetOccupant(unit, new GridCoords(0, 0));

			var rangeCalculator = MovementRangeCalculator.CreateForMapAndUnit(map, unit);
			var expected = new List<GridCoords> {new GridCoords(0, 0)};
			var actual = rangeCalculator.GetMovementRange();

			CollectionAssert.AreEquivalent(expected, actual);
		}

		// i guess it could get attack range as well? this might be calculated a bit different
		// can be just adjacnet
		// single target at a range
		// piercing or multi target
		// line of sight
	}
}
