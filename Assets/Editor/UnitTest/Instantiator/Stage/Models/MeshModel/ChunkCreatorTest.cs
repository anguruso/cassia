﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Common.Exceptions;
using Config;
using Instantiator.Stage.Models.MeshModel;
using NUnit.Framework;
using UnitTest.Utils.Asserts;
using UnityEngine;

namespace UnitTest.Builder.Stage.Models.MeshModel
{
	[TestFixture]
	public class ChunkCreatorTest
	{
		private ChunkCreator _creator;
		private MapTextureConfig _mapTextureConfig;

		[SetUp]
		public void SetUp()
		{
		    _mapTextureConfig = new MapTextureConfig
		    {
		        TextureTiles = new Dictionary<string, List<Vector2>>
		        {
		            {
		                "dirt",
		                new List<Vector2>
		                {
		                    new Vector2(0, 0),
		                    new Vector2(0, 1),
		                    new Vector2(0, 2),
		                    new Vector2(0, 3)
		                }
		            },

		            {
		                "grass",
		                new List<Vector2>
		                {
		                    new Vector2(1, 0),
		                    new Vector2(1, 1),
		                    new Vector2(1, 2),
		                    new Vector2(1, 3)
		                }
		            },
		        }
		    };

		    var factory = ChunkFactory.Create(_mapTextureConfig);
			_creator = ChunkCreator.CreateWithChunkFactory(factory);
		}

		[Test]
		public void CreateSingleChunkAtCenter()
		{
		    var heights = new[,]
		    {
		        {1}
		    };


		    var squareViews = new[,]
		    {
		        {
		            new SquareViewConfig
		            {
		                TopTextures = new string[] {"grass"},
		                RightTextures = new string[] {"dirt"},
		                FrontTextures = new string[] {"dirt"}
		            }
		        }
		    };


            var chunks = _creator.CreateChunks(heights, squareViews, _mapTextureConfig);

			Assert.AreEqual(1, chunks.Count);
			Assert.AreEqual(3, chunks[0].GetFaces().Count());
			ChunkAssert.IsPositionEqual(new IntVector3(0, 0, 0), chunks[0] as Chunk);

			var topFace = chunks[0].GetFaces().ToList().First();
			var rightFace = chunks[0].GetFaces().ToList().Skip(1).First();
			CollectionAssert.AreEqual(_mapTextureConfig.TextureTiles["grass"], topFace.GetUv());
			CollectionAssert.AreEqual(_mapTextureConfig.TextureTiles["dirt"], rightFace.GetUv());
		}

		[Test]
		public void CreateNotEnoughTextures()
		{
		    var heights = new[,]
		    {
		        {1}
		    };


		    var squareViews = new[,]
		    {
		        {
		            new SquareViewConfig
		            {
		                TopTextures = new string[] {"grass"},
		                RightTextures = new string[] {},
		                FrontTextures = new string[] {"dirt"}
		            }
		        }
		    };


            var chunks = _creator.CreateChunks(heights, squareViews, _mapTextureConfig);

			var expected = new List<Vector2> {Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero};
			var actual = chunks[0].GetFaces().ToList().Skip(1).First().GetUv();
            
			CollectionAssert.AreEqual(expected, actual);
		}

		[Test]
		public void CreateTwoChunks()
		{
		    var heights = new[,]
		    {
		        {1, 1}
		    };

		    var squareViews = new[,]
		    {
		        {
		            new SquareViewConfig
		            {
		                TopTextures = new string[] {"dirt"},
		                RightTextures = new string[] {"dirt"},
		            },
		            new SquareViewConfig
		            {
		                TopTextures = new string[] {"dirt"},
		                RightTextures = new string[] {"dirt"},
		                FrontTextures = new string[] {"dirt"}
		            },
		        }
		    };

            var chunks = _creator.CreateChunks(heights, squareViews, _mapTextureConfig);

			Assert.AreEqual(2, chunks.Count);
			Assert.IsNull((chunks[0] as Chunk).Front);
			Assert.NotNull((chunks[1] as Chunk).Front);
			Assert.NotNull((chunks[0] as Chunk).Right);
			Assert.NotNull((chunks[1] as Chunk).Right);
			ChunkAssert.IsPositionEqual(new IntVector3(0, 0, 0), chunks[0] as Chunk);
			ChunkAssert.IsPositionEqual(new IntVector3(0, 0, 1), chunks[1] as Chunk);
		}

		[Test]
		public void CreateChunksWith4Height()
		{
		    var heights = new[,]
		    {
		        {4}
		    };

		    var squareViews = new[,]
		    {
		        {
		            new SquareViewConfig
		            {
		                TopTextures = new string[] {"dirt"},
		                RightTextures = new string[] {"dirt", "dirt", "dirt", "dirt"},
		                FrontTextures = new string[] {"dirt", "dirt", "dirt", "dirt"}
		            }
		        }
		    };

            var chunks = _creator.CreateChunks(heights, squareViews, _mapTextureConfig);

			Assert.AreEqual(4, chunks.Count);

			Assert.IsNull((chunks[0] as Chunk).Top);
			Assert.IsNull((chunks[1] as Chunk).Top);
			Assert.IsNull((chunks[2] as Chunk).Top);
			Assert.NotNull((chunks[3] as Chunk).Top);

			ChunkAssert.IsPositionEqual(new IntVector3(0, 0, 0), chunks[0] as Chunk);
			ChunkAssert.IsPositionEqual(new IntVector3(0, 1, 0), chunks[1] as Chunk);
			ChunkAssert.IsPositionEqual(new IntVector3(0, 2, 0), chunks[2] as Chunk);
			ChunkAssert.IsPositionEqual(new IntVector3(0, 3, 0), chunks[3] as Chunk);
		}

		[Test]
		public void CreateChunkWith0Height()
		{
		    var heights = new[,]
		    {
		        {0}
		    };

		    var squareViews = new[,]
		    {
		        {
		            new SquareViewConfig()
		        }
		    };

            var chunks = _creator.CreateChunks(heights, squareViews, _mapTextureConfig);

			Assert.AreEqual(0, chunks.Count);
		}

		[Test]
		public void CreateFourChunks()
		{
		    var heights = new[,]
		    {
		        {2, 0},
		        {1, 1}
		    };


		    var squareViews = new[,]
		    {
		        {
		            new SquareViewConfig
		            {
		                TopTextures = new string[] {"dirt"},
		                RightTextures = new string[] {"dirt"},
		                FrontTextures = new string[] {"dirt", "dirt"}
		            },
		            new SquareViewConfig
		            {
		            }
		        },
		        {
		            new SquareViewConfig
		            {
		                TopTextures = new string[] {"dirt"},
		                RightTextures = new string[] {"dirt"},
		            },
		            new SquareViewConfig
		            {
		                TopTextures = new string[] {"dirt"},
		                RightTextures = new string[] {"dirt"},
		                FrontTextures = new string[] {"dirt"}
		            },
		        }
		    };

            var chunks = _creator.CreateChunks(heights, squareViews, _mapTextureConfig);

			Assert.AreEqual(4, chunks.Count);

			Assert.NotNull((chunks[0] as Chunk).Front);
			Assert.IsNull((chunks[0] as Chunk).Right);

			Assert.NotNull((chunks[1] as Chunk).Front);
			Assert.NotNull((chunks[1] as Chunk).Right);

			Assert.IsNull((chunks[2] as Chunk).Front);
			Assert.NotNull((chunks[2] as Chunk).Right);

			Assert.NotNull((chunks[3] as Chunk).Front);
			Assert.NotNull((chunks[3] as Chunk).Right);

			ChunkAssert.IsPositionEqual(new IntVector3(0, 0, 0), chunks[0] as Chunk);
			ChunkAssert.IsPositionEqual(new IntVector3(0, 1, 0), chunks[1] as Chunk);
			ChunkAssert.IsPositionEqual(new IntVector3(1, 0, 0), chunks[2] as Chunk);
			ChunkAssert.IsPositionEqual(new IntVector3(1, 0, 1), chunks[3] as Chunk);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreateHeightsNull()
		{
			_creator.CreateChunks(null, new SquareViewConfig[0,0], new MapTextureConfig());
		}

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateSquareViewsNull()
        {
            _creator.CreateChunks(new int[0,0], null, new MapTextureConfig());
        }

        [Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreateTextureConfigNull()
		{
			_creator.CreateChunks(new int[0,0], new SquareViewConfig[0, 0], null);
		}

		[Test]
		public void CreateEmpty()
		{
		    var heights = new int[,] {{}};
		    var squareViews = new SquareViewConfig[,] {{}};

            var chunks = _creator.CreateChunks(heights, squareViews, _mapTextureConfig);

			Assert.IsEmpty(chunks);
		}

	    [Test]
        [ExpectedException(typeof(InvalidConfigException), ExpectedMessage = "Cannot create chunks because Map and MapView sizes do not match. " +
                                                                             "Map is size (1, 2), MapView is size (1, 1).")]
	    public void CreateMismatchedConfigs()
	    {
	        var heights = new int[,] { { 1, 1 } };

	        var squareViews = new SquareViewConfig[,]
	        {
	            {
	                new SquareViewConfig()
	            }
	        };


            _creator.CreateChunks(heights, squareViews, _mapTextureConfig);
	    }
	}
}
