﻿using System;
using System.Collections.Generic;
using Common.StateMachine.StateRuleBuilders;

namespace Common.StateMachine
{
	public class StateMachine<TKey>
	{
		public delegate StateRule<TKey> BuildRuleDelegate(StateRule<TKey> buildRuleDelegate);

		private Dictionary<TKey, State<TKey>> _states;
		private bool _started;

		public TKey CurrentState { get; private set; }

		public void Init(TKey state)
		{
			CurrentState = state;
			_states = new Dictionary<TKey, State<TKey>>();
		}

		public StateRuleBuilder<TKey> In(TKey state)
		{
			if (_started)
			{
				var message = "Cannot add rule after state machine has started.";
				throw new InvalidOperationException(message);
			}

			var ruleBuilder = StateRuleBuilder<TKey>.Create();
			GetState(state).AddRuleBuilder(ruleBuilder);
			return ruleBuilder;
		}

		private State<TKey> GetState(TKey state)
		{
			if (!_states.ContainsKey(state))
				_states.Add(state, State<TKey>.CreateWithTransitionCallback(state, OnTransition));

			return _states[state];
		}

		public void Start()
		{
			foreach (var state in _states.Values)
			{
				state.BuildRules();
			}

			GetState(CurrentState).Enter();
			_started = true;
		}

		private void OnTransition(TKey targetState)
		{
			GetState(CurrentState).Exit();
			CurrentState = targetState;
			GetState(CurrentState).Enter();
		}
	}
}
