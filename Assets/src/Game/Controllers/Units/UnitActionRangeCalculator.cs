﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Game.Models.Stage;

namespace Game.Controllers.Units
{
    public class UnitActionRangeCalculator : IUnitActionRangeCalculator
    {
        [Inject]
        public IMap Map { get; set; }

        public IEnumerable<GridCoords> GetRange(GridCoords target, Facing direction, IEnumerable<GridCoordsOffset> directionalOffsets)
        {
            var range = new List<GridCoords>();
            foreach (var directionalOffset in directionalOffsets)
            {
                var rotatedOffset = GetDirectionalOffset(direction, directionalOffset);
                var position = target.GetOffset(rotatedOffset);

                if (Map.IsWithinLevel(position))
                {
                    range.Add(position);
                }
            }

            return range;
        }
        private GridCoordsOffset GetDirectionalOffset(Facing direction, GridCoordsOffset offset)
        {
            if (direction == Facing.Forward)
                return offset;
            if (direction == Facing.Right)
                return new GridCoordsOffset(offset.Column * -1, offset.Row);
            if (direction == Facing.Back)
                return new GridCoordsOffset(offset.Row * -1, offset.Column * -1);
            if (direction == Facing.Left)
                return new GridCoordsOffset(offset.Column, offset.Row * -1);

            throw new InvalidOperationException("Invalid direction.");
        }

        public IEnumerable<GridCoords> GetRotatedRange(GridCoords gridCoords, List<GridCoordsOffset> directionalOffsets)
        {
            var range = new HashSet<GridCoords>();

            foreach (var directionalOffset in directionalOffsets)
            {
                var rotatedOffsets = GetRotatedOffsets(directionalOffset);
                foreach (var rotatedOffset in rotatedOffsets)
                {
                    var position = gridCoords.GetOffset(rotatedOffset);
                    if (Map.IsWithinLevel(position))
                    {
                        range.Add(position);
                    }
                }
            }

            return range;
        }

        private IEnumerable<GridCoordsOffset> GetRotatedOffsets(GridCoordsOffset offset)
        {
            return new List<GridCoordsOffset>
            {
                GetDirectionalOffset(Facing.Forward, offset),
                GetDirectionalOffset(Facing.Right, offset),
                GetDirectionalOffset(Facing.Back, offset),
                GetDirectionalOffset(Facing.Left, offset)
            };
        }
    }
}
