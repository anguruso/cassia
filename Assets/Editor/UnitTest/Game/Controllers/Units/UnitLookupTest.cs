﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Controllers.Units;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Controllers.Units
{
    [TestFixture]
    public class UnitLookupTest
    {
        private UnitLookup _unitLookup;

        [SetUp]
        public void SetUp()
        {
            _unitLookup = new UnitLookup();
            _unitLookup.Map = Substitute.For<IMap>();
        }

        [Test]
        public void GetUnitsInArea()
        {
            var square12 = Substitute.For<ISquare>();
            var unit12 = Substitute.For<IUnit>();
            square12.Occupant.Returns(unit12);
            _unitLookup.Map.GetSquare(new GridCoords(1, 2)).Returns(square12);

            var square34 = Substitute.For<ISquare>();
            var unit34 = Substitute.For<IUnit>();
            square34.Occupant.Returns(unit34);
            _unitLookup.Map.GetSquare(new GridCoords(3, 4)).Returns(square34);

            var area = new List<GridCoords>
            {
                new GridCoords(1, 2),
                new GridCoords(3, 4),
                new GridCoords(5, 6)
            };

            var expectedUnits = new List<IUnit> {unit12, unit34};
            var actualUnits = _unitLookup.GetUnits(area);

            CollectionAssert.AreEquivalent(expectedUnits, actualUnits);
        }

        [Test]
        public void DoNotGetOccupantInArea()
        {
            var square = Substitute.For<ISquare>();
            var occupant = Substitute.For<IOccupant>();
            square.Occupant.Returns(occupant);
            _unitLookup.Map.GetSquare(new GridCoords(1, 1)).Returns(square);

            var area = new List<GridCoords> {new GridCoords(1, 1)};

            CollectionAssert.IsEmpty(_unitLookup.GetUnits(area));
        }


    }
}
