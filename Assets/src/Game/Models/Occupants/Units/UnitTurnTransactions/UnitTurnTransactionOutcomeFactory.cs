﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units.UnitActions;
using UnityEngine.Assertions;

namespace Game.Models.Occupants.Units.UnitTurnTransactions
{
    public class UnitTurnTransactionOutcomeFactory : IUnitTurnTransactionOutcomeFactory
    {
        [Inject]
        public IUnitLookup UnitLookup { get; set; }

        [Inject]
        public IDirectionCalculator DirectionCalculator { get; set; }

		[Inject]
	    public IUnitActionOutcomeFactory UnitActionOutcomeFactory { get; set; }

	    public IUnitTurnTransactionOutcome GetOutcome(IUnitActionOrigin unitActionOrigin, IUnitAction unitAction, GridCoords targetPosition)
        {
			var actor = unitActionOrigin.Actor;

			var outcomes = new List<IUnitActionOutcome>();
		    var selfEffects = unitAction.SelfEffectBuilders;

		    if (selfEffects.Any())
		    {
				outcomes.Add(UnitActionOutcomeFactory.CreateUnitActionOutcome(actor.Stats, actor, selfEffects));
		    }

			var direction = DirectionCalculator.GetFacing(unitActionOrigin.ActorPosition, targetPosition);
			var areaOfEffect = unitAction.GetAreaOfEffect(targetPosition, direction).ToList();
			var affectedUnits = UnitLookup.GetUnits(areaOfEffect);
		    var targetEffects = unitAction.TargetEffectBuilders;

		    if (targetEffects.Any())
		    {
				foreach (var affectedUnit in affectedUnits)
				{
					outcomes.Add(UnitActionOutcomeFactory.CreateUnitActionOutcome(actor.Stats, affectedUnit, targetEffects));
				}
			}

			return UnitTurnTransactionOutcome.Create(areaOfEffect, outcomes);
		}
    }
}
