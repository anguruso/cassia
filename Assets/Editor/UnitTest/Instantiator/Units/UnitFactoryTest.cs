﻿using Config;
using Game.Models.Occupants.Units;
using Instantiator.Units;
using NSubstitute;
using NUnit.Framework;
using strange.extensions.injector.api;

namespace UnitTest.Builder.Units
{
    [TestFixture]
    public class UnitFactoryTest
    {
        private UnitFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _factory = new UnitFactory();
            _factory.InjectionBinder = Substitute.For<IInjectionBinder>();
        }

        [Test]
        public void CreateUnit()
        {
            var expectedUnit = Substitute.For<IInjectableUnit>();
            _factory.InjectionBinder.GetInstance<IInjectableUnit>().Returns(expectedUnit);

            var unitConfig = new UnitConfig();
            var actualUnit = _factory.CreateUnit(unitConfig);

            expectedUnit.Received(1).LoadConfig(unitConfig);
            Assert.AreSame(expectedUnit, actualUnit);
        }
    }
}
