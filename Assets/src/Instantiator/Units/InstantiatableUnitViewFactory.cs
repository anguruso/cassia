﻿using Config;

namespace Instantiator.Units
{
    public class InstantiatableUnitViewFactory : IInstantiatableUnitViewFactory
    {
        [Inject]
        public IUnitFactory UnitFactory { get; set; }

        public IInstantiatableUnitView Build(LevelUnitConfig config, UnitViewConfig viewConfig)
        {
            var buildableUnitView = new InstantiatableUnitView();
            var unit = UnitFactory.CreateUnit(config.Unit);

            buildableUnitView.Unit = unit;
            buildableUnitView.StartPosition = config.StartPosition;
            buildableUnitView.PrefabPath = viewConfig.PrefabPath;

            return buildableUnitView;
        }
    }
}
