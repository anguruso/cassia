﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Game.Commands;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.Occupants.Units.UnitTurnTransactions;
using Game.Models.Stage;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Commands
{
    [TestFixture]
    public class ApplyUnitTurnTransactionCommandTest
    {
        private ApplyUnitTurnTransactionCommand _command;

        [SetUp]
        public void SetUp()
        {
            _command = new ApplyUnitTurnTransactionCommand();
            _command.UnitTurnTransaction = Substitute.For<IUnitTurnTransaction>();
            _command.Map = Substitute.For<IMap>();
        }

        [Test]
        public void Execute()
        {
			var actor = Substitute.For<IUnit>();
			_command.UnitTurnTransaction.Actor.Returns(actor);

			var unitAction = Substitute.For<IUnitAction>();
			_command.UnitTurnTransaction.UnitAction.Returns(unitAction);
			_command.UnitTurnTransaction.Facing.Returns(Facing.Left);

			_command.UnitTurnTransaction.MovePosition.Returns(new GridCoords(1, 2));

			var affectedUnit0 = Substitute.For<IUnit>();
			var effects0 = new List<IUnitActionEffect> { Substitute.For<IUnitActionEffect>(), Substitute.For<IUnitActionEffect>() };
			var affectedUnit1 = Substitute.For<IUnit>();
			var effects1 = new List<IUnitActionEffect> { Substitute.For<IUnitActionEffect>() };

			var outcomes = new List<IUnitActionOutcome>
			{
				new UnitActionOutcome { AffectedUnit = affectedUnit0, Effects = effects0 },
				new UnitActionOutcome { AffectedUnit = affectedUnit1, Effects = effects1 }
			};
			var transactionOutcome = Substitute.For<IUnitTurnTransactionOutcome>();
			transactionOutcome.UnitActionOutcomes.Returns(outcomes);
			_command.UnitTurnTransaction.Outcome.Returns(transactionOutcome);
			_command.UnitTurnTransaction.IsReady.Returns(true);

			_command.Execute();

			_command.Map.Received(1).SetOccupant(actor, new GridCoords(1, 2));
			actor.Received(1).Facing = Facing.Left;
			affectedUnit0.Received(1).ApplyUnitActionEffect(effects0[0], actor.Stats);
			affectedUnit0.Received(1).ApplyUnitActionEffect(effects0[1], actor.Stats);
			affectedUnit1.Received(1).ApplyUnitActionEffect(effects1[0], actor.Stats);
        }

        [Test]
		[ExpectedException(typeof(ArgumentException), ExpectedMessage = "Cannot apply UnitTurnTransaction if it is not ready.")]
        public void ExecuteWhenUnitTurnTransactionNotReady()
        {
	        _command.UnitTurnTransaction.IsReady.Returns(false);
			_command.Execute();
        }
    }
}
