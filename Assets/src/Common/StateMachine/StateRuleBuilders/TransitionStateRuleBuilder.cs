using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.StateMachine.StateRuleBuilders
{
	public class TransitionStateRuleBuilder<TKey> : StateRuleBuilder<TKey>
	{
	    private Func<bool> _predicate; 

		private TransitionStateRuleBuilder() { }

		public static TransitionStateRuleBuilder<TKey> Create(List<StateMachine<TKey>.BuildRuleDelegate> buildActions, Func<bool> predicate)
		{
			return new TransitionStateRuleBuilder<TKey>
			{
                _predicate = predicate,
				_buildActions = buildActions
			};
		}

		public TransitionStateRuleBuilder<TKey> Execute(Action action)
		{
			_buildActions.Add(rule =>
			{
				((StateRule<TKey>)rule).ConditionalTransitions.Last().TransitionAction = action;
				return rule;
			});
			return Create(_buildActions, _predicate);
		}
	}

	public class TransitionStateRuleBuilder<TKey, T0> : StateRuleBuilder<TKey>
	{
		private Func<T0, bool> _predicate;

		private TransitionStateRuleBuilder() { }

		public static TransitionStateRuleBuilder<TKey, T0> Create(List<StateMachine<TKey>.BuildRuleDelegate> buildActions, Func<T0, bool> predicate)
		{
			return new TransitionStateRuleBuilder<TKey, T0>
			{
				_predicate = predicate,
				_buildActions = buildActions
			};
		}

		public PreTerminatingStateRuleBuilder<TKey, T0> GoTo(TKey targetState)
		{
			_buildActions.Add(rule =>
			{
				((StateRule<TKey, T0>)rule).AddTransition(targetState, x => true);
				return rule;
			});
			return PreTerminatingStateRuleBuilder<TKey, T0>.Create(_buildActions);
		}

		public GuardedStateRuleBuilder<TKey, T0> If(Func<T0, bool> predicate)
		{
			return GuardedStateRuleBuilder<TKey, T0>.CreateFromPredicate(_buildActions, predicate);
		}

		public TransitionStateRuleBuilder<TKey, T0> Execute(Action<T0> action)
		{
			_buildActions.Add(rule =>
			{
				((StateRule<TKey, T0>)rule).ConditionalTransitions.Last().TransitionAction = action;
				return rule;
			});
			return Create(_buildActions, _predicate);
		}
	}

    public class TransitionStateRuleBuilder<TKey, T0, T1> : StateRuleBuilder<TKey>
    {
        private Func<T0, T1, bool> _predicate;

        private TransitionStateRuleBuilder() { }

        public static TransitionStateRuleBuilder<TKey, T0, T1> Create(List<StateMachine<TKey>.BuildRuleDelegate> buildActions, Func<T0, T1, bool> predicate)
        {
            return new TransitionStateRuleBuilder<TKey, T0, T1>
            {
                _predicate = predicate,
                _buildActions = buildActions
            };
        }

        public PreTerminatingStateRuleBuilder<TKey, T0, T1> GoTo(TKey targetState)
        {
            _buildActions.Add(rule =>
            {
                ((StateRule<TKey, T0, T1>)rule).AddTransition(targetState, (x, y) => true);
                return rule;
            });
            return PreTerminatingStateRuleBuilder<TKey, T0, T1>.Create(_buildActions);
        }

        public GuardedStateRuleBuilder<TKey, T0, T1> If(Func<T0, T1, bool> predicate)
        {
            return GuardedStateRuleBuilder<TKey, T0, T1>.CreateFromPredicate(_buildActions, predicate);
        }

        public TransitionStateRuleBuilder<TKey, T0, T1> Execute(Action<T0, T1> action)
        {
            _buildActions.Add(rule =>
            {
                ((StateRule<TKey, T0, T1>)rule).ConditionalTransitions.Last().TransitionAction = action;
                return rule;
            });
            return Create(_buildActions, _predicate);
        }
    }
}