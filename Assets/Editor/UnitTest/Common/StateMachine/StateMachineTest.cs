﻿using System;
using System.Collections.Generic;
using Common.StateMachine;
using NUnit.Framework;
using strange.extensions.signal.impl;

namespace UnitTest.Common.StateMachine
{
	[TestFixture]
	public class StateMachineTest
	{
		private enum TestState { Stopped, Walking, Running }

		[Test]
		public void SimpleTransition()
		{
			var stateMachine = new StateMachine<TestState>();
			stateMachine.Init(TestState.Stopped);

			var speedUpSignal = new Signal();
			stateMachine.In(TestState.Stopped).On(speedUpSignal).GoTo(TestState.Walking);
			stateMachine.In(TestState.Walking).On(speedUpSignal).GoTo(TestState.Running);

			stateMachine.Start();

			Assert.AreEqual(TestState.Stopped, stateMachine.CurrentState);
			speedUpSignal.Dispatch();
			Assert.AreEqual(TestState.Walking, stateMachine.CurrentState);
			speedUpSignal.Dispatch();
			Assert.AreEqual(TestState.Running, stateMachine.CurrentState);
		}

		[Test]
		public void CreateTransitionsInReverseOrder()
		{
			var stateMachine = new StateMachine<TestState>();
			stateMachine.Init(TestState.Walking);

			var slowDownSignal = new Signal();
			var speedUpSignal = new Signal();
			stateMachine.In(TestState.Walking).On(slowDownSignal).GoTo(TestState.Stopped);
			stateMachine.In(TestState.Walking).On(speedUpSignal).GoTo(TestState.Running);
			stateMachine.Start();

			speedUpSignal.Dispatch();
			slowDownSignal.Dispatch();

			Assert.AreEqual(TestState.Running, stateMachine.CurrentState);
		}

		[Test]
		public void DoNotTransitionOnWrongState()
		{
			var stateMachine = new StateMachine<TestState>();
			stateMachine.Init(TestState.Stopped);

			var runSignal = new Signal();
			stateMachine.In(TestState.Walking).On(runSignal).GoTo(TestState.Running);
			stateMachine.Start();

			runSignal.Dispatch();

			Assert.AreEqual(TestState.Stopped, stateMachine.CurrentState);
		}

		[Test]
		public void TransitionWithAction()
		{
			var stateMachine = new StateMachine<TestState>();
			stateMachine.Init(TestState.Walking);

			var runSignal = new Signal();
			var executed = false;
			stateMachine.In(TestState.Walking).On(runSignal).GoTo(TestState.Running).Execute(() => { executed = true; });
			stateMachine.Start();

			Assert.IsFalse(executed);
			runSignal.Dispatch();
			Assert.IsTrue(executed);
		}

		[Test]
		public void TransitionWithEntryAction()
		{
			var stateMachine = new StateMachine<TestState>();
			stateMachine.Init(TestState.Stopped);

			var walkSignal = new Signal();
			var entryCount = 0;
			stateMachine.In(TestState.Walking).OnEntry(() => { entryCount++; });
			stateMachine.In(TestState.Walking).OnEntry(() => { entryCount++; });
			stateMachine.In(TestState.Stopped).On(walkSignal).GoTo(TestState.Walking);
			stateMachine.Start();

			walkSignal.Dispatch();

			Assert.AreEqual(2, entryCount);
		}

		[Test]
		public void TransitionWithExitAction()
		{
			var stateMachine = new StateMachine<TestState>();
			stateMachine.Init(TestState.Stopped);

			var walkSignal = new Signal();
			var entryCount = 0;
			stateMachine.In(TestState.Stopped).OnExit(() => { entryCount++; });
			stateMachine.In(TestState.Stopped).OnExit(() => { entryCount++; });
			stateMachine.In(TestState.Stopped).On(walkSignal).GoTo(TestState.Walking);
			stateMachine.Start();

			walkSignal.Dispatch();

			Assert.AreEqual(2, entryCount);
		}

		[Test]
		public void TransitionWithGuard()
		{
            var stateMachine = new StateMachine<TestState>();
            stateMachine.Init(TestState.Stopped);

            var walkSignal = new Signal();
            stateMachine.In(TestState.Stopped).On(walkSignal).If(() => false).GoTo(TestState.Walking);

		    var runSignal = new Signal();
            stateMachine.In(TestState.Stopped).On(runSignal).If(() => true).GoTo(TestState.Running);

            stateMachine.Start();

            Assert.AreEqual(TestState.Stopped, stateMachine.CurrentState);
            walkSignal.Dispatch();
            Assert.AreEqual(TestState.Stopped, stateMachine.CurrentState);
            runSignal.Dispatch();
            Assert.AreEqual(TestState.Running, stateMachine.CurrentState);
        }
	}
}
