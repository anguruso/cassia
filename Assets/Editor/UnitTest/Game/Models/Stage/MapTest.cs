﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Config;
using Game.Models.Stage;
using NUnit.Framework;

namespace UnitTest.Game.Models.Stage
{
	[TestFixture]
	public class MapTest
	{
		[Test]
		public void Create()
		{
			var heights = new[,]
            {
                {1, 2, 3},
                {4, 5, 6}
            };

            var map = new Map();
			map.LoadHeights(heights);

			Assert.AreEqual(1, map.GetSquare(new GridCoords(0, 0)).Height);
			Assert.AreEqual(2, map.GetSquare(new GridCoords(0, 1)).Height);
			Assert.AreEqual(3, map.GetSquare(new GridCoords(0, 2)).Height);
			Assert.AreEqual(4, map.GetSquare(new GridCoords(1, 0)).Height);
			Assert.AreEqual(5, map.GetSquare(new GridCoords(1, 1)).Height);
			Assert.AreEqual(6, map.GetSquare(new GridCoords(1, 2)).Height);
		}

		[Test]
		[ExpectedException(typeof(IndexOutOfRangeException), 
			ExpectedMessage = "Cannot get square (2, 1) because level is of size (1, 1).")]
		public void GetSquareOutOfRange()
		{
			var heights = new[,]
            {
                { 1 }
            };

            var map = new Map();
			map.LoadHeights(heights);
			map.GetSquare(new GridCoords(2, 1));
		}

		[Test]
		public void GetAdjacentSquares()
		{
			var heights = new[,]
            {
                { 1, 2, 3 },
                { 4, 5, 6 }
            };

            var map = new Map();
			map.LoadHeights(heights);

			var expected = new List<GridCoords>
			{
				new GridCoords(0, 0),
				new GridCoords(0, 2),
				new GridCoords(1, 1)
			};
			var actual = map.GetAdjacentGridCoords(new GridCoords(0, 1));
			CollectionAssert.AreEquivalent(expected, actual);
		}
	}
}
