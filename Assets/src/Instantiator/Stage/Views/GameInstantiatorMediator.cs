﻿using Common.DataContainers;
using Game.Controllers;
using Instantiator.Stage.Signals.ViewSignals;
using Instantiator.Units;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Instantiator.Stage.Views
{
	public class GameInstantiatorMediator : Mediator
	{
		[Inject]
		public GameInstantiatorView View { get; set; }

		[Inject]
		public IInstantiateMapViewSignal InstantiateMapViewSignal { get; set; } 

		[Inject]
		public IInstantiateUnitViewSignal InstantiateUnitViewSignal { get; set; }

		[Inject]
		public IWorldGridCoordsConverter WorldGridCoordsConverter { get; set; }

		public override void OnRegister()
		{
			InstantiateMapViewSignal.AddListener(BuildLevel);
			InstantiateUnitViewSignal.AddListener(BuildUnit);
		}

		private void BuildLevel(Mesh mesh)
		{
			View.BuildLevel(mesh);
		}

		private void BuildUnit(IInstantiatableUnitView instantiatableUnit, GridCoords position)
		{
			var worldPosition = WorldGridCoordsConverter.GetWorldPosition(position);
			var rotation = WorldGridCoordsConverter.GetRotation(instantiatableUnit.Unit.Facing); 
			View.BuildUnit(instantiatableUnit, worldPosition, rotation);
		}
	}
}
