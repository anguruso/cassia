using Common.Signals;
using Common.StateMachine;
using NUnit.Framework;
using strange.extensions.signal.impl;

namespace UnitTest.Common.StateMachine
{
    [TestFixture]
    public class StateMachineTwoParamsSignalTest
    {
        private enum TestState { Stopped, Walking, Running, Jumping }

        private readonly SpeedUpSignal _speedUpSignal = new SpeedUpSignal();
        private class SpeedUpSignal : Signal<int, bool>, ISignal<int, bool> { }

        [Test]
        public void Transition()
        {
            var stateMachine = new StateMachine<TestState>();
            stateMachine.Init(TestState.Stopped);

            stateMachine.In(TestState.Stopped).On(_speedUpSignal).GoTo(TestState.Walking);
            stateMachine.Start();

            _speedUpSignal.Dispatch(0, true);
            Assert.AreEqual(TestState.Walking, stateMachine.CurrentState);
        }

        [Test]
        public void TransitionWithGuard()
        {
            var stateMachine = new StateMachine<TestState>();
            stateMachine.Init(TestState.Stopped);

            stateMachine.In(TestState.Stopped)
                .On(_speedUpSignal).If((x, y) => x > 0).GoTo(TestState.Walking);
            stateMachine.Start();

            _speedUpSignal.Dispatch(0, true);
            Assert.AreEqual(TestState.Stopped, stateMachine.CurrentState);

            _speedUpSignal.Dispatch(1, true);
            Assert.AreEqual(TestState.Walking, stateMachine.CurrentState);
        }

        [Test]
        public void TransitionWithMultipleGuards()
        {
            var stateMachine = new StateMachine<TestState>();
            stateMachine.Init(TestState.Stopped);

            stateMachine.In(TestState.Stopped)
                .On(_speedUpSignal).If((x, y) => x == 0).GoTo(TestState.Walking)
                .If((x, y) => x > 0).GoTo(TestState.Running);
            stateMachine.Start();

            _speedUpSignal.Dispatch(1, true);
            Assert.AreEqual(TestState.Running, stateMachine.CurrentState);
        }

        [Test]
        public void TransitionToUnguardedWithAction()
        {
            var stateMachine = new StateMachine<TestState>();
            var executed = false;

            stateMachine.Init(TestState.Stopped);
            stateMachine.In(TestState.Stopped)
                .On(_speedUpSignal).If((x, y) => x == 0).GoTo(TestState.Walking)
                .If((x, y) => x > 0).GoTo(TestState.Running)
                .GoTo(TestState.Jumping).Execute((x, y) => { executed = true; });
            stateMachine.Start();

            _speedUpSignal.Dispatch(-1, true);
            Assert.AreEqual(TestState.Jumping, stateMachine.CurrentState);
            Assert.IsTrue(executed);
        }

        [Test]
        public void TransitionWithAction()
        {
            var stateMachine = new StateMachine<TestState>();
            stateMachine.Init(TestState.Stopped);

            var speed = 0;

            stateMachine.In(TestState.Stopped).On(_speedUpSignal).GoTo(TestState.Jumping).Execute((x, y) => { speed = x; });
            stateMachine.Start();

            Assert.AreEqual(0, speed);
            _speedUpSignal.Dispatch(2, true);
            Assert.AreEqual(2, speed);
        }

        [Test]
        public void TransitionWithEntryAction()
        {
            var stateMachine = new StateMachine<TestState>();
            stateMachine.Init(TestState.Stopped);

            var entered = false;

            stateMachine.In(TestState.Walking).OnEntry(() => { entered = true; });
            stateMachine.In(TestState.Stopped).On(_speedUpSignal).GoTo(TestState.Walking);
            stateMachine.Start();

            _speedUpSignal.Dispatch(0, true);

            Assert.IsTrue(entered);
        }

        [Test]
        public void TransitionWithExitAction()
        {
            var stateMachine = new StateMachine<TestState>();
            stateMachine.Init(TestState.Stopped);

            var exited = false;

            stateMachine.In(TestState.Stopped).OnExit(() => { exited = true; });
            stateMachine.In(TestState.Stopped).On(_speedUpSignal).GoTo(TestState.Walking);
            stateMachine.Start();

            _speedUpSignal.Dispatch(0, true);

            Assert.IsTrue(exited);
        }
    }
}