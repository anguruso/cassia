﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;

namespace Game.Models.Stage
{
    public class SelectableSquares : ISelectableSquares
    {
        private List<GridCoords> _selectedSquares;

        public SelectableSquares()
        {
            _selectedSquares = new List<GridCoords>();
        }

        public void Set(IEnumerable<GridCoords> squares)
        {
            _selectedSquares = squares.ToList();
        }

        public IEnumerable<GridCoords> Get()
        {
            return _selectedSquares;
        }

        public void Clear()
        {
            _selectedSquares.Clear();
        }
    }
}
