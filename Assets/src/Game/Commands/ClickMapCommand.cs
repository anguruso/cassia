﻿using System.Diagnostics;
using Common.DataContainers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using Game.Signals;
using Game.Signals.ControllerSignals;
using strange.extensions.command.impl;

namespace Game.Commands
{
	public class ClickMapCommand : Command
	{
		[Inject]
		public GridCoords TargetCoords { get; set; }

		[Inject]
		public IMap Map { get; set; }

        [Inject]
	    public ITriggerMapSignal TriggerMapSignal { get; set; }

        [Inject]
	    public ITriggerUnitSignal TriggerUnitSignal { get; set; }

	    public override void Execute()
	    {
			var square = Map.GetSquare(TargetCoords);
			if (ContainsSelectableUnit(square) && square.Occupant is IUnit) //ew
				TriggerUnitSignal.Dispatch((IUnit) square.Occupant, TargetCoords);
			else
                TriggerMapSignal.Dispatch(TargetCoords); // TODO maybe you can select the SQUARE.
		}

		private bool ContainsSelectableUnit(ISquare square)
		{
			return square.IsOccupied && square.Occupant.IsSelectable;
		}
	}
}
