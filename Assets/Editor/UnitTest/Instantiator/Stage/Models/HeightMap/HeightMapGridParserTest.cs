﻿using System;
using Common.DataContainers;
using Instantiator.Stage.Models.HeightMap;
using NUnit.Framework;

namespace UnitTest.StageBuilder.Models.HeightMap
{
    [TestFixture]
    public class HeightMapGridParserTest
    {
        private HeightMapParser _heightMapParser;

        [SetUp]
        public void SetUp()
        {
            _heightMapParser = HeightMapParser.Create();
        }

	    [Test]
	    public void Parse()
	    {
		    var heightMap = new int[,]
		    {
				{1, 4, 3},
				{2, 1, 0}
		    };
		    var heightMapGrid = _heightMapParser.Parse(heightMap);

		    for (var row = 0; row < heightMap.GetLength(0); ++row)
		    {
			    for (var column = 0; column < heightMap.GetLength(1); ++column)
			    {
					GridCoords coords = new GridCoords(row, column);
				    Assert.AreEqual(heightMap[row, column], heightMapGrid.GetSquare(coords).Height);
			    }
		    }
	    }

        [Test]
        public void ParseEmpty()
        {
            var empty = new int[0, 0];
            var heightMapGrid = _heightMapParser.Parse(empty);

            Assert.AreEqual(0, heightMapGrid.Width);
            Assert.AreEqual(0, heightMapGrid.Depth);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ParseNull()
        {
            _heightMapParser.Parse(null);
        }
    }
}
