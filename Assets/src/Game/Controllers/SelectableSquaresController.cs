﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Stage;
using Game.Signals.ViewSignals;

namespace Game.Controllers
{
    public class SelectableSquaresController : ISelectableSquaresController
    {
        [Inject]
        public ISelectableSquares SelectableSquares { get; set; }

        [Inject]
        public IShowMovementRangeViewSignal ShowMovementRangeViewSignal { get; set; }

        [Inject]
        public IHideMovementRangeViewSignal HideMovementRangeViewSignal { get; set; }

        public void Set(IEnumerable<GridCoords> highlightedSquares)
        {
            SelectableSquares.Set(highlightedSquares);

            HideMovementRangeViewSignal.Dispatch();
            ShowMovementRangeViewSignal.Dispatch(highlightedSquares);
        }

        public IEnumerable<GridCoords> Get()
        {
            return SelectableSquares.Get();
        } 

        public void Clear()
        {
            SelectableSquares.Clear();

            HideMovementRangeViewSignal.Dispatch();
        }
    }
}
