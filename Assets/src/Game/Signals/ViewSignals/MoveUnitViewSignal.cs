﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Common.DataContainers;
using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.ViewSignals
{
	public class MoveUnitViewSignal : Signal<IEnumerable<GridCoords>>, IMoveUnitViewSignal
	{
	}

	public interface IMoveUnitViewSignal : ISignal<IEnumerable<GridCoords>>
	{
	}
}
