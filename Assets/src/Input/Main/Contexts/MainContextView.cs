﻿using strange.extensions.context.impl;

namespace Input.Main.Contexts
{
	public class MainContextView : ContextView
	{
		private void Awake()
		{
			context = new RootContext(this, true);
			context.Start();
		}
	}
}
