﻿namespace Game.Models.Occupants.Units.UnitActions
{
	public class UnitActionEffect : IUnitActionEffect
	{
		public static UnitActionEffect CreateWithStats(IUnitActionEffectBuilder effectBuilder, IUnitStats actorStats, IUnitStats affectedUnitStats)
		{
			return new UnitActionEffect
			{
				UnitActionId = effectBuilder.UnitActionId,
				Accuracy = effectBuilder.Accuracy,
				AbnormalStatus = effectBuilder.AbnormalStatus,
				Duration = effectBuilder.Duration,
				StatType = effectBuilder.StatType,
				IsStackable = effectBuilder.IsRemoveable,
				IsRemoveable = effectBuilder.IsRemoveable,
				Delta = effectBuilder.GetDelta(actorStats, affectedUnitStats)
			};
		}

		public string UnitActionId { get; private set; }
		public float Accuracy { get; private set; }
		public AbnormalStatusType? AbnormalStatus { get; private set; }
		public int? Duration { get; private set; }
		public StatType StatType { get; private set; }
		public bool IsStackable { get; private set; }
		public bool IsRemoveable { get; private set; }
		public int Delta { get; private set; }
	}
}