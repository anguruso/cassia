﻿using Config;
using Game.Models.Occupants.Units.UnitActions;

namespace Instantiator.Units
{
    public interface IUnitActionFactory
    {
        IUnitAction CreateUnitAction(UnitActionConfig config);
    }
}