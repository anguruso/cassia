﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Config;
using Game.Models.Occupants;

namespace Game.Models.Stage
{
	public class Map : IMap
	{
		private Square[,] _squares;
		private Dictionary<IOccupant, GridCoords> _occupants;

		public void LoadHeights(int[,] heights)
		{
			var depth = heights.GetLength(0);
			var width = heights.GetLength(1);

			_squares = new Square[depth, width];

			for (var row = 0; row < depth; ++ row)
			{
				for (var column = 0; column < width; ++column)
				{
					var squareConfig = heights[row, column];
					_squares[row, column] = Square.CreateWithHeight(squareConfig);
				}
			}

			_occupants = new Dictionary<IOccupant, GridCoords>();
		}

		public int Depth { get { return _squares.GetLength(0); } }
		public int Width { get { return _squares.GetLength(1); } }

		public bool HasOccupant(IOccupant occupant)
		{
			return _occupants.ContainsKey(occupant);
		}

		public ISquare GetSquare(GridCoords gridCoords)
		{
			if (!IsWithinLevel(gridCoords))
			{
				var message = string.Format("Cannot get square ({0}, {1}) because level is of size ({2}, {3}).", gridCoords.Row, gridCoords.Column, Depth, Width);
				throw new IndexOutOfRangeException(message);
			}

			return _squares[gridCoords.Row, gridCoords.Column];
		}

		public bool IsWithinLevel(GridCoords gridCoords)
		{
			return gridCoords.Row >= 0 && gridCoords.Row < Depth && gridCoords.Column >= 0 && gridCoords.Column < Width;
		}

		public GridCoords GetGridCoords(IOccupant occupant)
		{
			if (occupant == null) throw new ArgumentNullException("occupant");
			if (!_occupants.ContainsKey(occupant)) throw new ArgumentException("Cannot get GridCoords for an occupant that is not on the map.");

			return _occupants[occupant];
		}

		public void SetOccupant(IOccupant occupant, GridCoords gridCoords)
		{
			var targetSquare = GetSquare(new GridCoords(gridCoords.Row, gridCoords.Column));

			if (targetSquare.Occupant == occupant) return;
			if (targetSquare.IsOccupied)
			{
				var message = string.Format("Cannot set occupant at {0} because this square is occupied by {1}.", gridCoords, targetSquare.Occupant);
				throw new InvalidOperationException(message);
			}

			targetSquare.SetOccupant(occupant);

			if (_occupants.ContainsKey(occupant))
			{
				GetSquare(_occupants[occupant]).RemoveOccupant();
				_occupants[occupant] = gridCoords;
			}
			else
			{
				_occupants.Add(occupant, gridCoords);
			}
		}

		public IEnumerable<GridCoords> GetAdjacentGridCoords(GridCoords gridCoords)
		{
			var adjacentGridCoords = new List<GridCoords>
			{
				new GridCoords(gridCoords.Row - 1, gridCoords.Column),
				new GridCoords(gridCoords.Row + 1, gridCoords.Column),
				new GridCoords(gridCoords.Row, gridCoords.Column - 1),
				new GridCoords(gridCoords.Row, gridCoords.Column + 1)
			};

			return adjacentGridCoords.Where(x => IsWithinLevel(x));
		}
	}
}
