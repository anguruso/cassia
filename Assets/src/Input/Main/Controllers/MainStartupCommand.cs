﻿using strange.extensions.command.impl;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Input.Main.Controllers
{
	public class MainStartupCommand : Command
	{
		public override void Execute()
		{
			SceneManager.LoadScene("game");
		}
	}
}
