﻿using System.Collections.Generic;
using Common.DataContainers;
using Config;

namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IUnitAction
	{
        string Description { get; }
        IEnumerable<GridCoordsOffset> EffectiveRange { get; }
        string Name { get; }
        IEnumerable<GridCoordsOffset> TargetRange { get; }
		IEnumerable<IUnitActionEffectBuilder> SelfEffectBuilders { get; }
		IEnumerable<IUnitActionEffectBuilder> TargetEffectBuilders { get; }
        IEnumerable<GridCoords> GetTargetRange(GridCoords target);
        IEnumerable<GridCoords> GetAreaOfEffect(GridCoords target, Facing direction);
        void LoadConfig(UnitActionConfig config);
    }
}