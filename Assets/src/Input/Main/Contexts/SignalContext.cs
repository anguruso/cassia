﻿using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using UnityEngine;

namespace Input.Main.Contexts
{
	public class SignalContext : MVCSContext
	{
		public SignalContext() : base()
			{
		}

		public SignalContext(MonoBehaviour view, bool autoStartup = true) : base(view, autoStartup)
		{

		}

		protected override void addCoreComponents()
		{
			base.addCoreComponents();
			injectionBinder.Unbind<ICommandBinder>();
			injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
		}
	}
}
