﻿using System;
using Config;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using NUnit.Framework;

namespace UnitTest.Game.Models.Stage
{
	[TestFixture]
	public class SquareTest
	{
		private Square _square;

		[SetUp]
		public void SetUp()
		{
			_square =  Square.CreateWithHeight(1);
		}

		[Test]
		public void Create()
		{
			Assert.AreEqual(1, _square.Height);
			Assert.IsFalse(_square.IsOccupied);
		}

		[Test]
		public void IsAllyPathable()
		{
			var allyTeam = Affiliation.CreateWithName("Ally");
			var allyCharacter = new Unit {Affiliation = allyTeam};
			var character = new Unit { Affiliation = allyTeam };

			_square.SetOccupant(allyCharacter);

			Assert.IsTrue(_square.IsOccupied);
			Assert.IsTrue(_square.IsPathableBy(character));
		}

		[Test]
		public void UnaffiliatedCharacterNeverPathable()
		{
			var character = new Unit();
			var otherCharacter = new Unit();
			_square.SetOccupant(character);

			Assert.IsFalse(_square.IsPathableBy(otherCharacter));
		}

		[Test]
		public void IsNonAllyUnpathable()
		{
			var nonAllyCharacter = new Unit();

			_square.SetOccupant(nonAllyCharacter);

			var allyTeam = Affiliation.CreateWithName("Ally");
			var character = new Unit {Affiliation = allyTeam};

			Assert.IsTrue(_square.IsOccupied);
			Assert.IsFalse(_square.IsPathableBy(character));
		}

		[Test]
		public void SetOccupant()
		{
			var unit = new Unit();
			_square.SetOccupant(unit);

			Assert.IsTrue(_square.IsOccupied);
		}

		[Test]
		public void SetOccupantToSelf()
		{
			var unit = new Unit();
			_square.SetOccupant(unit);
			_square.SetOccupant(unit);

			Assert.IsTrue(_square.IsOccupied);
		}

		[Test]
		public void RemoveOccupant()
		{
			var unitModel = new Unit();
			_square.SetOccupant(unitModel);
			_square.RemoveOccupant();

			Assert.IsFalse(_square.IsOccupied);
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException), ExpectedMessage = "Cannot set occupant on square occupied by Unit(Unnamed).")]
		public void SetOccupantOnOccupied()
		{
			var unitModel = new Unit();
			_square.SetOccupant(unitModel);

			var unitModelNew = new Unit();
			_square.SetOccupant(unitModelNew);
		}
	}
}
