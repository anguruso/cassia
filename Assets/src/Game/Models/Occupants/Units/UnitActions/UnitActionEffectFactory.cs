namespace Game.Models.Occupants.Units.UnitActions
{
	public class UnitActionEffectFactory : IUnitActionEffectFactory
	{
		public IUnitActionEffect CreateUnitActionEffect(IUnitActionEffectBuilder effectBuilder, IUnitStats actorStats, IUnitStats affectedUnitStats)
		{
			return UnitActionEffect.CreateWithStats(effectBuilder, actorStats, affectedUnitStats);
		}
	}
}