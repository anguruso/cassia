﻿using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    public class MapTextureConfig
    {
        public Dictionary<string,List<Vector2>> TextureTiles { get; set; }
    }
}
