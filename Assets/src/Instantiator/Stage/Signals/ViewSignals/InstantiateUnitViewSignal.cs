﻿using Common.DataContainers;
using Common.Signals;
using Instantiator.Units;
using strange.extensions.signal.impl;

namespace Instantiator.Stage.Signals.ViewSignals
{
	public class InstantiateUnitViewSignal : Signal<IInstantiatableUnitView, GridCoords>, IInstantiateUnitViewSignal
	{
	}

	public interface IInstantiateUnitViewSignal : ISignal<IInstantiatableUnitView, GridCoords>
	{
	}
}
