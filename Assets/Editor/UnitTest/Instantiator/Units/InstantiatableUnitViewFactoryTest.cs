﻿using System.Collections.Generic;
using Common.DataContainers;
using Common.Exceptions;
using Config;
using Game.Models.Occupants.Units;
using Instantiator.Units;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Builder.Units
{
    [TestFixture]
    public class InstantiatableUnitViewFactoryTest
    {
        private InstantiatableUnitViewFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _factory = new InstantiatableUnitViewFactory();
            _factory.UnitFactory = Substitute.For<IUnitFactory>();
        }

        [Test]
        public void BuildUnit()
        {
            var levelUnitConfig = new LevelUnitConfig
            {
                Unit = new UnitConfig
                {
                    Name = "Test",
                    Stats = new UnitStatsConfig
                    {
                        new StatConfig {StatType = StatType.Health, Value = 1},
                        new StatConfig {StatType = StatType.Power, Value = 2}
                    },
                    EquippedActionIds = new List<string> {"TestAction"}
                },
                StartPosition = new GridPosition(new GridCoords(1, 2), Facing.Left)
            };

            var unitViewConfig = new UnitViewConfig
            {
                PrefabPath = "TestPrefabPath"
            };

            var expectedUnit = new Unit();
            _factory.UnitFactory.CreateUnit(levelUnitConfig.Unit).Returns(expectedUnit);

            var buildableUnitView = _factory.Build(levelUnitConfig, unitViewConfig);

            Assert.AreEqual(expectedUnit, buildableUnitView.Unit);
            Assert.AreEqual("TestPrefabPath", buildableUnitView.PrefabPath);
            Assert.AreEqual(new GridPosition(new GridCoords(1, 2), Facing.Left), buildableUnitView.StartPosition);
        }
    }
}
