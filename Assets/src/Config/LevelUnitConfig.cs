﻿using System.Runtime.InteropServices;
using Common.DataContainers;
using Config.JsonConverters;
using Newtonsoft.Json;

namespace Config
{
    public class LevelUnitConfig
    {
        public UnitConfig Unit { get; set; }
        public string UnitViewId { get; set; }
        [JsonConverter(typeof(GridPositionConverter))]
        public GridPosition StartPosition { get; set; } 
    }
}