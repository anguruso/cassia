using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.CommandSignals
{
    public class TriggerApplyTransactionButtonSignal : Signal, ITriggerApplyTransactionButtonSignal
    {
    }

    public interface ITriggerApplyTransactionButtonSignal : ISignal
    {
    }
}