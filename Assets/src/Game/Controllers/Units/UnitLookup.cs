﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Occupants.Units;
using Game.Models.Stage;

namespace Game.Controllers.Units
{
    public class UnitLookup : IUnitLookup
    {
        [Inject]
        public IMap Map { get; set; }

        public IEnumerable<IUnit> GetUnits(IEnumerable<GridCoords> area)
        {
            var units = new List<IUnit>();

            foreach (var gridCoords in area)
            {
                var square = Map.GetSquare(gridCoords);
                if (square.Occupant is IUnit)
                {
                    units.Add((IUnit)square.Occupant);
                }
            }

            return units;
        }
    }
}
