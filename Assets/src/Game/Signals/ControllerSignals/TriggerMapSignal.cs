﻿using Common.DataContainers;
using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.ControllerSignals
{
    public class TriggerMapSignal : Signal<GridCoords>, ITriggerMapSignal
    {
    }

    public interface ITriggerMapSignal : ISignal<GridCoords>
    {
    }
}
