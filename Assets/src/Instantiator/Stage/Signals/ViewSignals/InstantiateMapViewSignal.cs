﻿using Common.Signals;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Instantiator.Stage.Signals.ViewSignals
{
	public class InstantiateMapViewSignal : Signal<Mesh>, IInstantiateMapViewSignal
	{
	}

	public interface IInstantiateMapViewSignal : ISignal<Mesh>
	{
	}
}
