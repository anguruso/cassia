﻿using Common.DataContainers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.Occupants.Units.UnitTurnTransactions
{
    public interface IUnitTurnTransaction
    {
        IUnit Actor { get; }
        GridCoords MovePosition { get; }
        IUnitTurnTransactionOutcome Outcome { get; }
        IUnitAction UnitAction { get; }
        bool IsReady { get; }
	    UnitTurnTransactionState CurrentState { get; }
	    GridCoords StartPosition { get; }
	    Facing Facing { get; }
	    void Clear();
	    void SelectMovePosition(GridCoords movePosition);
        void SelectTargetPosition(GridCoords targetPosition);
	    void SelectFacing(Facing facing);
        void SelectUnitAction(IUnitAction unitAction);
	    void SelectActor(IUnit unit, GridCoords startPosition);
    }
}