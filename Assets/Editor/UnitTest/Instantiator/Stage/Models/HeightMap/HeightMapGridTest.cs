﻿using System;
using Common.DataContainers;
using Instantiator.Stage.Models.HeightMap;
using NUnit.Framework;

namespace UnitTest.StageBuilder.Models.HeightMap
{
    public class HeightMapGridTest
    {
        [Test]
        public void AddSquare()
        {
	        var grid = HeightMapGrid.CreateWithDimensions(1, 1);
			var square = grid.AddSquareWithHeight(3, new GridCoords(0, 0));

	        Assert.AreEqual(3, square.Height);
        }

	    [Test]
	    public void GetSquare()
	    {
			var grid = HeightMapGrid.CreateWithDimensions(1, 1);
			grid.AddSquareWithHeight(3, new GridCoords(0, 0));
		    var square = grid.GetSquare(new GridCoords(0, 0));

			Assert.AreEqual(3, square.Height);
		}

        [Test]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetSquareOutOfRange()
        {
			var grid = HeightMapGrid.CreateWithDimensions(1, 1);
	        grid.GetSquare(new GridCoords(1, 1));
        }

        [Test]
        public void GetDimensions()
        {
	        var grid = HeightMapGrid.CreateWithDimensions(24, 41);
			
			Assert.AreEqual(24, grid.Width);
	        Assert.AreEqual(41, grid.Depth);
        }

	    [Test]
		[ExpectedException(typeof(ArgumentOutOfRangeException))]
		public void AddSquareOutOfRange()
	    {
		    var grid = HeightMapGrid.CreateWithDimensions(2, 2);
			grid.AddSquareWithHeight(2, new GridCoords(-1, 1));
	    }

	    [Test]
	    public void GetUninitializedSquare()
	    {
			var grid = HeightMapGrid.CreateWithDimensions(1, 1);
		    var square = grid.GetSquare(new GridCoords(0, 0));

		    Assert.AreEqual(0, square.Height);
	    }

	    [Test]
		[ExpectedException(typeof(InvalidOperationException), 
			ExpectedMessage = "Could not add a square at position (0, 0) because it already contains one.")]
	    public void AddSquareToOccupied()
	    {
		    var grid = HeightMapGrid.CreateWithDimensions(1, 1);
		    grid.AddSquareWithHeight(1, new GridCoords(0, 0));
		    grid.AddSquareWithHeight(4, new GridCoords(0, 0));
	    }
    }
}
