﻿using System;
using Config;
using Game.Models.Occupants;

namespace Game.Models.Stage
{
	public class Square : ISquare
	{
		private Square() { }

		public static Square CreateWithHeight(int height)
		{
			return new Square
			{
				Height = height,
				Occupant = EmptyOccupant.None
			};
		}

		public int Height { get; private set; }
		public bool IsOccupied { get { return Occupant != EmptyOccupant.None; } }
		

		public IOccupant Occupant { get; private set; }

		public void SetOccupant(IOccupant occupant)
		{
			if (Occupant == occupant) return;
			if (Occupant != EmptyOccupant.None)
			{
				var message = string.Format("Cannot set occupant on square occupied by {0}.", Occupant);
				throw new InvalidOperationException(message);
			}

			Occupant = occupant;
		}

		public void RemoveOccupant()
		{
			Occupant = EmptyOccupant.None;
		}

		public bool IsPathableBy(IOccupant occupant)
		{
			return Occupant.AllowsPassingBy(occupant);
		}
	}
}