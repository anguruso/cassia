﻿using System.Collections.Generic;

namespace Config
{
	public class LevelConfig
	{
	    public LevelConfig()
	    {
	        MapHeights = new int[0,0];
	    }

        public int[,] MapHeights { get; set; }
        public List<LevelUnitConfig> Units { get; set; }
	}
}
