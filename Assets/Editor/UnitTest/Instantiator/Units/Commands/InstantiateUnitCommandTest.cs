﻿using System.Collections.Generic;
using Common.DataContainers;
using Config;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using Instantiator.Stage.Signals.ViewSignals;
using Instantiator.Units;
using Instantiator.Units.Commands;
using NSubstitute;
using NUnit.Framework;
using UnityEditor;

namespace UnitTest.Builder.Units.Commands
{
	[TestFixture]
	public class InstantiateUnitCommandTest
	{
		private InstantiateUnitCommand _command;

		[SetUp]
		public void SetUp()
		{
		    _command = new InstantiateUnitCommand
		    {
		        Map = Substitute.For<IMap>(),
		        InstantiatableUnitViewFactory = Substitute.For<IInstantiatableUnitViewFactory>(),
		        InstantiateUnitViewSignal = Substitute.For<IInstantiateUnitViewSignal>(),
		    };
		}

		[Test]
		public void BuildUnits()
		{
		    var levelUnitConfig = new LevelUnitConfig
		    {
                StartPosition = new GridPosition(new GridCoords(1, 0), Facing.Back)
		    };
		    var unitViewConfig = new UnitViewConfig();

            var buildableUnitView = Substitute.For<IInstantiatableUnitView>();
			var builtUnit = Substitute.For<IUnit>();
            buildableUnitView.Unit.Returns(builtUnit);

			buildableUnitView.StartPosition.Returns(levelUnitConfig.StartPosition);
			_command.InstantiatableUnitViewFactory.Build(levelUnitConfig, unitViewConfig).Returns(buildableUnitView);

			_command.LevelUnitConfig = levelUnitConfig;
			_command.UnitViewConfig = unitViewConfig;

			_command.Execute();

			builtUnit.Received(1).Facing = Facing.Back;
			_command.Map.Received(1).SetOccupant(buildableUnitView.Unit, new GridCoords(1, 0));
			_command.InstantiateUnitViewSignal.Received().Dispatch(buildableUnitView, new GridCoords(1, 0));
		}
	}
}
