﻿using System.Collections.Generic;
using Config;

namespace Game.Models.Occupants.Units.UnitActions
{
    public interface IUnitActionEffectBuildersFactory
    {
        IEnumerable<IUnitActionEffectBuilder> GetEffectBuilders(string unitActionId, IEnumerable<UnitActionEffectConfig> effectConfigs);
    }
}