namespace Common.StateMachine.StateRuleBuilders
{
	public class TerminatingStateRuleBuilder<TKey>
	{
		private TerminatingStateRuleBuilder() { }

		public static TerminatingStateRuleBuilder<TKey> Create()
		{
			return new TerminatingStateRuleBuilder<TKey>();
		}
	}
}