﻿using System;
using System.Collections.Generic;
using Input;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;
using Vectrosity;

namespace Game.Views
{
	public class MapView : View, IClickable
	{
		public Signal<Vector3> ClickSignal = new Signal<Vector3>();

		public void OnClick(Click click)
		{
			ClickSignal.Dispatch(click.Position);
		}
	}
}
