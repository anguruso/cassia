﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Views.MonoBehaviours.UI
{
	public class OutcomeOverviewUI : MonoBehaviour
	{
		public Text Damage, Accuracy;
		public GameObject Background;

		public void Show(int damage, int accuracy)
		{
			Damage.gameObject.SetActive(true);
			Accuracy.gameObject.SetActive(true);
			Background.SetActive(true);

			Damage.text = string.Format("Damage: {0}", damage);
			Accuracy.text = string.Format("Accuracy: {0}%", accuracy);
		}

		public void Hide()
		{
			Damage.gameObject.SetActive(false);
			Accuracy.gameObject.SetActive(false);
			Background.SetActive(false);
		}
	}
}