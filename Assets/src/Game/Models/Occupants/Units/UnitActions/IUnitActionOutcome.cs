﻿using System.Collections.Generic;

namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IUnitActionOutcome
	{
		IUnit AffectedUnit { get; }
		IEnumerable<IUnitActionEffect> Effects { get; }
	}
}