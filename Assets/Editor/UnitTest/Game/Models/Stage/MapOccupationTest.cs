using System;
using Common.DataContainers;
using Config;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using NUnit.Framework;

namespace UnitTest.Game.Models.Stage
{
	[TestFixture]
	public class MapOccupationTest
	{
		private Map _map;

		[SetUp]
		public void SetUp()
		{
		    var heights = new[,]
		    {
		        {1, 1}
		    };

            _map = new Map();
			_map.LoadHeights(heights);
		}

		[Test]
		public void GetOccupantCoords()
		{
			var occupant = new Unit();
			_map.SetOccupant(occupant, new GridCoords(0, 1));

			var occupantCoords = _map.GetGridCoords(occupant);

			Assert.AreEqual(new GridCoords(0, 1), occupantCoords);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetGridCoordsForNull()
		{
			_map.GetGridCoords(null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException), 
			ExpectedMessage = "Cannot get GridCoords for an occupant that is not on the map.")]
		public void GetGridCoordsForOccupantNotOnMap()
		{
			var occupant = new Unit();
			_map.GetGridCoords(occupant);
		}

		[Test]
		public void HasOccupant()
		{
			var occupant = new Unit();
			Assert.IsFalse(_map.HasOccupant(occupant));

			_map.SetOccupant(occupant, new GridCoords(0, 0));
			Assert.IsTrue(_map.HasOccupant(occupant));
		}
	}
}