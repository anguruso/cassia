﻿using System.Collections.Generic;
using Common.DataContainers;

namespace Config
{
    public class UnitActionConfig
    {
        public UnitActionConfig()
        {
            TargetRange = new List<GridCoordsOffset>();
            AreaOfEffect = new List<GridCoordsOffset>();
            TargetEffects = new List<UnitActionEffectConfig>();
            ActorEffects = new List<UnitActionEffectConfig>();
        }
        
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<GridCoordsOffset> TargetRange { get; set; }
        public List<GridCoordsOffset> AreaOfEffect { get; set; }
        public List<UnitActionEffectConfig> TargetEffects { get; set; }
        public List<UnitActionEffectConfig> ActorEffects { get; set; }
    }
}
