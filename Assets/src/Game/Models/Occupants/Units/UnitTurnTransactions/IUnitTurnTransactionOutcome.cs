﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.Occupants.Units.UnitTurnTransactions
{
    public interface IUnitTurnTransactionOutcome
    {
        IEnumerable<GridCoords> AreaOfEffect { get; }
        IEnumerable<IUnitActionOutcome> UnitActionOutcomes { get; }
    }
}