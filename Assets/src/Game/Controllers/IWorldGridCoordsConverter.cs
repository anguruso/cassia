﻿using Common.DataContainers;
using Config;
using UnityEngine;

namespace Game.Controllers
{
	public interface IWorldGridCoordsConverter
	{
		Vector3 GetClosestWorldPositionCenter(Vector3 worldPosition);
		GridCoords GetGridCoords(Vector3 worldPosition);
		Vector3 GetWorldPosition(GridCoords gridCoords);
		void LoadConfigs(int[,] heights, MapInstantiatorConfig mapInstantiatorConfig);
		Quaternion GetRotation(Facing facing);
	}
}