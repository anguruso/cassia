﻿using Common.DataContainers;

namespace Instantiator.Stage.Models.HeightMap
{
    public class HeightMapTile
    {
        private HeightMapTile() { }

        public static HeightMapTile CreateWithLeftBackPoint(IntVector3 leftBackPoint)
        {
            return new HeightMapTile
            {
                LeftBack = leftBackPoint,
                RightBack = leftBackPoint + IntVector3.Right,
                LeftFront = leftBackPoint + IntVector3.Forward,
                RightFront = leftBackPoint + IntVector3.Right + IntVector3.Forward
            };
        }

        public IntVector3 LeftBack { get; private set; }
        public IntVector3 RightBack { get; private set; }
        public IntVector3 LeftFront { get; private set; }
        public IntVector3 RightFront { get; private set; }
    }
}