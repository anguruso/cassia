﻿using Common.DataContainers;
using Game.Models.Occupants;
using Game.Signals.CommandSignals;
using Game.Signals.ViewSignals;

namespace Game.Controllers.Units
{
	public interface IUnitTurnTransactionStateMachine
	{
		void Init(UnitTurnTransactionState initialState);
		UnitTurnTransactionState CurrentState { get; }
	}
}