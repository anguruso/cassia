using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;

namespace Instantiator.Stage.Models.MeshModel
{
	public class Chunk : IChunk
	{
		internal Chunk() { }

		internal ChunkFaceFactory FaceFactory;
		public IntVector3 Position;

		public ChunkFace Top { get; private set; }
		public ChunkFace Right { get; private set; }
		public ChunkFace Front { get; private set; }


		public IEnumerable<ChunkFace> GetFaces()
		{
			return new List<ChunkFace> { Top, Right, Front }.Where(x => x != null);
		}

		public void AddTopFaceWithTextureTileName(string textureTileName)
		{
			Top = FaceFactory.CreateTopFaceAtPosition(Position, textureTileName);
		}

		public void AddRightFaceWithTextureTileName(string tileTextureName)
		{
			Right = FaceFactory.CreateRightFaceAtPosition(Position, tileTextureName);
		}

		public void AddRightFaceWithTextureTileName()
		{
			throw new NotImplementedException("Chunk does not have a right face.");
		}

		public void AddBackFaceWithTextureTileName()
		{
			throw new NotImplementedException("Chunk does not have a back face.");
		}

		public void AddFrontFaceWithTextureTileName(string tileTextureName)
		{
			Front = FaceFactory.CreateFrontFaceAtPosition(Position, tileTextureName);
		}
	}
}