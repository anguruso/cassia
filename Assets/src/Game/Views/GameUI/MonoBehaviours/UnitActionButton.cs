﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Views.GameUI.MonoBehaviours
{
    [RequireComponent(typeof(Button))]
    public class UnitActionButton : MonoBehaviour
    {
        public Text ButtonText;

        private Button _button;
        private Action<int> _onClick;

        public int Index { get; set; }

        public string Text
        {
            get { return ButtonText.text; }
            set { ButtonText.text = value; }
        }

        public void Start()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnClick);
        }

        public void OnClick(Action<int> callback)
        {
            _onClick = callback;
        }

        private void OnClick()
        {
            _onClick = _onClick ?? (x => { });
            _onClick(Index);
        }
    }
}
