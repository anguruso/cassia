﻿using Config;
using Game.Models.Stage;
using Instantiator.Stage.Signals.ViewSignals;
using strange.extensions.command.impl;

namespace Instantiator.Units.Commands
{
	public class InstantiateUnitCommand : Command
	{
        [Inject]
        public LevelUnitConfig LevelUnitConfig { get; set; }

        [Inject]
        public UnitViewConfig UnitViewConfig { get; set; }

        [Inject]
		public IInstantiateUnitViewSignal InstantiateUnitViewSignal { get; set; }

		[Inject]
		public IMap Map { get; set; }

        [Inject]
	    public IInstantiatableUnitViewFactory InstantiatableUnitViewFactory { get; set; }

	    public override void Execute()
	    {
	        var buildableUnitView = InstantiatableUnitViewFactory.Build(LevelUnitConfig, UnitViewConfig);
		    buildableUnitView.Unit.Facing = LevelUnitConfig.StartPosition.Facing;

            Map.SetOccupant(buildableUnitView.Unit, LevelUnitConfig.StartPosition.GridCoords);
            InstantiateUnitViewSignal.Dispatch(buildableUnitView, LevelUnitConfig.StartPosition.GridCoords);
        }
	}
}
