﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Config;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.Pathing;
using Game.Signals.ControllerSignals;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils.Asserts;

namespace UnitTest.Game.Models.Occupants.Units
{
	[TestFixture]
	public class UnitTest
	{
		private Unit _unit;

		[SetUp]
		public void SetUp()
		{
            _unit = new Unit();
			_unit.Pathfinder = Substitute.For<IPathfinder>();
		    _unit.UnitActionLibrary = Substitute.For<IUnitActionLibrary>();
		    _unit.Stats = Substitute.For<IUnitStats>();
		}

		[Test]
		public void LoadConfig()
		{
			var config = new UnitConfig
			{
                Name = "Test",
                Stats = new UnitStatsConfig()
			};
			_unit.LoadConfig(config);

		    _unit.Stats.Received(1).LoadConfig(config.Stats);
		}

		[Test]
		public void IsSelectable()
		{
			Assert.IsTrue(_unit.IsSelectable);
		}

		[Test]
		public void GetPathTo()
		{
			var expectedPath = new List<GridCoords> {new GridCoords(1, 2), new GridCoords(3, 4)};
			_unit.Pathfinder = Substitute.For<IPathfinder>();
			_unit.Pathfinder.GetPathTo(_unit, new GridCoords(1, 2)).Returns(expectedPath);

			CollectionAssert.AreEqual(expectedPath, _unit.GetPathTo(new GridCoords(1, 2)));
		}

		[Test]
		public void GetMovementRange()
		{
			var expectedMovementRange = new List<GridCoords> {new GridCoords(4, 3), new GridCoords(2, 1)};
			_unit.Pathfinder = Substitute.For<IPathfinder>();
			_unit.Pathfinder.GetMovementRange(_unit).Returns(expectedMovementRange);

			CollectionAssert.AreEqual(expectedMovementRange, _unit.GetMovementRange());
		}

	    [Test]
	    public void GetUnitAction()
	    {
	        var expectedUnitAction0 = new UnitAction();
	        _unit.UnitActionLibrary.GetUnitAction("TestAction0").Returns(expectedUnitAction0);

	        var expectedUnitAction1 = new UnitAction();
	        _unit.UnitActionLibrary.GetUnitAction("TestAction1").Returns(expectedUnitAction1);

	        var unitConfig = new UnitConfig
	        {
	            EquippedActionIds = new List<string> {"TestAction0", "TestAction1"}
	        };
	        _unit.LoadConfig(unitConfig);

	        Assert.AreSame(expectedUnitAction0, _unit.GetUnitAction(0));
	        Assert.AreSame(expectedUnitAction1, _unit.GetUnitAction(1));
	    }

	    [Test]
        [ExpectedException(typeof(ArgumentException), ExpectedMessage = "No UnitAction with index 3 for Unit(Test).")]
	    public void GetInvalidUnitAction()
	    {
            var unitConfig = new UnitConfig
            {
                Name = "Test",
                EquippedActionIds = new List<string> { "TestAction0", "TestAction1" }
            };
            _unit.LoadConfig(unitConfig);
	        _unit.GetUnitAction(3);
	    }

	    [Test]
	    public void GetEquippedUnitActions()
	    {
            var expectedUnitAction0 = new UnitAction();
            _unit.UnitActionLibrary.GetUnitAction("TestAction0").Returns(expectedUnitAction0);

            var expectedUnitAction1 = new UnitAction();
            _unit.UnitActionLibrary.GetUnitAction("TestAction1").Returns(expectedUnitAction1);

            var unitConfig = new UnitConfig
            {
                EquippedActionIds = new List<string> { "TestAction0", "TestAction1" }
            };
            _unit.LoadConfig(unitConfig);

	        CollectionAssert.AreEqual(new List<UnitAction> {expectedUnitAction0, expectedUnitAction1}, _unit.EquippedUnitActions);
	    }

	    [Test]
	    public void ApplyUnitActionEffect()
	    {
		    var actorStats = Substitute.For<IUnitStats>();

	        var unitActionEffect = Substitute.For<IUnitActionEffect>();
	        unitActionEffect.Accuracy.Returns(1);
		    unitActionEffect.Delta.Returns(-2);
			unitActionEffect.StatType.Returns(StatType.Health);

	        _unit.ApplyUnitActionEffect(unitActionEffect, actorStats);

			_unit.Stats.Received(1).ApplyModifier(Arg.Is<IStatsModifier>(x => x.StatType == StatType.Health && x.Delta == -2));
	    }

	    [Test]
	    public void ApplyMissedUnitActionEffect()
	    {
            var unitActionEffect = Substitute.For<IUnitActionEffect>();
            unitActionEffect.Accuracy.Returns(0);
		    unitActionEffect.Delta.Returns(-2);
			unitActionEffect.StatType.Returns(StatType.Health);

            _unit.ApplyUnitActionEffect(unitActionEffect, Substitute.For<IUnitStats>());

	        _unit.Stats.DidNotReceive().ApplyModifier(Arg.Any<IStatsModifier>());
	    }

		[Test]
		public void SetFacing()
		{
			var unit = new Unit();
			unit.Facing = Facing.Back;
			Assert.AreEqual(Facing.Back, unit.Facing);
		}
	}
}
