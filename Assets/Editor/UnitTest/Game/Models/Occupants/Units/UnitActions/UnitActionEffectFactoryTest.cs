﻿using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Occupants.Units.UnitActions
{
	[TestFixture]
	public class UnitActionEffectFactoryTest
	{
		private UnitActionEffectFactory _factory;

		[SetUp]
		public void SetUp()
		{
			_factory = new UnitActionEffectFactory();
		}

		[Test]
		public void Create()
		{
			var actorStats = Substitute.For<IUnitStats>();
			var affectedUnitStats = Substitute.For<IUnitStats>();

			var builder = Substitute.For<IUnitActionEffectBuilder>();
			builder.UnitActionId.Returns("TestUnitAction");
			builder.Accuracy.Returns(0.75f);
			builder.AbnormalStatus.Returns(AbnormalStatusType.Weaken);
			builder.StatType.Returns(StatType.Jump);
			builder.Duration.Returns(4);
			builder.IsStackable.Returns(true);
			builder.IsRemoveable.Returns(true);
			builder.GetDelta(actorStats, affectedUnitStats).Returns(11);

			var unitActionEffect = _factory.CreateUnitActionEffect(builder, actorStats, affectedUnitStats);

			Assert.AreEqual("TestUnitAction", unitActionEffect.UnitActionId);
			Assert.AreEqual(0.75f, unitActionEffect.Accuracy);
			Assert.AreEqual(AbnormalStatusType.Weaken, unitActionEffect.AbnormalStatus);
			Assert.AreEqual(StatType.Jump, unitActionEffect.StatType);
			Assert.AreEqual(4, unitActionEffect.Duration);
			Assert.IsTrue(unitActionEffect.IsStackable);
			Assert.IsTrue(unitActionEffect.IsRemoveable);
			Assert.AreEqual(11, unitActionEffect.Delta);
		}
	}
}
