﻿using System;
using System.Collections.Generic;
using Config;
using UnityEngine;

namespace Game.Models.Occupants.Units.UnitActions
{
    public class UnitActionEffectBuildersFactory : IUnitActionEffectBuildersFactory
    {
        public IEnumerable<IUnitActionEffectBuilder> GetEffectBuilders(string unitActionId, IEnumerable<UnitActionEffectConfig> effectConfigs)
        {
            var unitActionEffects = new List<IUnitActionEffectBuilder>();

            foreach (var effectConfig in effectConfigs)
            {
                var unitActionEffect = UnitActionEffectBuilder.Create();

                unitActionEffect.UnitActionId = unitActionId;
                unitActionEffect.Accuracy = effectConfig.Accuracy;
                unitActionEffect.AbnormalStatus = effectConfig.AbnormalStatus;
                unitActionEffect.Duration = effectConfig.Duration;
                unitActionEffect.IsStackable = effectConfig.IsStackable;
                unitActionEffect.IsRemoveable = effectConfig.IsRemoveable;
                unitActionEffect.StatType = effectConfig.TargetStatType;
	            unitActionEffect.GetDelta = GetDeltaDelegate(effectConfig.ActorMultiplier, effectConfig.TargetMultiplier);
                unitActionEffects.Add(unitActionEffect);
            }

            return unitActionEffects;
        }

	    private UnitActionEffectBuilder.GetDeltaDelegate GetDeltaDelegate(StatsMultiplierConfig actorMultiplierConfig, StatsMultiplierConfig targetMultiplierConfig)
	    {
		    return (actorStats, targetStats) =>
		    {
			    var actorDelta = GetDelta(actorMultiplierConfig, actorStats);
			    var targetDelta = GetDelta(targetMultiplierConfig, targetStats);

				// the target cannot prevent more damage than is dealt
			    if (HasDifferentSigns(actorDelta, targetDelta))
			    {
				    if (Mathf.Abs(targetDelta) > Mathf.Abs(actorDelta)) return 0;
			    }

			    return (int) Math.Round(actorDelta + targetDelta);
		    };
	    }

	    private float GetDelta(StatsMultiplierConfig multiplierConfig, IUnitStats stats)
	    {
		    if (multiplierConfig == null) return 0;

		    var statType = multiplierConfig.StatType;
		    var multiplier = multiplierConfig.Value;
		    return stats.Get(statType)*multiplier;
	    }

	    private bool HasDifferentSigns(float first, float second)
	    {
		    return (first < 0 && second > 0) || (first > 0 && second < 0);
	    }
    }
}
