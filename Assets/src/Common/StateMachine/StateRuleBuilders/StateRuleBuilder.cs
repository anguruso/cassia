﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Signals;
using strange.extensions.signal.api;
using strange.extensions.signal.impl;

namespace Common.StateMachine.StateRuleBuilders
{
	public class StateRuleBuilder<TKey>
	{
        protected List<StateMachine<TKey>.BuildRuleDelegate> _buildActions;

        protected StateRuleBuilder() { }

        internal StateRule<TKey> Build()
        {
            StateRule<TKey> rule = StateRule<TKey>.Create();
            rule = _buildActions.Aggregate(rule, (current, buildAction) => buildAction(current));
            return rule;
        }

        public static StateRuleBuilder<TKey> Create()
		{
			return new StateRuleBuilder<TKey>
			{
				_buildActions = new List<StateMachine<TKey>.BuildRuleDelegate>()
			};
		}
		
		public PreTransitionStateRuleBuilder<TKey> On(IBaseSignal transitionSignal)
		{
			_buildActions.Add(rule =>
			{
				rule.Signal = transitionSignal;
				return rule;
			});
			return PreTransitionStateRuleBuilder<TKey>.Create(_buildActions);
		}

		public PreTransitionStateRuleBuilder<TKey, T0> On<T0>(ISignal<T0> transitionSignal)
		{
			_buildActions.Add(rule => StateRule<TKey, T0>.CreateWithSignal(rule, transitionSignal));

			_buildActions.Add(rule =>
			{
				rule.Signal = transitionSignal;
				return rule;
			});
			return PreTransitionStateRuleBuilder<TKey, T0>.Create(_buildActions);
		}

        public PreTransitionStateRuleBuilder<TKey, T0, T1> On<T0, T1>(ISignal<T0, T1> transitionSignal)
        {
            _buildActions.Add(rule => StateRule<TKey, T0, T1>.CreateWithSignal(rule, transitionSignal));

            _buildActions.Add(rule =>
            {
                rule.Signal = transitionSignal;
                return rule;
            });
            return PreTransitionStateRuleBuilder<TKey, T0, T1>.Create(_buildActions);
        }


        public StateRuleBuilder<TKey> OnEntry(Action action)
		{
			_buildActions.Add(rule =>
			{
				rule.EntryAction = action;
				return rule;
			});
			return this;
		}

		public StateRuleBuilder<TKey> OnExit(Action action)
		{
			_buildActions.Add(rule =>
			{
				rule.ExitAction = action;
				return rule;
			});
			return this;
		}
	}
}