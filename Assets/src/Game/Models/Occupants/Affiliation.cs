﻿namespace Game.Models.Occupants
{
	public class Affiliation
	{
		protected Affiliation() { }

		public static Affiliation CreateWithName(string name)
		{
			return new Affiliation
			{
				Name = name
			};
		}

		public string Name { get; private set; }

		// TODO: this needs to be tested or something sometime
		public virtual bool IsFriendlyWith(Affiliation other)
		{
			// TODO: just returns true if the affiliation is the same for now
			return this == other;
		}

		public static readonly NoneAffiliation None = new NoneAffiliation();
	}

	public class NoneAffiliation : Affiliation
	{
		public override bool IsFriendlyWith(Affiliation other)
		{
			return false;
		}
	}
}
