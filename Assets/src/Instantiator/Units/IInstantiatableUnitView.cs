﻿using Common.DataContainers;
using Game.Models.Occupants.Units;

namespace Instantiator.Units
{
    public interface IInstantiatableUnitView
    {
        string PrefabPath { get; }
        GridPosition StartPosition { get; set; }
        IUnit Unit { get; }
    }
}