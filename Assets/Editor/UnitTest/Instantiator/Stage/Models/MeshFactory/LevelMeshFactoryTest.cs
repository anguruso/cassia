﻿using System.Collections.Generic;
using Config;
using Instantiator.Stage.Models.MeshFactory;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.StageBuilder.Models.MeshFactory
{
	[TestFixture]
	public class LevelMeshFactoryTest
	{
		LevelMeshFactory _meshFactory;
		MapTextureConfig _mapTextureConfig;

		[SetUp]
		public void SetUp()
		{
			_mapTextureConfig = new MapTextureConfig();
			_mapTextureConfig.TextureTiles = new Dictionary<string, List<Vector2>>
			{
				{
					"dirt",
					new List<Vector2>
					{
						new Vector2(0, 0),
						new Vector2(0, 1),
						new Vector2(0, 2),
						new Vector2(0, 3)
					}
				},

				{
					"grass",
					new List<Vector2>
					{
						new Vector2(1, 0),
						new Vector2(1, 1),
						new Vector2(1, 2),
						new Vector2(1, 3)
					}
				},
			};

			_meshFactory = LevelMeshFactory.CreateWithDimensions(1, 0.2f);
		}

		[Test]
		public void CreateChunk()
		{
		    var heights = new[,]
		    {
		        {1}
		    };

            var mapViewConfig = new MapViewConfig
		    {
		        Squares = new[,]
		        {
		            {
		                new SquareViewConfig
		                {
		                    TopTextures = new string[] {"grass"},
		                    RightTextures = new string[] {"dirt"},
		                    FrontTextures = new string[] {"dirt"}
		                }
		            }
		        }
		    };

		    var mesh = _meshFactory.CreateMesh(heights, mapViewConfig, _mapTextureConfig);

			Assert.AreEqual(12, mesh.vertices.Length);
            CollectionAssert.AreEqual(new Vector3[]
            {
                 new Vector3(0.5f, 0.2f, -0.5f),
                 new Vector3(-0.5f, 0.2f, -0.5f),
                 new Vector3(-0.5f, 0.2f, 0.5f),
                 new Vector3(0.5f, 0.2f, 0.5f),

                 new Vector3(0.5f, 0.2f, -0.5f),
                 new Vector3(0.5f, 0.2f, 0.5f),
                 new Vector3(0.5f, 0f, 0.5f),
                 new Vector3(0.5f, 0f, -0.5f),

                 new Vector3(0.5f, 0.2f, 0.5f),
                 new Vector3(-0.5f, 0.2f, 0.5f),
                 new Vector3(-0.5f, 0f, 0.5f),
                 new Vector3(0.5f, 0f, 0.5f)
            }, mesh.vertices);

            CollectionAssert.AreEqual(new int[]
            {
                0, 1, 2, 2, 3, 0,
                4, 5, 6, 6, 7, 4,
                8, 9, 10, 10, 11, 8
            }, mesh.triangles);

            CollectionAssert.AreEqual(new Vector2[]
            {
                new Vector2(1, 0),
                new Vector2(1, 1),
                new Vector2(1, 2),
                new Vector2(1, 3),

                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(0, 2),
                new Vector2(0, 3),

                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(0, 2),
                new Vector2(0, 3)

            }, mesh.uv);
		}

	    [Test]
	    public void CreateTwoChunks()
	    {
	        var heights = new[,]
	        {
	            {2}
	        };

            var mapViewConfig = new MapViewConfig
	        {
	            Squares = new[,]
	            {
	                {
	                    new SquareViewConfig
	                    {
	                        TopTextures = new string[] {"grass"},
	                        RightTextures = new string[] {"dirt", "dirt"},
	                        FrontTextures = new string[] {"dirt", "dirt"}
	                    }
	                }
	            }
	        };
	        var mesh = _meshFactory.CreateMesh(heights, mapViewConfig, _mapTextureConfig);

            Assert.AreEqual(20, mesh.vertices.Length);
            CollectionAssert.AreEqual(new Vector3[]
            {
                // 0-right
                 new Vector3(0.5f, 0.2f, -0.5f),
                 new Vector3(0.5f, 0.2f, 0.5f),
                 new Vector3(0.5f, 0f, 0.5f),
                 new Vector3(0.5f, 0f, -0.5f),

                 // 0-front
                 new Vector3(0.5f, 0.2f, 0.5f),
                 new Vector3(-0.5f, 0.2f, 0.5f),
                 new Vector3(-0.5f, 0f, 0.5f),
                 new Vector3(0.5f, 0f, 0.5f),

                 // 1-top
                 new Vector3(0.5f, 0.4f, -0.5f),
                 new Vector3(-0.5f, 0.4f, -0.5f),
                 new Vector3(-0.5f, 0.4f, 0.5f),
                 new Vector3(0.5f, 0.4f, 0.5f),

                 // 1-right
                 new Vector3(0.5f, 0.4f, -0.5f),
                 new Vector3(0.5f, 0.4f, 0.5f),
                 new Vector3(0.5f, 0.2f, 0.5f),
                 new Vector3(0.5f, 0.2f, -0.5f), 

                 // 1-front
                 new Vector3(0.5f, 0.4f, 0.5f),
                 new Vector3(-0.5f, 0.4f, 0.5f),
                 new Vector3(-0.5f, 0.2f, 0.5f),
                 new Vector3(0.5f, 0.2f, 0.5f)
            }, mesh.vertices);
            CollectionAssert.AreEqual(new int[]
            {
                0, 1, 2, 2, 3, 0,
                4, 5, 6, 6, 7, 4,
                8, 9, 10, 10, 11, 8,
                12, 13, 14, 14, 15, 12,
                16, 17, 18, 18, 19, 16
            }, mesh.triangles);
	    }
	}
}
