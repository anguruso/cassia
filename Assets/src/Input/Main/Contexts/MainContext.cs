﻿using Input.Main.Controllers;
using Input.Main.Signals;
using UnityEngine;

namespace Input.Main.Contexts
{
	public class RootContext : SignalContext
	{

		public RootContext(MonoBehaviour view, bool autoStartup) : base(view, autoStartup)
		{

		}

		protected override void mapBindings()
		{
			commandBinder.Bind<MainStartupSignal>().To<MainStartupCommand>().Once();
		}

		public override void Launch()
		{
			var mainStartupSignal = injectionBinder.GetInstance<MainStartupSignal>();
			mainStartupSignal.Dispatch();
		}
	}
}
