using UnityEngine;

namespace Game.Views
{
	public class Click
	{
		private Click() { }

		public static Click CreateFromRaycastHit(RaycastHit hit)
		{
			return new Click
			{
				Position = hit.point
			};
		}

		public Vector3 Position { get; set; }
	}
}