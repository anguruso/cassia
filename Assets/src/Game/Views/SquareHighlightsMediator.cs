﻿using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Game.Controllers;
using Game.Signals.ViewSignals;
using strange.extensions.mediation.impl;

namespace Game.Views
{
	public class SquareHighlightsMediator : Mediator
	{
		[Inject]
		public SquareHighlightsView View { get; set; }

		[Inject]
		public IWorldGridCoordsConverter WorldGridCoordsConverter { get; set; }

		[Inject]
		public IShowMovementRangeViewSignal ShowMovementRangeViewSignal { get; set; }

		[Inject]
		public IHideMovementRangeViewSignal HideMovementRangeViewSignal { get; set; }

		public override void OnRegister()
		{
			ShowMovementRangeViewSignal.AddListener(ShowMovementRange);
			HideMovementRangeViewSignal.AddListener(HideMovementRange);
		}

		private void ShowMovementRange(IEnumerable<GridCoords> gridCoords)
		{
			var positions = gridCoords.Select(x => WorldGridCoordsConverter.GetWorldPosition(x));
			View.HighlightSquares(positions);
		}

		private void HideMovementRange()
		{
			View.UnhighlightAllSquares();
		}
	}
}
