﻿namespace Common.DataContainers
{
	public class GridPosition
	{
		public GridPosition(GridCoords gridCoords, Facing facing)
		{
			GridCoords = gridCoords;
			Facing = facing;
		}

		public GridCoords GridCoords { get; private set; }
		public Facing Facing { get; private set; }


		protected bool Equals(GridPosition other)
		{
			return GridCoords.Equals(other.GridCoords) && Facing.Equals(other.Facing);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((GridPosition) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (GridCoords.GetHashCode()*397) ^ Facing.GetHashCode();
			}
		}
	}
}