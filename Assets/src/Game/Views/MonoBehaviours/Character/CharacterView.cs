﻿using UnityEngine;

namespace Game.Views.MonoBehaviours.Character
{
    public abstract class CharacterView : MonoBehaviour
    {
        public abstract void Walk();
        public abstract void Stop();
    }
}
