using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.CommandSignals
{
	public class TriggerCancelTransactionButtonSignal : Signal, ITriggerCancelTransactionButtonSignal
	{
	}

	public interface ITriggerCancelTransactionButtonSignal : ISignal
	{
	}
}