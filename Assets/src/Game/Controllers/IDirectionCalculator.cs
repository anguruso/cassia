﻿using Common.DataContainers;

namespace Game.Controllers
{
    public interface IDirectionCalculator
    {
		Facing GetFacing(GridCoords from, GridCoords to);
    }
}