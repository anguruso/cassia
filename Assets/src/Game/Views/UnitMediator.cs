﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using Common.DataContainers;
using Game.Controllers;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Signals.ControllerSignals;
using Game.Signals.ViewSignals;
using strange.extensions.mediation.impl;

namespace Game.Views
{
	public class UnitMediator : Mediator
	{
		[Inject]
		public UnitView View { get; set; }

		[Inject]
		public IWorldGridCoordsConverter WorldGridCoordsConverter { get; set; }

        [Inject]
        public IMoveUnitViewCompleteSignal MoveUnitViewCompleteSignal { get; set; }

		[Inject]
		public ISetUnitPositionViewSignal SetUnitPositionViewSignal { get; set; }

		private IUnit Unit
		{
			get { return View.Unit; }
		}

		public override void OnRegister()
		{
			Unit.MoveUnitViewSignal.AddListener(MoveUnit); 
			Unit.SetUnitPositionViewSignal.AddListener(SetUnitPosition);

            View.MoveCompleteSignal.AddListener(OnMoveComplete);
		}

		private void MoveUnit(IEnumerable<GridCoords> path)
		{
			var worldPositionPath = path.Select(x => WorldGridCoordsConverter.GetWorldPosition(x));
			View.MoveTo(worldPositionPath);
		}

		private void SetUnitPosition(GridPosition position)
		{
			var worldPosition = WorldGridCoordsConverter.GetWorldPosition(position.GridCoords);
			var worldRotation = WorldGridCoordsConverter.GetRotation(position.Facing);
			View.SetTo(worldPosition, worldRotation);
		}

	    public void OnMoveComplete()
	    {
	        MoveUnitViewCompleteSignal.Dispatch(Unit);
	    }
	}
}