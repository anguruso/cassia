﻿namespace Game.Models.Occupants.Units
{
    public enum StatType
    {
        Health,
        Power,
        Defense,
        Move,
        Jump
    }
}
