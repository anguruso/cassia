﻿using System.Collections.Generic;
using Common.DataContainers;
using Config;
using Instantiator.Stage.Models.MeshModel;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.StageBuilder.Models.MeshModel
{
	[TestFixture]
	public class ChunkTest
	{
		private ChunkFactory _factory;

		[SetUp]
		public void SetUp()
		{
            MapTextureConfig mapTextureConfig = new MapTextureConfig();
		    mapTextureConfig.TextureTiles = new Dictionary<string, List<Vector2>>
		    {
		        {"test", new List<Vector2> {Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero}}
		    };
			_factory = ChunkFactory.Create(mapTextureConfig);
		}

		[Test]
		public void GetFaces()
		{
			var chunk = _factory.CreateChunkAt(new IntVector3(0, 0, 0)) as Chunk;
			chunk.AddTopFaceWithTextureTileName("test");
			chunk.AddRightFaceWithTextureTileName("test");
			chunk.AddFrontFaceWithTextureTileName("test");

			var expectedFaces = new List<ChunkFace>
			{
				chunk.Top,
				chunk.Right,
				chunk.Front
			};
			var actualFaces = chunk.GetFaces();
			
			CollectionAssert.AreEquivalent(expectedFaces, actualFaces);
		}

		[Test]
		public void GetCulledFaces()
		{
			var chunk = _factory.CreateChunkAt(new IntVector3(0, 0, 0)) as Chunk;
			chunk.AddTopFaceWithTextureTileName("test");
			chunk.AddFrontFaceWithTextureTileName("test");

			var expectedFaces = new List<ChunkFace>
			{
				chunk.Top,
				chunk.Front
			};
			var actualFaces = chunk.GetFaces();

			CollectionAssert.AreEquivalent(expectedFaces, actualFaces);
		}
	}
}
