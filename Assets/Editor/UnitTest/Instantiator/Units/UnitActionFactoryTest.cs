﻿using Config;
using Game.Models.Occupants.Units;
using Instantiator.Units;
using NSubstitute;
using NUnit.Framework;
using strange.extensions.injector.api;

namespace UnitTest.Builder.Units
{
    [TestFixture]
    public class UnitActionFactoryTest
    {
        private UnitActionFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _factory = new UnitActionFactory();
            _factory.InjectionBinder = Substitute.For<IInjectionBinder>();
        }

        [Test]
        public void LoadConfig()
        {
            var expectedUnitAction = Substitute.For<IInjectableUnitAction>();
            _factory.InjectionBinder.GetInstance<IInjectableUnitAction>().Returns(expectedUnitAction);

            var unitActionConfig = new UnitActionConfig();
            var actualUnitAction = _factory.CreateUnitAction(unitActionConfig);

            expectedUnitAction.Received(1).LoadConfig(unitActionConfig);
            Assert.AreEqual(expectedUnitAction, actualUnitAction);
        }
    }
}
