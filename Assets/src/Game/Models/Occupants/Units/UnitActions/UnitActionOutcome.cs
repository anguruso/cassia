﻿using System;
using System.Collections.Generic;

namespace Game.Models.Occupants.Units.UnitActions
{
    public class UnitActionOutcome : IUnitActionOutcome
	{
        public IUnit AffectedUnit { get; set; }
        public IEnumerable<IUnitActionEffect> Effects { get; set; }
    }
}