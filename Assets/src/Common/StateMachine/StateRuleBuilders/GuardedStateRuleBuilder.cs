using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.StateMachine.StateRuleBuilders
{
    public class GuardedStateRuleBuilder<TKey> : StateRuleBuilder<TKey>
    {
        private Func<bool> _predicate;

        public static GuardedStateRuleBuilder<TKey> CreateFromPredicate(List<StateMachine<TKey>.BuildRuleDelegate> buildActions, Func<bool> predicate)
        {
            return new GuardedStateRuleBuilder<TKey>
            {
                _buildActions = buildActions,
                _predicate = predicate
            };
        }

        public TransitionStateRuleBuilder<TKey> GoTo(TKey targetState)
        {
            _buildActions.Add(rule =>
            {
                ((StateRule<TKey>)rule).AddTransition(targetState, _predicate);
                return rule;
            });
            return TransitionStateRuleBuilder<TKey>.Create(_buildActions, _predicate);
        }
    }

	public class GuardedStateRuleBuilder<TKey, T0> : StateRuleBuilder<TKey>
	{
        private Func<T0, bool> _predicate;

		public static GuardedStateRuleBuilder<TKey, T0> CreateFromPredicate(List<StateMachine<TKey>.BuildRuleDelegate> buildActions,
			Func<T0, bool> predicate)
		{
			return new GuardedStateRuleBuilder<TKey, T0>
			{
				_buildActions = buildActions,
				_predicate = predicate
			};
		}

		public TransitionStateRuleBuilder<TKey, T0> GoTo(TKey targetState)
		{
			_buildActions.Add(rule =>
			{
				((StateRule<TKey, T0>)rule).AddTransition(targetState, _predicate);
				return rule;
			});
			return TransitionStateRuleBuilder<TKey, T0>.Create(_buildActions, _predicate);
		}
	}

    public class GuardedStateRuleBuilder<TKey, T0, T1> : StateRuleBuilder<TKey>
    {
        private Func<T0, T1, bool> _predicate;

        public static GuardedStateRuleBuilder<TKey, T0, T1> CreateFromPredicate(List<StateMachine<TKey>.BuildRuleDelegate> buildActions,
            Func<T0, T1, bool> predicate)
        {
            return new GuardedStateRuleBuilder<TKey, T0, T1>
            {
                _buildActions = buildActions,
                _predicate = predicate
            };
        }

        public TransitionStateRuleBuilder<TKey, T0, T1> GoTo(TKey targetState)
        {
            _buildActions.Add(rule =>
            {
                ((StateRule<TKey, T0, T1>)rule).AddTransition(targetState, _predicate);
                return rule;
            });
            return TransitionStateRuleBuilder<TKey, T0, T1>.Create(_buildActions, _predicate);
        }
    }
}