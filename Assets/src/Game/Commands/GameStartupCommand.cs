﻿using System.Collections.Generic;
using Common.DataContainers;
using Config;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Models.Stage;
using Game.Signals;
using Instantiator.Stage.Models.MeshFactory;
using Instantiator.Stage.Signals.CommandSignals;
using Instantiator.Stage.Signals.ViewSignals;
using Newtonsoft.Json;
using strange.extensions.command.impl;
using UnityEngine;

namespace Game.Commands
{
	public class GameStartupCommand : Command
	{
		[Inject]
		public IInstantiateMapViewSignal InstantiateMapViewSignal { get; set; }

		[Inject]
		public IInstantiateUnitSignal InstantiateUnitSignal { get; set; }

		[Inject]
		public IMap Map { get; set; }

		[Inject]
		public IWorldGridCoordsConverter WorldGridCoordsConverter { get; set; }

        [Inject]
        public IUnitActionLibrary UnitActionLibrary { get; set; }

		public override void Execute()
		{
            // TODP: i need to try loading these with an artificial delay to simulate loading from online
			var mapBuilderConfigLoader = ConfigLoader.CreateForPath("Configs/MapInstantiator/");
			var mapBuilderConfig = mapBuilderConfigLoader.LoadConfig<MapInstantiatorConfig>("map_instantiator_test");

		    var mapViewConfigLoader = ConfigLoader.CreateForPath("Configs/MapView/");
		    var mapViewConfig = mapViewConfigLoader.LoadConfig<MapViewConfig>("map_view_test");

			var textureConfigLoader = ConfigLoader.CreateForPath("Configs/MapTexture/");
			var textureConfig = textureConfigLoader.LoadConfig<MapTextureConfig>(mapViewConfig.TextureConfig);

		    var levelConfigLoader = ConfigLoader.CreateForPath("Configs/Level/");
		    var levelConfig = levelConfigLoader.LoadConfig<LevelConfig>("level_test");

		    var unitViewsConfigLoader = ConfigLoader.CreateForPath("Configs/UnitView/");
		    var unitViewsConfig = unitViewsConfigLoader.LoadConfig<UnitViewsConfig>("unit_views_test");

		    var unitActionsConfigLoader = ConfigLoader.CreateForPath("Configs/UnitAction/");
		    var unitActionsConfig = unitActionsConfigLoader.LoadConfig<UnitActionsConfig>("unit_actions_test");
            UnitActionLibrary.LoadConfigs(unitActionsConfig.UnitActions);

			var levelMeshFactory = LevelMeshFactory.CreateWithDimensions(mapBuilderConfig.ChunkSize, mapBuilderConfig.ChunkHeight);
			var mesh = levelMeshFactory.CreateMesh(levelConfig.MapHeights, mapViewConfig, textureConfig);

			InstantiateMapViewSignal.Dispatch(mesh);
			WorldGridCoordsConverter.LoadConfigs(levelConfig.MapHeights, mapBuilderConfig);

			//  TODO: fix this... these MUST be called in order. awful.
			Map.LoadHeights(levelConfig.MapHeights);
		    foreach (var levelUnitConfig in levelConfig.Units)
		    {
                // TODO: should probably verify this in some other command
		        var unitViewConfig = unitViewsConfig.UnitViews[levelUnitConfig.UnitViewId];
		        InstantiateUnitSignal.Dispatch(levelUnitConfig, unitViewConfig);
		    }
		}
	}
}
