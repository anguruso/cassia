﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.Occupants.Units.UnitTurnTransactions
{
    public class UnitTurnTransactionOutcome : IUnitTurnTransactionOutcome
    {
        private UnitTurnTransactionOutcome() { }

        public static UnitTurnTransactionOutcome Create(IEnumerable<GridCoords> areaOfEffect, IEnumerable<IUnitActionOutcome> unitActionOutcomes)
        {
            return new UnitTurnTransactionOutcome
            {
                AreaOfEffect = areaOfEffect,
                UnitActionOutcomes = unitActionOutcomes
            };
        }

        public IEnumerable<GridCoords> AreaOfEffect { get; private set; }
        public IEnumerable<IUnitActionOutcome>  UnitActionOutcomes { get; private set; } 
    }
}