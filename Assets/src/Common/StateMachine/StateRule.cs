﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Signals;
using strange.extensions.signal.api;
using strange.extensions.signal.impl;
using UnityEngineInternal;

namespace Common.StateMachine
{
	public class StateRule<TKey>
	{
		private IBaseSignal _signal;
	    private Action<IBaseSignal, object[]> _listener;
		public List<ConditionalTransition<TKey>> ConditionalTransitions;

		protected StateRule() { }

		public static StateRule<TKey> Create()
		{
			return new StateRule<TKey>
			{
				EntryAction = () => { },
				ExitAction = () => { },
				ConditionalTransitions = new List<ConditionalTransition<TKey>>(),
			};
		}

        public Action EntryAction { get; set; }
        public Action ExitAction { get; set; }

        public void AddTransition(TKey state, Func<bool> predicate)
		{
			var transition = ConditionalTransition<TKey>.CreateForTargetStateAndPredicate(state, predicate);
			ConditionalTransitions.Add(transition);
		}

		public virtual bool IsTransition
		{
			get { return ConditionalTransitions.Any(); }
		}

		public virtual IBaseSignal Signal
		{
			get { return _signal; }
			set { _signal = (BaseSignal) value; }	
		}

		public virtual void SetTransitionCallback(Action<TKey> onTransition)
		{
            _listener = (signal, args) =>
            {
                foreach (var conditionalTransition in ConditionalTransitions)
                {
                    if (conditionalTransition.ShouldTransition())
                    {
                        onTransition(conditionalTransition.TargetState);
                        conditionalTransition.TransitionAction();
                        return;
                    }
                }
            };
        }

		public virtual void StartListening()
		{
			if (IsTransition) _signal.AddListener(_listener);
		}

		public virtual void StopListening()
		{
			if (IsTransition) _signal.RemoveListener(_listener);
		}
	}

	internal class StateRule<TKey, T0> : StateRule<TKey>
	{
		private ISignal<T0> _signal;
		private Action<T0> _listener;
		public new List<ConditionalTransition<TKey, T0>> ConditionalTransitions; 

		private StateRule() { }

		public static StateRule<TKey, T0> CreateWithSignal(StateRule<TKey> baserule, ISignal<T0> signal)
		{
			return new StateRule<TKey, T0>
			{
				EntryAction = baserule.EntryAction,
				ExitAction = baserule.ExitAction,
				_signal = signal,
				ConditionalTransitions = new List<ConditionalTransition<TKey, T0>>()
			};
		}

		public void AddTransition(TKey state, Func<T0, bool> predicate)
		{
			var conditionalTransition = ConditionalTransition<TKey, T0>.CreateForTargetStateAndPredicate(state, predicate);
			ConditionalTransitions.Add(conditionalTransition);
		}

		public override bool IsTransition
		{
			get { return ConditionalTransitions.Any(); }
		}

		public override IBaseSignal Signal
		{
			get { return _signal; }
			set { _signal = (ISignal<T0>)value; }
		}

		public override void SetTransitionCallback(Action<TKey> onTransition)
		{
			_listener = x =>
			{
				foreach (var conditionalTransition in ConditionalTransitions)
				{
					if (conditionalTransition.ShouldTransition(x))
					{
						onTransition(conditionalTransition.TargetState);
						conditionalTransition.TransitionAction(x);
						return;
					}
				}
			};
		}

		public override void StartListening()
		{
			if (IsTransition) _signal.AddListener(_listener);
		}

		public override void StopListening()
		{
			if (IsTransition) _signal.RemoveListener(_listener);
		}
	}

    internal class StateRule<TKey, T0, T1> : StateRule<TKey>
    {
        private ISignal<T0, T1> _signal;
        private Action<T0, T1> _listener;
        public new List<ConditionalTransition<TKey, T0, T1>> ConditionalTransitions;

        private StateRule() { }

        public static StateRule<TKey, T0, T1> CreateWithSignal(StateRule<TKey> baserule, ISignal<T0, T1> signal)
        {
            return new StateRule<TKey, T0, T1>
            {
                EntryAction = baserule.EntryAction,
                ExitAction = baserule.ExitAction,
                _signal = signal,
                ConditionalTransitions = new List<ConditionalTransition<TKey, T0, T1>>()
            };
        }

        public void AddTransition(TKey state, Func<T0, T1, bool> predicate)
        {
            var conditionalTransition = ConditionalTransition<TKey, T0, T1>.CreateForTargetStateAndPredicate(state, predicate);
            ConditionalTransitions.Add(conditionalTransition);
        }

        public override bool IsTransition
        {
            get { return ConditionalTransitions.Any(); }
        }

        public override IBaseSignal Signal
        {
            get { return _signal; }
            set { _signal = (ISignal<T0, T1>)value; }
        }

        public override void SetTransitionCallback(Action<TKey> onTransition)
        {
            _listener = (x, y) =>
            {
                foreach (var conditionalTransition in ConditionalTransitions)
                {
                    if (conditionalTransition.ShouldTransition(x, y))
                    {
                        onTransition(conditionalTransition.TargetState);
                        conditionalTransition.TransitionAction(x, y);
                        return;
                    }
                }
            };
        }

        public override void StartListening()
        {
            if (IsTransition) _signal.AddListener(_listener);
        }

        public override void StopListening()
        {
            if (IsTransition) _signal.RemoveListener(_listener);
        }
    }
}