﻿using System.Collections.Generic;
using Config;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Controllers
{
    public interface IUnitActionLibrary
    {
        IUnitAction GetUnitAction(string unitActionId);
        void LoadConfigs(IEnumerable<UnitActionConfig> configs);
    }
}