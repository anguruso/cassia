﻿using System;
using Game.Controllers.Units;
using Game.Models.Occupants.Units.UnitActions;
using UnityEngine;

namespace Game.Models.Occupants.Units
{
    public class StatsModifier : IStatsModifier
    {
        private StatsModifier() { }

        public static StatsModifier Create(IUnitActionEffect unitActionEffect, IUnitStats actorStats, IUnitStats targetStats)
        {
            return new StatsModifier
            {
                UnitActionId = unitActionEffect.UnitActionId,
                StatType = unitActionEffect.StatType,
				Delta = unitActionEffect.Delta,
                Duration = unitActionEffect.Duration,
                AbnormalStatus = unitActionEffect.AbnormalStatus,
                IsStackable = unitActionEffect.IsStackable,
                IsRemoveable = unitActionEffect.IsRemoveable
            };
        }

        public string UnitActionId { get; private set; }
        public StatType StatType { get; private set; }
        public int Delta { get; private set; }
        public int? Duration { get; private set; }
        public AbnormalStatusType? AbnormalStatus { get; private set; }
        public bool IsStackable { get; private set; }
        public bool IsRemoveable { get; set; }

        public void Combine(IStatsModifier modifier)
        {
            if (StatType != modifier.StatType)
            {
                var message = string.Format("Cannot combine StatsModifier with type '{0}' with one of type '{1}'.", StatType, modifier.StatType);
                throw new ArgumentException(message);
            }

            Delta = IsStackable ? modifier.Delta + Delta : modifier.Delta;
            Duration = modifier.Duration;
        }

        public void ReduceDuration()
        {
            Duration--;
        }
    }
}
