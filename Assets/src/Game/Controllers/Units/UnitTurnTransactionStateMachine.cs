﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Common.DataContainers;
using Common.StateMachine;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitTurnTransactions;
using Game.Models.Stage;
using Game.Signals;
using Game.Signals.CommandSignals;
using Game.Signals.ControllerSignals;
using Game.Signals.ViewSignals;
using strange.extensions.signal.impl;

namespace Game.Controllers.Units
{
	public class UnitTurnTransactionStateMachine : IUnitTurnTransactionStateMachine
	{
		[Inject]
		public ITriggerUnitSignal TriggerUnitSignal { get; set; }

		[Inject]
		public ITriggerMapSignal TriggerMapSignal { get; set; }

		[Inject]
		public IMoveUnitViewCompleteSignal MoveUnitViewCompleteSignal { get; set; }

		[Inject]
		public ITriggerUnitActionButtonSignal TriggerUnitActionButtonSignal { get; set; }

		[Inject]
		public ITriggerApplyTransactionButtonSignal TriggerApplyTransactionButtonSignal { get; set; }

		[Inject]
		public ITriggerCancelTransactionButtonSignal TriggerCancelTransactionButtonSignal { get; set; }

		[Inject]
		public IApplyUnitTurnTransactionSignal ApplyUnitTurnTransactionSignal { get; set; }

		[Inject]
		public IUnitTurnTransaction UnitTurnTransaction { get; set; }

		[Inject]
		public ISelectableSquaresController SelectableSquaresController { get; set; }

		[Inject]
		public IUpdateGameUIViewSignal UpdateGameUIViewSignal { get; set; }

		[Inject]
		public IDirectionCalculator DirectionCalculator { get; set; }
		
		private StateMachine<UnitTurnTransactionState> _stateMachine;

		public UnitTurnTransactionState CurrentState
		{
			get { return _stateMachine.CurrentState; }
		}

		public void Init(UnitTurnTransactionState initialState)
		{
			_stateMachine = new StateMachine<UnitTurnTransactionState>();
			_stateMachine.Init(initialState);

			InitializeNothingState();
			InitializeActorSelectedState();
			InitializeActorMovingState();
			InitializePreSelectUnitActionState();
			InitializePreSelectTargetState();
			InitializePreApplyTransactionState();

			_stateMachine.Start();
		}

		private void InitializeNothingState()
		{
			_stateMachine.In(UnitTurnTransactionState.Nothing).On(TriggerUnitSignal).GoTo(UnitTurnTransactionState.ActorSelected).Execute((actor, actorPosition) =>
			{
				UnitTurnTransaction.SelectActor(actor, actorPosition);
				SelectableSquaresController.Set(actor.GetMovementRange());

				UpdateGameUIViewSignal.Dispatch();
			});
		}

		private void InitializeActorSelectedState()
		{
			_stateMachine.In(UnitTurnTransactionState.ActorSelected).On(TriggerMapSignal)
			.If(movePosition => SelectableSquaresController.Get().Contains(movePosition)).GoTo(UnitTurnTransactionState.Moving).Execute(
				movePosition =>
				{
					UnitTurnTransaction.SelectMovePosition(movePosition);
					var path = UnitTurnTransaction.Actor.GetPathTo(movePosition);
					UnitTurnTransaction.Actor.MoveUnitViewSignal.Dispatch(path);
					SelectableSquaresController.Clear();

					UpdateGameUIViewSignal.Dispatch();
				})
			.GoTo(UnitTurnTransactionState.Nothing).Execute(
				x =>
				{
					SelectableSquaresController.Clear();

					UpdateGameUIViewSignal.Dispatch();
				});

			_stateMachine.In(UnitTurnTransactionState.ActorSelected).On(TriggerUnitSignal)
				.If((actor, actorPosition) => UnitTurnTransaction.Actor == actor)
				.GoTo(UnitTurnTransactionState.PreSelectAction).Execute((actor, actorPosition) =>
				{
					UnitTurnTransaction.SelectMovePosition(actorPosition);
					SelectableSquaresController.Clear();

					UpdateGameUIViewSignal.Dispatch();
				})
				.GoTo(UnitTurnTransactionState.ActorSelected).Execute((actor, actorPosition) =>
				{
					UnitTurnTransaction.SelectActor(actor, actorPosition);
					SelectableSquaresController.Set(actor.GetMovementRange());

					UpdateGameUIViewSignal.Dispatch();
				});
		}

		private void InitializeActorMovingState()
		{
			_stateMachine.In(UnitTurnTransactionState.Moving).On(MoveUnitViewCompleteSignal).If(actor => UnitTurnTransaction.Actor == actor).GoTo(UnitTurnTransactionState.PreSelectAction).Execute(
			actor =>
			{
				UpdateGameUIViewSignal.Dispatch();
			});
		}

		private void InitializePreSelectUnitActionState()
		{
			_stateMachine.In(UnitTurnTransactionState.PreSelectAction).On(TriggerUnitActionButtonSignal).GoTo(UnitTurnTransactionState.PreSelectTarget).Execute(
			index =>
			{
				var unitAction = UnitTurnTransaction.Actor.GetUnitAction(index);
				UnitTurnTransaction.SelectUnitAction(unitAction);
				SelectableSquaresController.Set(unitAction.GetTargetRange(UnitTurnTransaction.MovePosition));
				UpdateGameUIViewSignal.Dispatch();
			});

			_stateMachine.In(UnitTurnTransactionState.PreSelectAction).On(TriggerMapSignal).GoTo(UnitTurnTransactionState.ActorSelected).Execute(
			position =>
			{
				var actor = UnitTurnTransaction.Actor;
				var startPosition = new GridPosition(UnitTurnTransaction.StartPosition, actor.Facing);

				actor.SetUnitPositionViewSignal.Dispatch(startPosition);
				SelectableSquaresController.Set(actor.GetMovementRange());
				UpdateGameUIViewSignal.Dispatch();
			});

			// TODO if you click another unit, its also cancel?
		}

		private void InitializePreSelectTargetState()
		{
			_stateMachine.In(UnitTurnTransactionState.PreSelectTarget).On(TriggerUnitSignal) // TODO: maybe let them trigger MAP for certain unit actions
			.If((targetUnit, targetPosition) => SelectableSquaresController.Get().Contains(targetPosition)).GoTo(UnitTurnTransactionState.PreApplyTransaction).Execute(
			(targetUnit, targetPosition) =>
			{
				UnitTurnTransaction.SelectTargetPosition(targetPosition);
				var movePosition = UnitTurnTransaction.MovePosition;
				var facing = DirectionCalculator.GetFacing(movePosition, targetPosition);
				UnitTurnTransaction.SelectFacing(facing);

				UnitTurnTransaction.Actor.SetUnitPositionViewSignal.Dispatch(new GridPosition(movePosition, facing));

				SelectableSquaresController.Set(UnitTurnTransaction.Outcome.AreaOfEffect); // NOT REALLY, ITS NOT ACTUALLY SELECTABLE
				UpdateGameUIViewSignal.Dispatch();
			}).GoTo(UnitTurnTransactionState.PreSelectAction).Execute(
			(actor, actorPosition) =>
			{
				SelectableSquaresController.Clear();
				UpdateGameUIViewSignal.Dispatch();
			});

			_stateMachine.In(UnitTurnTransactionState.PreSelectTarget).On(TriggerMapSignal).GoTo(UnitTurnTransactionState.PreSelectAction).Execute(
			position =>
			{
				SelectableSquaresController.Clear();
				UpdateGameUIViewSignal.Dispatch();
			});
		}

		private void InitializePreApplyTransactionState()
		{
			_stateMachine.In(UnitTurnTransactionState.PreApplyTransaction).On(TriggerApplyTransactionButtonSignal).GoTo(UnitTurnTransactionState.Nothing).Execute(
			() =>
			{
				ApplyUnitTurnTransactionSignal.Dispatch();
				UnitTurnTransaction.Clear();
				SelectableSquaresController.Clear();
				UpdateGameUIViewSignal.Dispatch();
			});

			_stateMachine.In(UnitTurnTransactionState.PreApplyTransaction).On(TriggerCancelTransactionButtonSignal).GoTo(UnitTurnTransactionState.PreSelectTarget).Execute(
			() =>
			{
				var unitAction = UnitTurnTransaction.UnitAction;
				var actorPosition = UnitTurnTransaction.MovePosition;

				SelectableSquaresController.Set(unitAction.GetTargetRange(actorPosition));
				UpdateGameUIViewSignal.Dispatch();
			});
		}
	}
}
