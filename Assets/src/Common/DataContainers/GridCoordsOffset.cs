﻿namespace Common.DataContainers
{
    public struct GridCoordsOffset
    {
        public GridCoordsOffset(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public int Row { get; set; }
        public int Column { get; set; }
    }
}
