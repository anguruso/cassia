﻿using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Config;
using Instantiator.Stage.Models.MeshModel;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.StageBuilder.Models.MeshModel
{
	// delete this
	[TestFixture]
	public class ChunkFactoryTest
	{
		private ChunkFactory _factory;

		[SetUp]
		public void SetUp()
		{
            MapTextureConfig mapTextureConfig = new MapTextureConfig();
            mapTextureConfig.TextureTiles = new Dictionary<string, List<Vector2>>
            {
                {"test", new List<Vector2> {Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero}}
            };
            _factory = ChunkFactory.Create(mapTextureConfig);
		}

		[Test]
		public void CreateChunk()
		{
			var chunk = _factory.CreateChunkAt(new IntVector3(0, 0, 0)) as Chunk;

			chunk.AddTopFaceWithTextureTileName("test");
			chunk.AddRightFaceWithTextureTileName("test");
			chunk.AddFrontFaceWithTextureTileName("test");

		    var topCorners = chunk.Top.GetCornerPositions().ToList();
			Assert.AreEqual(new IntVector3(1, 1, 0), topCorners[0]);
			Assert.AreEqual(new IntVector3(0, 1, 1), topCorners[2]);

		    var rightCorners = chunk.Right.GetCornerPositions().ToList();
			Assert.AreEqual(new IntVector3(1, 1, 0), rightCorners[0]);
			Assert.AreEqual(new IntVector3(1, 0, 1), rightCorners[2]);

		    var frontCorners = chunk.Front.GetCornerPositions().ToList();
			Assert.AreEqual(new IntVector3(1, 1, 1), frontCorners[0]);
			Assert.AreEqual(new IntVector3(0, 0, 1), frontCorners[2]);
		}
	}
}
