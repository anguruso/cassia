﻿namespace Config
{
	public class MapInstantiatorConfig
	{
		public float ChunkSize { get; set; }
		public float ChunkHeight { get; set; }
	}
}
