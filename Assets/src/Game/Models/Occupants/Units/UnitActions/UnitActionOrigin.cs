﻿using System;
using Common.DataContainers;

namespace Game.Models.Occupants.Units.UnitActions
{
    public class UnitActionOrigin : IUnitActionOrigin
    {
        private UnitActionOrigin() { }

        public static UnitActionOrigin CreateFromActorAndPosition(IUnit actor, GridCoords actorPosition)
        {
            if (actor == null) throw new ArgumentNullException("Actor");

            return new UnitActionOrigin
            {
                Actor = actor,
                ActorPosition = actorPosition
            };
        }
        
        public IUnit Actor { get; private set; }
        public GridCoords ActorPosition { get; private set; }
    }
}