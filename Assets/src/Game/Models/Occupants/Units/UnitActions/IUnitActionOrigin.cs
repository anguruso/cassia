﻿using Common.DataContainers;

namespace Game.Models.Occupants.Units.UnitActions
{
    public interface IUnitActionOrigin
    {
        IUnit Actor { get; }
        GridCoords ActorPosition { get; }
    }
}