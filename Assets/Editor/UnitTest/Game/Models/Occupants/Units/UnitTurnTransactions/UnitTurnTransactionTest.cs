﻿using Common.DataContainers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.Occupants.Units.UnitTurnTransactions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Occupants.Units.UnitTurnTransactions
{
    [TestFixture]
    public class UnitTurnTransactionTest
    {
        private UnitTurnTransaction _transaction;

        [SetUp]
        public void SetUp()
        {
            _transaction = new UnitTurnTransaction();
	        _transaction.StateMachine = Substitute.For<IUnitTurnTransactionStateMachine>();
            _transaction.UnitTurnTransactionOutcomeFactory = Substitute.For<IUnitTurnTransactionOutcomeFactory>();
        }

        [Test]
        public void Construct()
        {
            Assert.IsNull(_transaction.Actor);
            Assert.IsNull(_transaction.UnitAction);
            Assert.IsNull(_transaction.Outcome);

			_transaction.PostConstruct();
	        _transaction.StateMachine.Received(1).Init(UnitTurnTransactionState.Nothing);
        }

        [Test]
        public void NormalFlow()
        {
	        var actor = Substitute.For<IUnit>();

			var startPosition = new GridCoords(0, 1);
            _transaction.SelectActor(actor, startPosition);

            var movePosition = new GridCoords(1, 2);
            _transaction.SelectMovePosition(movePosition);

            var unitAction = Substitute.For<IUnitAction>();
            _transaction.SelectUnitAction(unitAction);

	        var facing = Facing.Right;
	        _transaction.SelectFacing(facing);

			var targetPosition = new GridCoords(3, 4);
            var outcome = Substitute.For<IUnitTurnTransactionOutcome>();
            _transaction.UnitTurnTransactionOutcomeFactory.GetOutcome(Arg.Is<UnitActionOrigin>(x => x.Actor == actor && x.ActorPosition == targetPosition), 
                unitAction, targetPosition).Returns(outcome);
            _transaction.SelectTargetPosition(targetPosition);

            Assert.AreEqual(actor, _transaction.Actor);
	        Assert.AreEqual(startPosition, _transaction.StartPosition);
            Assert.AreEqual(movePosition, _transaction.MovePosition);
            Assert.AreEqual(unitAction, _transaction.UnitAction);
	        Assert.AreEqual(facing, _transaction.Facing);
            Assert.AreEqual(outcome, _transaction.Outcome);
        }

        [Test]
        public void IsReady()
        {
            Assert.IsFalse(_transaction.IsReady);

            _transaction.SelectActor(Substitute.For<IUnit>(), new GridCoords(0, 1));
            Assert.IsFalse(_transaction.IsReady);

            _transaction.SelectMovePosition(new GridCoords(1, 2));
            Assert.IsFalse(_transaction.IsReady);

            _transaction.SelectUnitAction(Substitute.For<IUnitAction>());
            Assert.IsFalse(_transaction.IsReady);

            var unitActionOrigin = UnitActionOrigin.CreateFromActorAndPosition(_transaction.Actor, new GridCoords(1, 2));
            _transaction.UnitTurnTransactionOutcomeFactory.GetOutcome(unitActionOrigin, _transaction.UnitAction, new GridCoords(3, 4)).Returns(Substitute.For<IUnitTurnTransactionOutcome>());
            _transaction.SelectTargetPosition(new GridCoords(3, 4));

            _transaction.SelectTargetPosition(new GridCoords(3, 4));
            Assert.IsTrue(_transaction.IsReady);
        }

        [Test]
        public void Clear()
        {
            _transaction.SelectActor(Substitute.For<IUnit>(), new GridCoords(0, 1));
            _transaction.SelectMovePosition(new GridCoords(1, 2));
            _transaction.SelectUnitAction(Substitute.For<IUnitAction>());
            _transaction.SelectTargetPosition(new GridCoords(3, 4));

            _transaction.Clear();

            Assert.IsNull(_transaction.Actor);
            Assert.IsNull(_transaction.UnitAction);
            Assert.IsNull(_transaction.Outcome);
        }

	    [Test]
	    public void CurrentState()
	    {
		    _transaction.StateMachine.CurrentState.Returns(UnitTurnTransactionState.PreApplyTransaction);
		    var actualState = _transaction.CurrentState;

		    Assert.AreEqual(UnitTurnTransactionState.PreApplyTransaction, actualState);
	    }
	}
}
