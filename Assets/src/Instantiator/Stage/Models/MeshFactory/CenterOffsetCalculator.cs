﻿using System;
using UnityEngine;

namespace Instantiator.Stage.Models.MeshFactory
{
    public class CenterOffsetCalculator
    {
        private float _chunkSize;

        private CenterOffsetCalculator() { }

        public static CenterOffsetCalculator CreateWithDimensions(float chunkSize)
        {
            return new CenterOffsetCalculator
            {
                _chunkSize = chunkSize
            };
        }

        public Vector3 GetCenterOffset(int[,] heights)
        {
            if (heights == null)
                throw new ArgumentNullException("heights");

            var mapDepth = heights.GetLength(0);
            var mapWidth = mapDepth > 0 ? heights.GetLength(1) : 0;
            var xOffset = -((mapWidth) * _chunkSize) / 2f;
	        var yOffset = 0f;
            var zOffset = -((mapDepth) * _chunkSize) / 2f;
            return new Vector3(xOffset, yOffset, zOffset);
        }
    }
}
