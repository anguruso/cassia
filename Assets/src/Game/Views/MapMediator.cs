﻿using System.Collections.Generic;
using Game.Controllers;
using Game.Signals.CommandSignals;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views
{
	public class MapMediator : Mediator
	{
		[Inject]
		public MapView View { get; set; }

		[Inject]
		public IClickMapSignal ClickMapSignal { get; set; }

		[Inject]
		public IWorldGridCoordsConverter WorldGridCoordsConverter { get; set; }

		public override void OnRegister()
		{
			View.ClickSignal.AddListener(OnClick);
		}

		public void OnClick(Vector3 position)
		{
			var gridCoords = WorldGridCoordsConverter.GetGridCoords(position);
			ClickMapSignal.Dispatch(gridCoords);
		}
	}
}
