﻿using Config;
using Game.Models.Stage;

namespace UnitTest.Utils
{
	public class MapFactory
	{
		private MapFactory() { }

		public static MapFactory Create()
		{
			return new MapFactory();
		}

		public Map CreateMapWithHeights(int[,] heightMap)
		{
			var map = new Map();
			map.LoadHeights(heightMap);
			return map;
		}
	}
}
