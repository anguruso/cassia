﻿using Game.Models.Occupants;

namespace Game.Models.Stage
{
	public interface ISquare
	{
		int Height { get; }
		bool IsOccupied { get; }
		IOccupant Occupant { get; }

		bool IsPathableBy(IOccupant occupant);
		void RemoveOccupant();
		void SetOccupant(IOccupant occupant);
	}
}