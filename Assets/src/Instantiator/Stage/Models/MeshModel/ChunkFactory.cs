﻿using Common.DataContainers;
using Config;

namespace Instantiator.Stage.Models.MeshModel
{
	public class ChunkFactory : IChunkFactory
	{
		private ChunkFaceFactory _factory;

		private ChunkFactory() { }

		public static ChunkFactory Create(MapTextureConfig mapTextureConfig)
		{
			return new ChunkFactory
			{
				_factory = ChunkFaceFactory.Create(mapTextureConfig)
			};
		}

		public IChunk CreateChunkAt(IntVector3 position)
		{
			return new Chunk
			{
				FaceFactory = _factory,
				Position = position
			};
		}
	}
}
