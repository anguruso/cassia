﻿using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Config;
using Game.Controllers.Units;

namespace Game.Models.Occupants.Units.UnitActions
{
	public class UnitAction : IInjectableUnitAction
    {
	    private UnitActionConfig _config;

        [Inject]
	    public IUnitActionRangeCalculator RangeCalculator { get; set; }

        [Inject]
	    public IUnitActionEffectBuildersFactory EffectBuildersFactory { get; set; }

        public void LoadConfig(UnitActionConfig config)
	    {
	        _config = config;
	    }

        public string Name { get { return _config.Name; } }
        public string Description { get { return _config.Description; } }
        public IEnumerable<GridCoordsOffset> TargetRange { get { return _config.TargetRange.ToList(); } }
        public IEnumerable<GridCoordsOffset> EffectiveRange { get { return _config.AreaOfEffect.ToList(); } }

	    public IEnumerable<IUnitActionEffectBuilder> TargetEffectBuilders
	    {
		    get { return EffectBuildersFactory.GetEffectBuilders(_config.Id, _config.TargetEffects); }
	    }

	    public IEnumerable<IUnitActionEffectBuilder> SelfEffectBuilders
	    {
		    get { return EffectBuildersFactory.GetEffectBuilders(_config.Id, _config.ActorEffects); }
	    }

	    public IEnumerable<GridCoords> GetTargetRange(GridCoords target)
	    {
	        return RangeCalculator.GetRotatedRange(target, _config.TargetRange);
	    }

	    public IEnumerable<GridCoords> GetAreaOfEffect(GridCoords target, Facing direction)
	    {
	        return RangeCalculator.GetRange(target, direction, _config.AreaOfEffect);
	    }
	}
}
