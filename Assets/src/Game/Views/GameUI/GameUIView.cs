﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Views.GameUI.MonoBehaviours;
using Game.Views.MonoBehaviours.UI;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Views.GameUI
{
    public class GameUIView : View
    {
        public UnitOverviewUI UnitOverview;
        public List<UnitActionButton> UnitActionButtons;
        public ConfirmTransactionUI ConfirmTransaction;

        public Signal<int> UnitActionButtonClicked = new Signal<int>();
        public Signal ApplyButtonClicked = new Signal();
	    public Signal CancelButtonClicked = new Signal();

        protected override void Start()
        {
            for (var i = 0; i < UnitActionButtons.Count; ++i)
            {
                UnitActionButtons[i].Index = i;
                UnitActionButtons[i].OnClick(UnitActionButtonClicked.Dispatch);
            }

            ConfirmTransaction.OnApply.AddListener(ApplyButtonClicked.Dispatch);
			ConfirmTransaction.OnCancel.AddListener(CancelButtonClicked.Dispatch);
        }

        public void DisplayNothing()
        {
            UnitOverview.Hide();
            foreach (var unitActionButton in UnitActionButtons)
            {
                unitActionButton.gameObject.SetActive(false);
            }
            ConfirmTransaction.Hide();
        }

        public void DisplayUnitOverview(IUnit unit)
        {
			UnitOverview.ShowUnitOverview(unit);
			foreach (var unitActionButton in UnitActionButtons)
			{
				unitActionButton.gameObject.SetActive(false);
			}
	        ConfirmTransaction.Hide();
        }

        public void DisplayUnitActions(IUnit unit, IList<string> unitActionNames)
        {
			UnitOverview.ShowUnitOverview(unit);
			if (unitActionNames.Count > UnitActionButtons.Count)
            {
                var message = string.Format("Can only display {0} or fewer UnitActions.", UnitActionButtons.Count);
                throw new ArgumentException(message);
            }

            for (var i = 0; i < UnitActionButtons.Count; ++i)
            {
                if (unitActionNames.Count > i)
                {
                    UnitActionButtons[i].gameObject.SetActive(true);
                    UnitActionButtons[i].Text = unitActionNames[i];
                }
                else
                {
                    UnitActionButtons[i].gameObject.SetActive(false);
                }
            }
			ConfirmTransaction.Hide();
		}

        public void DisplayUnitActionEffects(IEnumerable<IUnitActionEffect> effects)
        {
	        UnitOverview.Hide();
			foreach (var unitActionButton in UnitActionButtons)
			{
				unitActionButton.gameObject.SetActive(false);
			}

			// TODO: only first effect for now
	        var firstEffect = effects.First();
			ConfirmTransaction.Show(firstEffect.Delta, Mathf.FloorToInt(firstEffect.Accuracy * 100));
        }
    }
}
